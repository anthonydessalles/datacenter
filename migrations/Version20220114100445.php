<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220114100445 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE media DROP rating, DROP position, DROP price, DROP played, DROP goals, DROP assists, DROP yellow_cards, DROP red_cards');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE media ADD rating VARCHAR(255) CHARACTER SET utf8 DEFAULT NULL COLLATE `utf8_unicode_ci`, ADD position VARCHAR(255) CHARACTER SET utf8 DEFAULT NULL COLLATE `utf8_unicode_ci`, ADD price DOUBLE PRECISION DEFAULT NULL, ADD played DOUBLE PRECISION DEFAULT NULL, ADD goals DOUBLE PRECISION DEFAULT NULL, ADD assists DOUBLE PRECISION DEFAULT NULL, ADD yellow_cards DOUBLE PRECISION DEFAULT NULL, ADD red_cards DOUBLE PRECISION DEFAULT NULL');
    }
}
