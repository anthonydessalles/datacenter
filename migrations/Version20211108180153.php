<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211108180153 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE profile (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE profile_link (profile_id INT NOT NULL, link_id INT NOT NULL, INDEX IDX_80641665CCFA12B8 (profile_id), INDEX IDX_80641665ADA40271 (link_id), PRIMARY KEY(profile_id, link_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE profile_playlist (profile_id INT NOT NULL, playlist_id INT NOT NULL, INDEX IDX_2EE8709CCCFA12B8 (profile_id), INDEX IDX_2EE8709C6BBD148 (playlist_id), PRIMARY KEY(profile_id, playlist_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE profile_link ADD CONSTRAINT FK_80641665CCFA12B8 FOREIGN KEY (profile_id) REFERENCES profile (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE profile_link ADD CONSTRAINT FK_80641665ADA40271 FOREIGN KEY (link_id) REFERENCES link (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE profile_playlist ADD CONSTRAINT FK_2EE8709CCCFA12B8 FOREIGN KEY (profile_id) REFERENCES profile (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE profile_playlist ADD CONSTRAINT FK_2EE8709C6BBD148 FOREIGN KEY (playlist_id) REFERENCES playlist (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE profile_link DROP FOREIGN KEY FK_80641665CCFA12B8');
        $this->addSql('ALTER TABLE profile_playlist DROP FOREIGN KEY FK_2EE8709CCCFA12B8');
        $this->addSql('DROP TABLE profile');
        $this->addSql('DROP TABLE profile_link');
        $this->addSql('DROP TABLE profile_playlist');
    }
}
