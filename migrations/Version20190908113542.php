<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190908113542 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE data DROP FOREIGN KEY FK_ADF3F3635CB2E05D');
        $this->addSql('ALTER TABLE data DROP FOREIGN KEY FK_ADF3F3633E4A79C1');
        $this->addSql('DROP TABLE login');
        $this->addSql('DROP TABLE password');
        $this->addSql('DROP INDEX IDX_ADF3F3635CB2E05D ON data');
        $this->addSql('DROP INDEX IDX_ADF3F3633E4A79C1 ON data');
        $this->addSql('ALTER TABLE data DROP login_id, DROP password_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE login (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE password (id INT AUTO_INCREMENT NOT NULL, hint VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE data ADD login_id INT DEFAULT NULL, ADD password_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE data ADD CONSTRAINT FK_ADF3F3633E4A79C1 FOREIGN KEY (password_id) REFERENCES password (id)');
        $this->addSql('ALTER TABLE data ADD CONSTRAINT FK_ADF3F3635CB2E05D FOREIGN KEY (login_id) REFERENCES login (id)');
        $this->addSql('CREATE INDEX IDX_ADF3F3635CB2E05D ON data (login_id)');
        $this->addSql('CREATE INDEX IDX_ADF3F3633E4A79C1 ON data (password_id)');
    }
}
