<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220728120210 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE video_type DROP FOREIGN KEY FK_8B490AF529C1004E');
        $this->addSql('CREATE TABLE sport (id INT AUTO_INCREMENT NOT NULL, slug VARCHAR(128) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, title VARCHAR(256) DEFAULT NULL, fr_title VARCHAR(256) DEFAULT NULL, en_title VARCHAR(256) DEFAULT NULL, year INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sport_type (sport_id INT NOT NULL, type_id INT NOT NULL, INDEX IDX_FB0179D5AC78BCF8 (sport_id), INDEX IDX_FB0179D5C54C8C93 (type_id), PRIMARY KEY(sport_id, type_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE sport_type ADD CONSTRAINT FK_FB0179D5AC78BCF8 FOREIGN KEY (sport_id) REFERENCES sport (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE sport_type ADD CONSTRAINT FK_FB0179D5C54C8C93 FOREIGN KEY (type_id) REFERENCES type (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE video');
        $this->addSql('DROP TABLE video_type');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE sport_type DROP FOREIGN KEY FK_FB0179D5AC78BCF8');
        $this->addSql('CREATE TABLE video (id INT AUTO_INCREMENT NOT NULL, allocine INT DEFAULT NULL, slug VARCHAR(128) CHARACTER SET utf8 NOT NULL COLLATE `utf8_general_ci`, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, title VARCHAR(256) CHARACTER SET utf8 DEFAULT NULL COLLATE `utf8_general_ci`, synopsis LONGTEXT CHARACTER SET utf8 DEFAULT NULL COLLATE `utf8_general_ci`, fr_title VARCHAR(256) CHARACTER SET utf8 DEFAULT NULL COLLATE `utf8_general_ci`, imdb VARCHAR(256) CHARACTER SET utf8 DEFAULT NULL COLLATE `utf8_general_ci`, en_title VARCHAR(256) CHARACTER SET utf8 DEFAULT NULL COLLATE `utf8_general_ci`, year INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE video_type (video_id INT NOT NULL, type_id INT NOT NULL, INDEX IDX_8B490AF529C1004E (video_id), INDEX IDX_8B490AF5C54C8C93 (type_id), PRIMARY KEY(video_id, type_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE video_type ADD CONSTRAINT FK_8B490AF529C1004E FOREIGN KEY (video_id) REFERENCES video (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE video_type ADD CONSTRAINT FK_8B490AF5C54C8C93 FOREIGN KEY (type_id) REFERENCES type (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE sport');
        $this->addSql('DROP TABLE sport_type');
    }
}
