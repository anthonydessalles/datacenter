<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210202121642 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE device ADD number INT NOT NULL, ADD bugs VARCHAR(256) DEFAULT NULL, CHANGE achat purchase VARCHAR(256) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
    }
}
