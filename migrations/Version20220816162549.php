<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220816162549 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE compatibility (id INT AUTO_INCREMENT NOT NULL, creator_id INT NOT NULL, game_id INT NOT NULL, type_id INT NOT NULL, device_id INT NOT NULL, storage VARCHAR(255) NOT NULL, status VARCHAR(255) NOT NULL, software VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_73CC286461220EA6 (creator_id), INDEX IDX_73CC2864E48FD905 (game_id), INDEX IDX_73CC2864C54C8C93 (type_id), INDEX IDX_73CC286494A4C7D4 (device_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE compatibility ADD CONSTRAINT FK_73CC286461220EA6 FOREIGN KEY (creator_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE compatibility ADD CONSTRAINT FK_73CC2864E48FD905 FOREIGN KEY (game_id) REFERENCES game (id)');
        $this->addSql('ALTER TABLE compatibility ADD CONSTRAINT FK_73CC2864C54C8C93 FOREIGN KEY (type_id) REFERENCES type (id)');
        $this->addSql('ALTER TABLE compatibility ADD CONSTRAINT FK_73CC286494A4C7D4 FOREIGN KEY (device_id) REFERENCES device (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE compatibility');
    }
}
