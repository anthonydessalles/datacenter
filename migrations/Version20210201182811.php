<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210201182811 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $sql = <<<SQL
ALTER TABLE video CHANGE french_title fr_title varchar(256);
ALTER TABLE video CHANGE original_title en_title varchar(256);
SQL;

        $this->addSql($sql);
    }

    public function down(Schema $schema) : void
    {
    }
}
