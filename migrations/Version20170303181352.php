<?php

namespace Application\Migrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170303181352 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE user_has_score (id INT AUTO_INCREMENT NOT NULL, score_id INT DEFAULT NULL, user_id INT DEFAULT NULL, is_home TINYINT(1) NOT NULL, points DOUBLE PRECISION NOT NULL, assists DOUBLE PRECISION DEFAULT NULL, saves DOUBLE PRECISION DEFAULT NULL, INDEX IDX_C9F0AA0B12EB0A51 (score_id), INDEX IDX_C9F0AA0BA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user_has_score ADD CONSTRAINT FK_C9F0AA0B12EB0A51 FOREIGN KEY (score_id) REFERENCES score (id)');
        $this->addSql('ALTER TABLE user_has_score ADD CONSTRAINT FK_C9F0AA0BA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE activity CHANGE start start DATETIME NOT NULL, CHANGE end end DATETIME NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE user_has_score');
        $this->addSql('ALTER TABLE activity CHANGE start start DATETIME NOT NULL, CHANGE end end DATETIME NOT NULL');
    }
}
