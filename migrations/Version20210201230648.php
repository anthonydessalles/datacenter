<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210201230648 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE book_type (book_id INT NOT NULL, type_id INT NOT NULL, INDEX IDX_95431C2116A2B381 (book_id), INDEX IDX_95431C21C54C8C93 (type_id), PRIMARY KEY(book_id, type_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE book_type ADD CONSTRAINT FK_95431C2116A2B381 FOREIGN KEY (book_id) REFERENCES book (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE book_type ADD CONSTRAINT FK_95431C21C54C8C93 FOREIGN KEY (type_id) REFERENCES type (id) ON DELETE CASCADE');
        $this->addSql('DROP INDEX IDX_9897EF97BE74E59A ON book');
        $this->addSql('DROP INDEX IDX_9897EF97989D9B62 ON book');
        $this->addSql('DROP INDEX IDX_9897EF97AFBA6FD8 ON book');
        $this->addSql('ALTER TABLE book CHANGE pagination pagination INT NOT NULL, CHANGE price price DOUBLE PRECISION NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE book_type');
        $this->addSql('ALTER TABLE book CHANGE pagination pagination INT DEFAULT 0 NOT NULL, CHANGE price price DOUBLE PRECISION DEFAULT \'0\' NOT NULL');
        $this->addSql('CREATE INDEX IDX_9897EF97BE74E59A ON book (updated_at)');
        $this->addSql('CREATE INDEX IDX_9897EF97989D9B62 ON book (slug)');
        $this->addSql('CREATE INDEX IDX_9897EF97AFBA6FD8 ON book (created_at)');
    }
}
