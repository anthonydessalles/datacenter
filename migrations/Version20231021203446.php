<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231021203446 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE game_link DROP FOREIGN KEY FK_DDB9F5DDE48FD905');
        $this->addSql('ALTER TABLE game_link DROP FOREIGN KEY FK_DDB9F5DDADA40271');
        $this->addSql('DROP TABLE game_link');
        $this->addSql('ALTER TABLE game ADD igdb_id INT DEFAULT NULL, ADD igdb_url VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
    }
}
