<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231012111951 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE profile_sport (profile_id INT NOT NULL, sport_id INT NOT NULL, INDEX IDX_ED517000CCFA12B8 (profile_id), INDEX IDX_ED517000AC78BCF8 (sport_id), PRIMARY KEY(profile_id, sport_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE profile_sport ADD CONSTRAINT FK_ED517000CCFA12B8 FOREIGN KEY (profile_id) REFERENCES profile (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE profile_sport ADD CONSTRAINT FK_ED517000AC78BCF8 FOREIGN KEY (sport_id) REFERENCES sport (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE profile_sport DROP FOREIGN KEY FK_ED517000CCFA12B8');
        $this->addSql('ALTER TABLE profile_sport DROP FOREIGN KEY FK_ED517000AC78BCF8');
        $this->addSql('DROP TABLE profile_sport');
    }
}
