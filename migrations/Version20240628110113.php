<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240628110113 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE game_screenshot (game_id INT NOT NULL, screenshot_id INT NOT NULL, INDEX IDX_B645D9ACE48FD905 (game_id), INDEX IDX_B645D9AC4A8B36A0 (screenshot_id), PRIMARY KEY(game_id, screenshot_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE game_screenshot ADD CONSTRAINT FK_B645D9ACE48FD905 FOREIGN KEY (game_id) REFERENCES game (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE game_screenshot ADD CONSTRAINT FK_B645D9AC4A8B36A0 FOREIGN KEY (screenshot_id) REFERENCES screenshot (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
    }
}
