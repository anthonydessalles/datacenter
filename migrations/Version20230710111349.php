<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230710111349 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE stand_up_file (stand_up_id INT NOT NULL, file_id INT NOT NULL, INDEX IDX_5E11E3E13CB08647 (stand_up_id), INDEX IDX_5E11E3E193CB796C (file_id), PRIMARY KEY(stand_up_id, file_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE stand_up_file ADD CONSTRAINT FK_5E11E3E13CB08647 FOREIGN KEY (stand_up_id) REFERENCES stand_up (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE stand_up_file ADD CONSTRAINT FK_5E11E3E193CB796C FOREIGN KEY (file_id) REFERENCES file (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE stand_up ADD max_height INT DEFAULT NULL, ADD max_width INT DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE stand_up_file DROP FOREIGN KEY FK_5E11E3E13CB08647');
        $this->addSql('ALTER TABLE stand_up_file DROP FOREIGN KEY FK_5E11E3E193CB796C');
        $this->addSql('DROP TABLE stand_up_file');
        $this->addSql('ALTER TABLE stand_up DROP max_height, DROP max_width');
    }
}
