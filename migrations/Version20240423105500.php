<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240423105500 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE profile_recording (profile_id INT NOT NULL, recording_id INT NOT NULL, INDEX IDX_7CCC228CCFA12B8 (profile_id), INDEX IDX_7CCC2288CA9A845 (recording_id), PRIMARY KEY(profile_id, recording_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE recording (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, source_url VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE profile_recording ADD CONSTRAINT FK_7CCC228CCFA12B8 FOREIGN KEY (profile_id) REFERENCES profile (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE profile_recording ADD CONSTRAINT FK_7CCC2288CA9A845 FOREIGN KEY (recording_id) REFERENCES recording (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
    }
}
