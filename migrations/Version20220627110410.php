<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220627110410 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE film (id INT AUTO_INCREMENT NOT NULL, slug VARCHAR(255) NOT NULL, allocine INT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, title VARCHAR(255) NOT NULL, fr_title VARCHAR(255) DEFAULT NULL, imdb VARCHAR(255) DEFAULT NULL, en_title VARCHAR(255) NOT NULL, year INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE film_type (film_id INT NOT NULL, type_id INT NOT NULL, INDEX IDX_E971BD2A567F5183 (film_id), INDEX IDX_E971BD2AC54C8C93 (type_id), PRIMARY KEY(film_id, type_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE film_album (film_id INT NOT NULL, album_id INT NOT NULL, INDEX IDX_A0F49013567F5183 (film_id), INDEX IDX_A0F490131137ABCF (album_id), PRIMARY KEY(film_id, album_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE film_type ADD CONSTRAINT FK_E971BD2A567F5183 FOREIGN KEY (film_id) REFERENCES film (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE film_type ADD CONSTRAINT FK_E971BD2AC54C8C93 FOREIGN KEY (type_id) REFERENCES type (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE film_album ADD CONSTRAINT FK_A0F49013567F5183 FOREIGN KEY (film_id) REFERENCES film (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE film_album ADD CONSTRAINT FK_A0F490131137ABCF FOREIGN KEY (album_id) REFERENCES album (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE film_type DROP FOREIGN KEY FK_E971BD2A567F5183');
        $this->addSql('ALTER TABLE film_album DROP FOREIGN KEY FK_A0F49013567F5183');
        $this->addSql('DROP TABLE film');
        $this->addSql('DROP TABLE film_type');
        $this->addSql('DROP TABLE film_album');
    }
}
