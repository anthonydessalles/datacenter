<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230803112224 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user_has_score ADD file_id INT DEFAULT NULL, ADD game_time INT DEFAULT NULL, ADD distance DOUBLE PRECISION DEFAULT NULL, ADD sprints INT DEFAULT NULL, ADD activity INT DEFAULT NULL, ADD max_sprint DOUBLE PRECISION DEFAULT NULL, ADD average_sprint DOUBLE PRECISION DEFAULT NULL, ADD max_shot DOUBLE PRECISION DEFAULT NULL, ADD running_time VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE user_has_score ADD CONSTRAINT FK_C9F0AA0B93CB796C FOREIGN KEY (file_id) REFERENCES file (id)');
        $this->addSql('CREATE INDEX IDX_C9F0AA0B93CB796C ON user_has_score (file_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user_has_score DROP FOREIGN KEY FK_C9F0AA0B93CB796C');
        $this->addSql('DROP INDEX IDX_C9F0AA0B93CB796C ON user_has_score');
        $this->addSql('ALTER TABLE user_has_score DROP file_id, DROP game_time, DROP distance, DROP sprints, DROP activity, DROP max_sprint, DROP average_sprint, DROP max_shot, DROP running_time');
    }
}
