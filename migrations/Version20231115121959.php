<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231115121959 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE game ADD suggestion_id INT DEFAULT NULL, ADD difficulties VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE game ADD CONSTRAINT FK_232B318CA41BB822 FOREIGN KEY (suggestion_id) REFERENCES game (id)');
        $this->addSql('CREATE INDEX IDX_232B318CA41BB822 ON game (suggestion_id)');
    }

    public function down(Schema $schema): void
    {
    }
}
