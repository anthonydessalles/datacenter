<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220728113409 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE stand_up (id INT AUTO_INCREMENT NOT NULL, slug VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, title VARCHAR(255) NOT NULL, fr_title VARCHAR(255) DEFAULT NULL, en_title VARCHAR(255) DEFAULT NULL, year INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE stand_up_type (stand_up_id INT NOT NULL, type_id INT NOT NULL, INDEX IDX_5E5082D83CB08647 (stand_up_id), INDEX IDX_5E5082D8C54C8C93 (type_id), PRIMARY KEY(stand_up_id, type_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE stand_up_album (stand_up_id INT NOT NULL, album_id INT NOT NULL, INDEX IDX_F3F0221C3CB08647 (stand_up_id), INDEX IDX_F3F0221C1137ABCF (album_id), PRIMARY KEY(stand_up_id, album_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE stand_up_type ADD CONSTRAINT FK_5E5082D83CB08647 FOREIGN KEY (stand_up_id) REFERENCES stand_up (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE stand_up_type ADD CONSTRAINT FK_5E5082D8C54C8C93 FOREIGN KEY (type_id) REFERENCES type (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE stand_up_album ADD CONSTRAINT FK_F3F0221C3CB08647 FOREIGN KEY (stand_up_id) REFERENCES stand_up (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE stand_up_album ADD CONSTRAINT FK_F3F0221C1137ABCF FOREIGN KEY (album_id) REFERENCES album (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE stand_up_type DROP FOREIGN KEY FK_5E5082D83CB08647');
        $this->addSql('ALTER TABLE stand_up_album DROP FOREIGN KEY FK_F3F0221C3CB08647');
        $this->addSql('DROP TABLE stand_up');
        $this->addSql('DROP TABLE stand_up_type');
        $this->addSql('DROP TABLE stand_up_album');
    }
}
