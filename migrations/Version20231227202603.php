<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231227202603 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE profile_type (profile_id INT NOT NULL, type_id INT NOT NULL, INDEX IDX_3A16D8BDCCFA12B8 (profile_id), INDEX IDX_3A16D8BDC54C8C93 (type_id), PRIMARY KEY(profile_id, type_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE serie_link (serie_id INT NOT NULL, link_id INT NOT NULL, INDEX IDX_EDC98DC3D94388BD (serie_id), INDEX IDX_EDC98DC3ADA40271 (link_id), PRIMARY KEY(serie_id, link_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_file (user_id INT NOT NULL, file_id INT NOT NULL, INDEX IDX_F61E7AD9A76ED395 (user_id), INDEX IDX_F61E7AD993CB796C (file_id), PRIMARY KEY(user_id, file_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE profile_type ADD CONSTRAINT FK_3A16D8BDCCFA12B8 FOREIGN KEY (profile_id) REFERENCES profile (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE profile_type ADD CONSTRAINT FK_3A16D8BDC54C8C93 FOREIGN KEY (type_id) REFERENCES type (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE serie_link ADD CONSTRAINT FK_EDC98DC3D94388BD FOREIGN KEY (serie_id) REFERENCES serie (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE serie_link ADD CONSTRAINT FK_EDC98DC3ADA40271 FOREIGN KEY (link_id) REFERENCES link (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_file ADD CONSTRAINT FK_F61E7AD9A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_file ADD CONSTRAINT FK_F61E7AD993CB796C FOREIGN KEY (file_id) REFERENCES file (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE progression ADD CONSTRAINT FK_D5B25073C54C8C93 FOREIGN KEY (type_id) REFERENCES type (id)');
        $this->addSql('CREATE INDEX IDX_D5B25073C54C8C93 ON progression (type_id)');
        $this->addSql('ALTER TABLE progression RENAME INDEX fk_d5b25073e48fd905 TO IDX_D5B25073E48FD905');
    }

    public function down(Schema $schema): void
    {
    }
}
