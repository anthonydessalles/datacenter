<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190227214000 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE source (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('DROP INDEX name ON activity');
        $this->addSql('ALTER TABLE file ADD source_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE file ADD CONSTRAINT FK_8C9F3610953C1C61 FOREIGN KEY (source_id) REFERENCES source (id)');
        $this->addSql('CREATE INDEX IDX_8C9F3610953C1C61 ON file (source_id)');
        $this->addSql('ALTER TABLE game_has_media DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE game_has_media ADD PRIMARY KEY (media_id, game_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE file DROP FOREIGN KEY FK_8C9F3610953C1C61');
        $this->addSql('DROP TABLE source');
        $this->addSql('CREATE UNIQUE INDEX name ON activity (name, start, end)');
        $this->addSql('DROP INDEX IDX_8C9F3610953C1C61 ON file');
        $this->addSql('ALTER TABLE file DROP source_id');
        $this->addSql('ALTER TABLE game_has_media DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE game_has_media ADD PRIMARY KEY (game_id, media_id)');
    }
}
