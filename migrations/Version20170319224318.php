<?php

namespace Application\Migrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170319224318 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        // Pays
        $this->addSql("INSERT INTO `country` (`id`, `slug`) VALUES (1, 'france');");
        
        // Adresse
        $this->addSql("INSERT INTO `location` (`id`, `country_id`, `slug`, `latitude`, `longitude`) VALUES
        (1, 1, 'lyon-09', NULL, NULL),
        (2, 1, 'saint-priest', NULL, NULL);");
        
        // Endroit
        $this->addSql("INSERT INTO `place` (`id`, `location_id`, `name`, `description`) VALUES
        (1, 1, 'Sport dans la Ville', NULL),
        (2, 2, 'iPlay', NULL),
        (3, 1, 'Keep Cool Vaise', NULL);");
        
        // Type
        $this->addSql("INSERT INTO `type` (`id`, `slug`, `font_awesome`) VALUES
        (1, 'profile', 'user'),
        (2, 'naming', 'pencil'),
        (3, 'software', 'windows'),
        (4, 'google_extension', 'plus'),
        (5, 'football', 'futbol-o'),
        (6, 'gym', 'heart-o');");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql("DELETE FROM `user` WHERE `user`.`id` = 1;");
        $this->addSql("DELETE FROM `type` WHERE `type`.`id` IN (1, 2, 3, 4, 5, 6);");
        $this->addSql("DELETE FROM `place` WHERE `place`.`id` IN (1, 2, 3);");
        $this->addSql("DELETE FROM `location` WHERE `location`.`id` IN (1, 2);");
        $this->addSql("DELETE FROM `country` WHERE `country`.`id` = 1;");
    }
}
