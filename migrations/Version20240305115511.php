<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240305115511 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE film ADD of_the_year INT DEFAULT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8244BE223146DEB2 ON film (of_the_year)');
        $this->addSql('ALTER TABLE game ADD of_the_year INT DEFAULT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_232B318C3146DEB2 ON game (of_the_year)');
    }

    public function down(Schema $schema): void
    {
    }
}
