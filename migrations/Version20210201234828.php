<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210201234828 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE device_type (device_id INT NOT NULL, type_id INT NOT NULL, INDEX IDX_5E7821394A4C7D4 (device_id), INDEX IDX_5E78213C54C8C93 (type_id), PRIMARY KEY(device_id, type_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE device_type ADD CONSTRAINT FK_5E7821394A4C7D4 FOREIGN KEY (device_id) REFERENCES device (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE device_type ADD CONSTRAINT FK_5E78213C54C8C93 FOREIGN KEY (type_id) REFERENCES type (id) ON DELETE CASCADE');
        $this->addSql('DROP INDEX IDX_1A6950AF989D9B62 ON device');
        $this->addSql('DROP INDEX IDX_1A6950AFBE74E59A ON device');
        $this->addSql('DROP INDEX IDX_1A6950AFAFBA6FD8 ON device');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE device_type');
        $this->addSql('CREATE INDEX IDX_1A6950AF989D9B62 ON device (slug)');
        $this->addSql('CREATE INDEX IDX_1A6950AFBE74E59A ON device (updated_at)');
        $this->addSql('CREATE INDEX IDX_1A6950AFAFBA6FD8 ON device (created_at)');
    }
}
