<?php

namespace Application\Migrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170207220859 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE data (id INT AUTO_INCREMENT NOT NULL, type_id INT DEFAULT NULL, login_id INT DEFAULT NULL, password_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, description LONGTEXT DEFAULT NULL, INDEX IDX_ADF3F363C54C8C93 (type_id), INDEX IDX_ADF3F3635CB2E05D (login_id), INDEX IDX_ADF3F3633E4A79C1 (password_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE data_has_link (data_id INT NOT NULL, link_id INT NOT NULL, INDEX IDX_A12E8B7237F5A13C (data_id), INDEX IDX_A12E8B72ADA40271 (link_id), PRIMARY KEY(data_id, link_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE link (id INT AUTO_INCREMENT NOT NULL, url VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_36AC99F1F47645AE (url), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE login (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE password (id INT AUTO_INCREMENT NOT NULL, hint VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE project (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, repository VARCHAR(255) DEFAULT NULL, number DOUBLE PRECISION DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE project_has_version (project_id INT NOT NULL, version_id INT NOT NULL, INDEX IDX_10224CE3166D1F9C (project_id), INDEX IDX_10224CE34BBC2705 (version_id), PRIMARY KEY(project_id, version_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE type (id INT AUTO_INCREMENT NOT NULL, slug VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(50) NOT NULL, password VARCHAR(88) NOT NULL, salt VARCHAR(23) NOT NULL, role VARCHAR(50) NOT NULL, UNIQUE INDEX UNIQ_8D93D6495E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE version (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, date DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE data ADD CONSTRAINT FK_ADF3F363C54C8C93 FOREIGN KEY (type_id) REFERENCES type (id)');
        $this->addSql('ALTER TABLE data ADD CONSTRAINT FK_ADF3F3635CB2E05D FOREIGN KEY (login_id) REFERENCES login (id)');
        $this->addSql('ALTER TABLE data ADD CONSTRAINT FK_ADF3F3633E4A79C1 FOREIGN KEY (password_id) REFERENCES password (id)');
        $this->addSql('ALTER TABLE data_has_link ADD CONSTRAINT FK_A12E8B7237F5A13C FOREIGN KEY (data_id) REFERENCES data (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE data_has_link ADD CONSTRAINT FK_A12E8B72ADA40271 FOREIGN KEY (link_id) REFERENCES link (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE project_has_version ADD CONSTRAINT FK_10224CE3166D1F9C FOREIGN KEY (project_id) REFERENCES project (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE project_has_version ADD CONSTRAINT FK_10224CE34BBC2705 FOREIGN KEY (version_id) REFERENCES version (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE data_has_link DROP FOREIGN KEY FK_A12E8B7237F5A13C');
        $this->addSql('ALTER TABLE data_has_link DROP FOREIGN KEY FK_A12E8B72ADA40271');
        $this->addSql('ALTER TABLE data DROP FOREIGN KEY FK_ADF3F3635CB2E05D');
        $this->addSql('ALTER TABLE data DROP FOREIGN KEY FK_ADF3F3633E4A79C1');
        $this->addSql('ALTER TABLE project_has_version DROP FOREIGN KEY FK_10224CE3166D1F9C');
        $this->addSql('ALTER TABLE data DROP FOREIGN KEY FK_ADF3F363C54C8C93');
        $this->addSql('ALTER TABLE project_has_version DROP FOREIGN KEY FK_10224CE34BBC2705');
        $this->addSql('DROP TABLE data');
        $this->addSql('DROP TABLE data_has_link');
        $this->addSql('DROP TABLE link');
        $this->addSql('DROP TABLE login');
        $this->addSql('DROP TABLE password');
        $this->addSql('DROP TABLE project');
        $this->addSql('DROP TABLE project_has_version');
        $this->addSql('DROP TABLE type');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE version');
    }
}
