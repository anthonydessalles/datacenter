<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220728111416 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE serie (id INT AUTO_INCREMENT NOT NULL, slug VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, title VARCHAR(255) NOT NULL, fr_title VARCHAR(255) DEFAULT NULL, en_title VARCHAR(255) DEFAULT NULL, year INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE serie_type (serie_id INT NOT NULL, type_id INT NOT NULL, INDEX IDX_57BB431BD94388BD (serie_id), INDEX IDX_57BB431BC54C8C93 (type_id), PRIMARY KEY(serie_id, type_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE serie_album (serie_id INT NOT NULL, album_id INT NOT NULL, INDEX IDX_F1945AD7D94388BD (serie_id), INDEX IDX_F1945AD71137ABCF (album_id), PRIMARY KEY(serie_id, album_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE serie_type ADD CONSTRAINT FK_57BB431BD94388BD FOREIGN KEY (serie_id) REFERENCES serie (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE serie_type ADD CONSTRAINT FK_57BB431BC54C8C93 FOREIGN KEY (type_id) REFERENCES type (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE serie_album ADD CONSTRAINT FK_F1945AD7D94388BD FOREIGN KEY (serie_id) REFERENCES serie (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE serie_album ADD CONSTRAINT FK_F1945AD71137ABCF FOREIGN KEY (album_id) REFERENCES album (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE serie_type DROP FOREIGN KEY FK_57BB431BD94388BD');
        $this->addSql('ALTER TABLE serie_album DROP FOREIGN KEY FK_F1945AD7D94388BD');
        $this->addSql('DROP TABLE serie');
        $this->addSql('DROP TABLE serie_type');
        $this->addSql('DROP TABLE serie_album');
    }
}
