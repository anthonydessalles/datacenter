<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230222214724 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE compatibility ADD updated_by_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE compatibility ADD CONSTRAINT FK_73CC2864896DBBDE FOREIGN KEY (updated_by_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_73CC2864896DBBDE ON compatibility (updated_by_id)');
    }

    public function down(Schema $schema): void
    {
    }
}
