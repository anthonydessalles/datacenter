<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210131184010 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $sqlVideo = <<<SQL
-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: db
-- Generation Time: Jan 31, 2021 at 06:39 PM
-- Server version: 5.7.32
-- PHP Version: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `datacenter`
--

-- --------------------------------------------------------

--
-- Table structure for table `video`
--

CREATE TABLE `video` (
  `id` int(11) NOT NULL,
  `allocine` int(11) NOT NULL DEFAULT '0',
  `slug` varchar(128) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `title` varchar(256) DEFAULT NULL,
  `synopsis` longtext,
  `french_title` varchar(256) DEFAULT NULL,
  `imdb` varchar(256) DEFAULT NULL,
  `original_title` varchar(256) DEFAULT NULL,
  `year` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `video`
--

INSERT INTO `video` (`id`, `allocine`, `slug`, `created_at`, `updated_at`, `title`, `synopsis`, `french_title`, `imdb`, `original_title`, `year`) VALUES
(9, 29279, 'american-pie-2', '2014-07-03 07:38:39', '2020-01-13 22:46:27', 'American Pie 2', '<p><a href=\"https://fr.wikipedia.org/wiki/American_Pie_2\" target=\"_blank\">https://fr.wikipedia.org/wiki/American_Pie_2</a></p>\r\n', NULL, 'tt0252866', NULL, 2001),
(16, 130440, 'avengers', '2014-07-03 07:39:29', '2020-01-13 22:47:26', 'Avengers', '<p><a href=\"https://fr.wikipedia.org/wiki/Avengers_(film)\" target=\"_blank\">fr.wikipedia.org/wiki/Avengers_(film)</a></p>\r\n', 'Avengers', 'tt0848228', 'Marvel\'s The Avengers', 2012),
(20, 51013, 'batman-begins', '2014-07-03 07:39:31', '2020-01-13 22:48:26', 'Batman Begins', '<p><a href=\"https://fr.wikipedia.org/wiki/Batman_Begins\" target=\"_blank\">https://fr.wikipedia.org/wiki/Batman_Begins</a></p>\r\n', '', 'tt0372784', NULL, 2005),
(21, 110161, 'batman-vs-dracula', '2014-07-03 08:27:11', '2020-01-13 23:06:13', 'Batman Vs Dracula', '<p><a href=\"https://en.wikipedia.org/wiki/The_Batman_vs._Dracula\" target=\"_blank\">https://en.wikipedia.org/wiki/The_Batman_vs._Dracula</a></p>\r\n', '', 'tt0472219', NULL, 2005),
(22, 133496, 'batman-contre-le-fantome-masque', '2014-07-03 08:29:50', '2020-01-13 23:07:33', 'Batman contre le Fantôme Masqué', '<p><a href=\"https://fr.wikipedia.org/wiki/Batman_contre_le_fant%C3%B4me_masqu%C3%A9\" target=\"_blank\">https://fr.wikipedia.org/wiki/Batman_contre_le_fant%C3%B4me_masqu%C3%A9</a></p>\r\n', '', 'tt0106364', NULL, 1993),
(23, 210807, 'batman-et-mr-freeze-subzero', '2014-07-03 08:31:41', '2020-01-13 23:08:41', 'Batman et Mr. Freeze : Subzero', '<p><a href=\"https://fr.wikipedia.org/wiki/Batman_et_Mr._Freeze_:_Subzero\" target=\"_blank\">https://fr.wikipedia.org/wiki/Batman_et_Mr._Freeze_:_Subzero</a></p>\r\n', '', 'tt0143127', NULL, 1998),
(25, 11181, 'batman-and-robin', '2014-07-03 08:38:11', '2020-01-13 23:09:55', 'Batman & Robin', '<p><a href=\"https://fr.wikipedia.org/wiki/Batman_et_Robin_(film,_1997)\" target=\"_blank\">https://fr.wikipedia.org/wiki/Batman_et_Robin_(film,_1997)</a></p>\r\n', '', 'tt0118688', NULL, 1997),
(26, 12858, 'batman-forever', '2014-07-03 08:39:26', '2020-01-13 23:10:29', 'Batman Forever', '<p><a href=\"https://fr.wikipedia.org/wiki/Batman_Forever\" target=\"_blank\">https://fr.wikipedia.org/wiki/Batman_Forever</a></p>\r\n', '', 'tt0112462', NULL, 1995),
(28, 182632, 'batman-la-mysterieuse-batwoman', '2014-07-03 08:43:14', '2020-01-13 23:11:31', 'Batman : La Mystérieuse Batwoman', '<p><a href=\"https://fr.wikipedia.org/wiki/Batman_:_La_Myst%C3%A9rieuse_Batwoman\" target=\"_blank\">https://fr.wikipedia.org/wiki/Batman_:_La_Myst%C3%A9rieuse_Batwoman</a></p>\r\n', '', 'tt0346578', NULL, 2003),
(29, 7570, 'batman-returns', '2014-07-03 08:46:58', '2020-01-13 23:12:41', 'Batman : Le Défi', '<p><a href=\"https://fr.wikipedia.org/wiki/Batman_:_Le_D%C3%A9fi\" target=\"_blank\">https://fr.wikipedia.org/wiki/Batman_:_Le_D%C3%A9fi</a></p>\r\n', 'Batman : Le Défi', 'tt0103776', 'Batman Returns', 1992),
(30, 209565, 'batman-the-dark-knight-returns-partie-1', '2014-07-03 08:48:54', '2020-01-13 23:14:39', 'Batman : The Dark Knight Returns, Partie 1', '<p><a href=\"https://fr.wikipedia.org/wiki/Batman:_The_Dark_Knight_Returns_(film)#Partie_1\">https://fr.wikipedia.org/wiki/Batman:_The_Dark_Knight_Returns_(film)#Partie_1</a></p>\r\n', '', 'tt2313197', 'Batman: The Dark Knight Returns, Part 1', 2012),
(31, 214031, 'batman-the-dark-knight-returns-partie-2', '2014-07-07 02:21:11', '2020-01-13 23:16:59', 'Batman : The Dark Knight Returns, Partie 2', '<p><a href=\"https://fr.wikipedia.org/wiki/Batman:_The_Dark_Knight_Returns_(film)#Partie_2\">https://fr.wikipedia.org/wiki/Batman:_The_Dark_Knight_Returns_(film)#Partie_2</a></p>\r\n', '', 'tt2166834', 'Batman: The Dark Knight Returns, Part 2', 2013),
(33, 4966, 'batman-1989', '2014-07-03 07:41:11', '2020-01-16 08:52:02', 'Batman (1989)', '<p><a href=\"https://fr.wikipedia.org/wiki/Batman_(film,_1989)\" target=\"_blank\">https://fr.wikipedia.org/wiki/Batman_(film,_1989)</a></p>\r\n', NULL, 'tt0096895', NULL, 1989),
(41, 54682, 'bob-l-eponge-le-film', '2014-07-07 02:33:39', '2020-01-13 23:23:20', 'Bob l\'éponge - le film', '<p><a href=\"https://fr.wikipedia.org/wiki/Bob_l%27%C3%A9ponge,_le_film\" target=\"_blank\">https://fr.wikipedia.org/wiki/Bob_l%27%C3%A9ponge,_le_film</a></p>\r\n', '', 'tt0434356', NULL, 2004),
(42, 10080, 'braveheart', '2014-07-03 07:43:43', '2020-01-13 23:24:11', 'Braveheart', '<p><a href=\"https://fr.wikipedia.org/wiki/Braveheart\" target=\"_blank\">https://fr.wikipedia.org/wiki/Braveheart</a></p>\r\n', '', 'tt0112573', NULL, 1995),
(43, 136557, 'captain-america-first-avenger', '2014-07-03 07:43:44', '2020-01-13 23:24:44', 'Captain America: First Avenger', '<p><a href=\"https://fr.wikipedia.org/wiki/Captain_America:_First_Avenger\" target=\"_blank\">https://fr.wikipedia.org/wiki/Captain_America:_First_Avenger</a></p>\r\n', '', 'tt0458339', 'Captain America: The First Avenger', 2011),
(60, 28869, 'daredevil', '2014-07-03 07:44:12', '2020-01-13 23:26:09', 'Daredevil', '<p><a href=\"https://fr.wikipedia.org/wiki/Daredevil_(film)\" target=\"_blank\">https://fr.wikipedia.org/wiki/Daredevil_(film)</a></p>\r\n', '', 'tt0287978', NULL, 2003),
(73, 0, 'franck-dubosc-j-vous-ai-pas-raconte', '2014-07-07 15:15:51', '2020-01-15 21:39:10', 'Franck Dubosc : J\'vous ai pas raconté ?', '<p><a href=\"https://fr.wikipedia.org/wiki/J%27vous_ai_pas_racont%C3%A9_%3F\" target=\"_blank\">https://fr.wikipedia.org/wiki/J%27vous_ai_pas_racont%C3%A9_%3F</a></p>\r\n', '', NULL, NULL, 2002),
(76, 53298, 'espace-detente', '2014-07-03 07:44:53', '2020-01-13 23:30:43', 'Espace Détente', '<p><a href=\"https://fr.wikipedia.org/wiki/Espace_D%C3%A9tente\" target=\"_blank\">https://fr.wikipedia.org/wiki/Espace_D%C3%A9tente</a></p>\r\n', '', 'tt0409937', NULL, 2005),
(82, 26880, 'freddy-contre-jason', '2014-07-03 07:45:08', '2020-01-13 23:32:26', 'Freddy contre Jason', '<p><a href=\"https://fr.wikipedia.org/wiki/Freddy_contre_Jason\" target=\"_blank\">https://fr.wikipedia.org/wiki/Freddy_contre_Jason</a></p>\r\n', '', 'tt0329101', 'Freddy vs. Jason', 2003),
(90, 24944, 'gladiator', '2014-07-07 03:41:49', '2020-01-13 23:33:37', 'Gladiator', '<p><a href=\"https://fr.wikipedia.org/wiki/Gladiator_(film,_2000)\" target=\"_blank\">https://fr.wikipedia.org/wiki/Gladiator_(film,_2000)</a></p>\r\n', '', 'tt0172495', NULL, 2000),
(91, 13665, 'goldeneye', '2014-07-03 07:45:13', '2020-01-13 23:37:38', 'GoldenEye', '<p><a href=\"https://fr.wikipedia.org/wiki/GoldenEye\" target=\"_blank\">https://fr.wikipedia.org/wiki/GoldenEye</a></p>\r\n', '', 'tt0113189', NULL, 1995),
(99, 46865, 'harry-potter-et-le-prisonnier-d-azkaban', '2014-07-07 03:54:46', '2020-01-13 23:40:10', 'Harry Potter et le Prisonnier d\'Azkaban', '<p><a href=\"https://fr.wikipedia.org/wiki/Harry_Potter_et_le_Prisonnier_d%27Azkaban_(film)\" target=\"_blank\">https://fr.wikipedia.org/wiki/Harry_Potter_et_le_Prisonnier_d%27Azkaban_(film)</a></p>\r\n', '', 'tt0304141', 'Harry Potter and the Prisoner of Azkaban', 0),
(115, 136190, 'iron-man-2', '2014-07-07 04:33:02', '2020-01-13 23:42:00', 'Iron Man 2', '<p><a href=\"https://en.wikipedia.org/wiki/Iron_Man_2\" target=\"_blank\">https://en.wikipedia.org/wiki/Iron_Man_2</a></p>\r\n', '', 'tt1228705', NULL, 2010),
(116, 139589, 'iron-man-3', '2014-07-07 04:35:29', '2020-01-13 23:43:10', 'Iron Man 3', '<p><a href=\"https://en.wikipedia.org/wiki/Iron_Man_3\" target=\"_blank\">https://en.wikipedia.org/wiki/Iron_Man_3</a></p>\r\n', '', 'tt1300854', NULL, 2013),
(117, 53751, 'iron-man', '2014-07-03 07:46:20', '2020-01-13 23:44:25', 'Iron Man', '<p><a href=\"https://en.wikipedia.org/wiki/Iron_Man_(2008_film)\" target=\"_blank\">https://en.wikipedia.org/wiki/Iron_Man_(2008_film)</a></p>\r\n', '', 'tt0371746', NULL, 2008),
(120, 220848, 'la-ligue-des-justiciers-le-paradoxe-flashpoint', '2014-07-07 04:41:05', '2020-01-13 23:45:13', 'La Ligue des Justiciers : Le Paradoxe Flashpoint', '<p><a href=\"https://fr.wikipedia.org/wiki/La_Ligue_des_justiciers_:_Le_Paradoxe_Flashpoint\" target=\"_blank\">https://fr.wikipedia.org/wiki/La_Ligue_des_justiciers_:_Le_Paradoxe_Flashpoint</a></p>\r\n', 'La Ligue des Justiciers : Paradoxe temporel', 'tt2820466', 'Justice League: The Flashpoint Paradox', 2013),
(122, 59194, 'kickboxer', '2014-07-07 04:52:33', '2020-01-13 23:46:39', 'Kickboxer', '<p><a href=\"https://en.wikipedia.org/wiki/Kickboxer_(1989_film)\" target=\"_blank\">https://en.wikipedia.org/wiki/Kickboxer_(1989_film)</a></p>\r\n', '', 'tt0097659', NULL, 1989),
(123, 46718, 'king-kong', '2014-07-07 04:54:36', '2020-01-13 23:48:07', 'King Kong', '<p><a href=\"https://fr.wikipedia.org/wiki/King_Kong_(film,_2005)\" target=\"_blank\">https://fr.wikipedia.org/wiki/King_Kong_(film,_2005)</a></p>\r\n', '', 'tt0360717', NULL, 2005),
(124, 54205, 'kingdom-of-heaven', '2014-07-03 07:46:42', '2020-01-13 23:49:21', 'Kingdom of Heaven', '<p><a href=\"https://fr.wikipedia.org/wiki/Kingdom_of_Heaven\" target=\"_blank\">https://fr.wikipedia.org/wiki/Kingdom_of_Heaven</a></p>\r\n', '', 'tt0320661', NULL, 2005),
(129, 35784, 'l-age-de-glace', '2014-07-07 05:02:08', '2020-01-13 23:51:03', 'Ice Age', '<p><a href=\"https://fr.wikipedia.org/wiki/L%27%C3%82ge_de_glace\" target=\"_blank\">https://fr.wikipedia.org/wiki/L%27%C3%82ge_de_glace</a></p>\r\n', '', 'tt0268380', NULL, 0),
(131, 114820, 'l-incroyable-hulk', '2014-07-07 05:07:37', '2020-01-13 23:52:35', 'L\'Incroyable Hulk', '<p><a href=\"https://fr.wikipedia.org/wiki/L%27Incroyable_Hulk_(film)\" target=\"_blank\">https://fr.wikipedia.org/wiki/L%27Incroyable_Hulk_(film)</a></p>\r\n', '', 'tt0800080', 'The Incredible Hulk', 2008),
(142, 0, 'la-finale-historique-france-bresil-du-12-juillet-98', '2014-07-07 17:25:35', '2020-01-15 21:38:11', 'La Finale Historique France/Brésil du 12 juillet 98', NULL, '', NULL, NULL, 2006),
(146, 208389, 'justice-league-doom', '2014-07-07 17:34:43', '2020-01-13 23:54:36', 'La Ligue des Justiciers : Échec', '<p><a href=\"https://fr.wikipedia.org/wiki/La_Ligue_des_justiciers_:_%C3%89chec\" target=\"_blank\">https://fr.wikipedia.org/wiki/La_Ligue_des_justiciers_:_%C3%89chec</a></p>\r\n', 'La Ligue des Justiciers : Echec', 'tt2027128', 'Justice League: Doom', 2012),
(156, 29104, 'le-boulet', '2014-07-03 07:47:56', '2020-01-13 23:56:02', 'Le Boulet', '<p><a href=\"https://fr.wikipedia.org/wiki/Le_Boulet\" target=\"_blank\">https://fr.wikipedia.org/wiki/Le_Boulet</a></p>\r\n', '', 'tt0283957', NULL, 2002),
(171, 27764, 'le-pacte-des-loups', '2014-07-03 07:48:24', '2020-01-13 23:57:36', 'Le Pacte des loups', '<p><a href=\"https://fr.wikipedia.org/wiki/Le_Pacte_des_loups\" target=\"_blank\">https://fr.wikipedia.org/wiki/Le_Pacte_des_loups</a></p>\r\n', '', 'tt0237534', NULL, 2001),
(175, 50356, 'le-roi-arthur', '2014-07-03 07:48:48', '2020-01-13 23:59:14', 'Le Roi Arthur', '<p><a href=\"https://fr.wikipedia.org/wiki/Le_Roi_Arthur\" target=\"_blank\">https://fr.wikipedia.org/wiki/Le_Roi_Arthur</a></p>\r\n', '', 'tt0349683', 'King Arthur', 2004),
(179, 29140, 'le-roi-scorpion', '2014-07-03 07:48:51', '2020-01-14 00:00:57', 'Le Roi Scorpion', '<p><a href=\"https://fr.wikipedia.org/wiki/Le_Roi_Scorpion\" target=\"_blank\">https://fr.wikipedia.org/wiki/Le_Roi_Scorpion</a></p>\r\n', '', 'tt0277296', 'The Scorpion King', 2002),
(181, 27070, 'le-seigneur-des-anneaux-la-communaute-de-l-anneau', '2014-07-07 07:38:34', '2020-01-14 00:02:24', 'Le Seigneur des Anneaux : La Communauté de l\'Anneau', '<p><a href=\"https://fr.wikipedia.org/wiki/Le_Seigneur_des_anneaux_:_La_Communaut%C3%A9_de_l%27anneau\" target=\"_blank\">https://fr.wikipedia.org/wiki/Le_Seigneur_des_anneaux_:_La_Communaut%C3%A9_de_l%27anneau</a></p>\r\n', '', 'tt0120737', 'The Lord of the Rings: The Fellowship of the Ring', 2001),
(182, 39187, 'le-seigneur-des-anneaux-le-retour-du-roi', '2014-07-03 07:49:04', '2020-01-14 00:04:20', 'Le Seigneur des Anneaux : Le Retour du Roi', '<p><a href=\"https://fr.wikipedia.org/wiki/Le_Seigneur_des_anneaux_:_Le_Retour_du_roi\" target=\"_blank\">https://fr.wikipedia.org/wiki/Le_Seigneur_des_anneaux_:_Le_Retour_du_roi</a></p>\r\n', '', 'tt0167260', 'The Lord of the Rings: The Return of the King', 2003),
(183, 39186, 'le-seigneur-des-anneaux-les-deux-tours', '2014-07-03 07:49:06', '2020-01-14 00:05:40', 'Le Seigneur des Anneaux : Les Deux Tours', '<p><a href=\"https://fr.wikipedia.org/wiki/Le_Seigneur_des_anneaux_:_Les_Deux_Tours\" target=\"_blank\">https://fr.wikipedia.org/wiki/Le_Seigneur_des_anneaux_:_Les_Deux_Tours</a></p>\r\n', '', 'tt0167261', 'The Lord of the Rings: The Two Towers', 2002),
(186, 57125, 'le-transporteur-2', '2014-07-03 07:49:10', '2020-01-15 21:36:03', 'Le Transporteur 2', '<p><a href=\"https://fr.wikipedia.org/wiki/Le_Transporteur_2\" target=\"_blank\">https://fr.wikipedia.org/wiki/Le_Transporteur_2</a></p>\r\n', '', 'tt0388482', 'Transporter 2', 2005),
(188, 0, 'les-100-plus-beaux-buts-de-l-ol', '2014-07-07 19:46:17', '2020-01-15 21:37:13', 'Les 100 plus Beaux Buts de l\'OL', NULL, '', NULL, NULL, 2006),
(215, 123348, 'man-of-steel', '2014-07-03 07:50:07', '2020-01-15 21:40:36', 'Man of Steel', '<p><a href=\"https://fr.wikipedia.org/wiki/Man_of_Steel\" target=\"_blank\">https://fr.wikipedia.org/wiki/Man_of_Steel</a></p>\r\n', NULL, 'tt0770828', NULL, 2013),
(225, 29274, 'meurs-un-autre-jour', '2014-07-03 07:50:27', '2020-01-15 21:42:31', 'Meurs un autre jour', '<p><a href=\"https://fr.wikipedia.org/wiki/Meurs_un_autre_jour\" target=\"_blank\">https://fr.wikipedia.org/wiki/Meurs_un_autre_jour</a></p>\r\n', '', 'tt0246460', 'Die Another Day', 2002),
(226, 34437, 'miami-vice-deux-flics-a-miami', '2014-07-03 07:50:31', '2020-01-15 21:44:31', 'Miami vice - Deux flics à Miami', '<p><a href=\"https://fr.wikipedia.org/wiki/Miami_Vice_:_Deux_Flics_%C3%A0_Miami\" target=\"_blank\">https://fr.wikipedia.org/wiki/Miami_Vice_:_Deux_Flics_%C3%A0_Miami</a></p>\r\n', '', 'tt0430357', 'Miami Vice', 2006),
(249, 46117, 'pirates-des-caraibes-la-malediction-du-black-pearl', '2014-07-03 07:52:33', '2020-01-15 21:47:35', 'Pirates des Caraïbes : La Malédiction du Black Pearl', '<p><a href=\"https://fr.wikipedia.org/wiki/Pirates_des_Cara%C3%AFbes_:_La_Mal%C3%A9diction_du_Black_Pearl\" target=\"_blank\">https://fr.wikipedia.org/wiki/Pirates_des_Cara%C3%AFbes_:_La_Mal%C3%A9diction_du_Black_Pearl</a></p>\r\n', '', 'tt0325980', 'Pirates of the Caribbean: The Curse of the Black Pearl', 2003),
(276, 41332, 'self-control', '2014-07-03 07:53:28', '2020-01-15 21:49:25', 'Self Control', '<p><a href=\"https://fr.wikipedia.org/wiki/Self_Control_(film)\" target=\"_blank\">https://fr.wikipedia.org/wiki/Self_Control_(film)</a></p>\r\n', '', 'tt0305224', 'Anger Management', 2003),
(293, 20754, 'star-wars-episode-i-la-menace-fantome', '2014-07-03 07:54:01', '2020-01-15 21:51:25', 'Star Wars : Épisode I - La Menace Fantôme', '<p><a href=\"https://fr.wikipedia.org/wiki/Star_Wars,_%C3%A9pisode_I_:_La_Menace_fant%C3%B4me\" target=\"_blank\">https://fr.wikipedia.org/wiki/Star_Wars,_%C3%A9pisode_I_:_La_Menace_fant%C3%B4me</a></p>\r\n', '', 'tt0120915', 'Star Wars: Episode I - The Phantom Menace', 1999),
(294, 29014, 'star-wars-episode-ii-l-attaque-des-clones', '2014-07-07 09:54:38', '2020-01-15 21:53:14', 'Star Wars : Épisode II - L\'Attaque des Clones', '<p><a href=\"https://fr.wikipedia.org/wiki/Star_Wars,_%C3%A9pisode_II_:_L%27Attaque_des_clones\" target=\"_blank\">https://fr.wikipedia.org/wiki/Star_Wars,_%C3%A9pisode_II_:_L%27Attaque_des_clones</a></p>\r\n', '', 'tt0121765', 'Star Wars: Episode II - Attack of the Clones', 2002),
(295, 40623, 'star-wars-episode-iii-la-revanche-des-sith', '2014-07-03 07:54:03', '2020-01-15 22:04:54', 'Star Wars : Épisode III - La Revanche des Sith', '<p><a href=\"https://fr.wikipedia.org/wiki/Star_Wars,_%C3%A9pisode_III_:_La_Revanche_des_Sith\" target=\"_blank\">https://fr.wikipedia.org/wiki/Star_Wars,_%C3%A9pisode_III_:_La_Revanche_des_Sith</a></p>\r\n', '', 'tt0121766', 'Star Wars: Episode III - Revenge of the Sith', 2005),
(296, 25801, 'star-wars-episode-iv-un-nouvel-espoir', '2014-07-03 07:54:04', '2020-01-15 22:04:32', 'Star Wars : Épisode IV - Un Nouvel Espoir', '<p><a href=\"https://fr.wikipedia.org/wiki/Star_Wars,_%C3%A9pisode_IV_:_Un_nouvel_espoir\" target=\"_blank\">https://fr.wikipedia.org/wiki/Star_Wars,_%C3%A9pisode_IV_:_Un_nouvel_espoir</a></p>\r\n', '', 'tt0076759', 'Star Wars: Episode IV - A New Hope', 1977),
(297, 25802, 'star-wars-episode-v-l-empire-contre-attaque', '2014-07-07 09:51:28', '2020-01-15 22:05:19', 'Star Wars : Épisode V - L\'Empire Contre-Attaque', '<p><a href=\"https://fr.wikipedia.org/wiki/Star_Wars,_%C3%A9pisode_V_:_L%27Empire_contre-attaque\" target=\"_blank\">https://fr.wikipedia.org/wiki/Star_Wars,_%C3%A9pisode_V_:_L%27Empire_contre-attaque</a></p>\r\n', '', 'tt0080684', 'Star Wars: Episode V - The Empire Strikes Back', 1980),
(298, 25803, 'star-wars-episode-vi-le-retour-du-jedi', '2014-07-03 07:54:05', '2020-01-15 22:04:10', 'Star Wars : Épisode VI - Le Retour du Jedi', '<p><a href=\"https://fr.wikipedia.org/wiki/Star_Wars,_%C3%A9pisode_VI_:_Le_Retour_du_Jedi\" target=\"_blank\">https://fr.wikipedia.org/wiki/Star_Wars,_%C3%A9pisode_VI_:_Le_Retour_du_Jedi</a></p>\r\n', '', 'tt0086190', 'Star Wars: Episode VI - Return of the Jedi', 1983),
(319, 132874, 'the-dark-knight-rises', '2014-07-03 07:54:36', '2020-01-15 22:07:00', 'The Dark Knight Rises', '<p><a href=\"https://fr.wikipedia.org/wiki/The_Dark_Knight_Rises\" target=\"_blank\">https://fr.wikipedia.org/wiki/The_Dark_Knight_Rises</a></p>\r\n', '', 'tt1345836', NULL, 2012),
(320, 115362, 'the-dark-knight-le-chevalier-noir', '2014-07-03 07:54:37', '2020-01-15 22:08:57', 'The Dark Knight, Le Chevalier Noir', '<p><a href=\"https://fr.wikipedia.org/wiki/The_Dark_Knight_:_Le_Chevalier_noir\" target=\"_blank\">https://fr.wikipedia.org/wiki/The_Dark_Knight_:_Le_Chevalier_noir</a></p>\r\n', '', 'tt0468569', 'The Dark Knight', 2008),
(324, 193108, 'thor-le-monde-des-tenebres', '2014-07-07 10:27:36', '2020-01-15 22:10:04', 'Thor : Le Monde des Ténèbres', '<p><a href=\"https://fr.wikipedia.org/wiki/Thor_:_Le_Monde_des_t%C3%A9n%C3%A8bres\" target=\"_blank\">https://fr.wikipedia.org/wiki/Thor_:_Le_Monde_des_t%C3%A9n%C3%A8bres</a></p>\r\n', '', 'tt1981115', 'Thor: The Dark World', 2013),
(325, 129477, 'thor', '2014-07-03 07:55:13', '2020-01-15 22:11:12', 'Thor', '<p><a href=\"https://fr.wikipedia.org/wiki/Thor_(film)\" target=\"_blank\">https://fr.wikipedia.org/wiki/Thor_(film)</a></p>\r\n', '', 'tt0800369', NULL, 2011),
(332, 47357, 'troie', '2014-07-03 07:55:29', '2020-01-15 22:13:30', 'Troie', '<p><a href=\"https://fr.wikipedia.org/wiki/Troie_(film)\" target=\"_blank\">https://fr.wikipedia.org/wiki/Troie_(film)</a></p>\r\n', '', 'tt0332452', 'Troy', 2004),
(333, 57555, 'two-for-the-money', '2014-07-03 07:55:30', '2020-01-15 22:14:59', 'Two for the Money', NULL, '', 'tt0417217', NULL, 2005),
(340, 57769, 'watchmen-les-gardiens', '2014-07-07 10:44:42', '2020-01-15 22:17:13', 'Watchmen - Les Gardiens', '<p><a href=\"https://fr.wikipedia.org/wiki/Watchmen_:_Les_Gardiens\" target=\"_blank\">https://fr.wikipedia.org/wiki/Watchmen_:_Les_Gardiens</a></p>\r\n', '', 'tt0409459', 'Watchmen', 2009),
(345, 146326, 'wolverine-le-combat-de-l-immortel', '2014-07-07 10:55:46', '2020-01-15 22:20:11', 'Wolverine : Le Combat de l\'Immortel', '<p><a href=\"https://fr.wikipedia.org/wiki/Wolverine_:_Le_Combat_de_l%27immortel\" target=\"_blank\">https://fr.wikipedia.org/wiki/Wolverine_:_Le_Combat_de_l%27immortel</a></p>\r\n', '', 'tt1430132', 'The Wolverine', 2013),
(349, 140894, 'x-men-le-commencement', '2014-07-03 07:56:00', '2020-01-15 22:22:33', 'X-Men : Le Commencement', '<p><a href=\"https://fr.wikipedia.org/wiki/X-Men_:_Le_Commencement\" target=\"_blank\">https://fr.wikipedia.org/wiki/X-Men_:_Le_Commencement</a></p>\r\n', '', 'tt1270798', 'X-Men: First Class', 2011),
(350, 111544, 'x-men-origins-wolverine', '2014-07-07 11:01:39', '2020-01-15 22:49:57', 'X-Men Origins: Wolverine', '<p><a href=\"https://fr.wikipedia.org/wiki/X-Men_Origins:_Wolverine\" target=\"_blank\">https://fr.wikipedia.org/wiki/X-Men_Origins:_Wolverine</a></p>\r\n', '', 'tt0458525', NULL, 2009),
(353, 0, 'zinedine-zidane-comme-dans-un-reve', '2014-07-07 23:06:27', '2020-01-15 22:26:58', 'Zinédine Zidane : Comme dans un Rêve', NULL, '', NULL, NULL, 2002),
(356, 0, 'olympique-lyonnais-champion-2004', '2014-07-09 15:41:23', '2020-01-15 22:28:04', 'Olympique Lyonnais, Champion 2004', NULL, NULL, NULL, NULL, 2004),
(361, 0, 'ivc-10', '2014-08-07 15:52:17', '2020-01-15 22:30:17', 'IVC 10', NULL, NULL, NULL, NULL, 0),
(362, 0, 'wwe-dx-vs-the-mcmahons', '2014-08-07 16:04:17', '2020-01-15 22:31:57', 'WWE DX vs The McMahons', NULL, '', NULL, NULL, 0),
(364, 0, 'wwe-cyber-sunday-2006', '2014-08-07 16:20:07', '2020-01-15 22:32:51', 'WWE Cyber Sunday 2006', NULL, '', NULL, NULL, 0),
(365, 0, 'wwe-survivor-series-2006', '2014-08-07 16:23:19', '2020-01-15 22:33:27', 'WWE Survivor Series 2006', NULL, '', NULL, NULL, 0),
(366, 0, 'wwe-ecw-december-to-dismember-2006', '2014-08-07 16:26:44', '2020-01-15 22:34:13', 'WWE ECW December to Dismember 2006', NULL, '', NULL, NULL, 0),
(367, 0, 'wwe-judgment-day-2007', '2014-08-07 16:30:17', '2020-01-15 22:35:17', 'WWE Judgment Day 2007', NULL, '', NULL, NULL, 0),
(368, 0, 'wwe-the-great-american-bash-2007', '2014-08-07 16:35:27', '2020-01-15 22:35:51', 'WWE The Great American Bash 2007', NULL, '', NULL, NULL, 0),
(369, 0, 'wwe-summerslam-2006', '2014-08-07 16:37:51', '2020-01-15 22:37:11', 'WWE SummerSlam 2006', NULL, '', NULL, NULL, 0),
(371, 222947, 'batman-assaut-sur-arkham', '2014-08-14 09:19:44', '2020-01-15 22:39:38', 'Batman : Assaut sur Arkham', '<p><a href=\"https://fr.wikipedia.org/wiki/Batman_:_Assaut_sur_Arkham\" target=\"_blank\">https://fr.wikipedia.org/wiki/Batman_:_Assaut_sur_Arkham</a></p>\r\n', '', 'tt3139086', 'Batman: Assault on Arkham', 2014),
(372, 0, 'best-of-wwe-smackdown-vs-raw', '2014-08-14 23:30:14', '2020-01-15 22:40:28', 'WWE Best of SmackDown vs Raw', NULL, NULL, NULL, NULL, 0),
(373, 129815, 'machete', '2014-09-08 03:48:30', '2020-01-15 22:41:55', 'Machete', '<p><a href=\"https://fr.wikipedia.org/wiki/Machete\" target=\"_blank\">https://fr.wikipedia.org/wiki/Machete</a></p>\r\n', '', 'tt0985694', NULL, 2010),
(374, 0, 'les-premiers-pas-de-vos-stars-preferees', '2014-09-08 16:39:20', '2020-01-15 22:42:48', 'Les Premiers Pas de vos Stars Préférées', NULL, NULL, NULL, NULL, 0),
(375, 0, 'the-twisted-disturbed-life-of-kane', '2014-09-08 16:46:17', '2020-01-15 22:43:28', 'WWE The Twisted, Disturbed Life of Kane', NULL, NULL, NULL, NULL, 0),
(382, 172170, 'batman-la-releve-le-retour-du-joker', '2015-03-16 08:06:27', '2020-01-15 22:44:01', 'Batman, la relève : Le Retour du Joker', '<p><a href=\"https://fr.wikipedia.org/wiki/Batman,_la_rel%C3%A8ve_:_Le_Retour_du_Joker\" target=\"_blank\">https://fr.wikipedia.org/wiki/Batman,_la_rel%C3%A8ve_:_Le_Retour_du_Joker</a></p>\r\n', 'Batman, la Relève : Le Retour du Joker', 'tt0233298', 'Batman Beyond: Return of the Joker', 2000),
(383, 193113, 'captain-america-le-soldat-de-l-hiver', '2015-03-16 09:08:20', '2020-01-15 22:45:47', 'Captain America : Le Soldat de l\'Hiver', '<p><a href=\"https://fr.wikipedia.org/wiki/Captain_America_:_Le_Soldat_de_l%27hiver\" target=\"_blank\">https://fr.wikipedia.org/wiki/Captain_America_:_Le_Soldat_de_l%27hiver</a></p>\r\n', '', 'tt1843866', 'Captain America: The Winter Soldier', 2014),
(384, 196604, 'les-gardiens-de-la-galaxie', '2015-03-16 09:13:47', '2020-01-15 22:46:21', 'Les Gardiens de la Galaxie', '<p><a href=\"https://fr.wikipedia.org/wiki/Les_Gardiens_de_la_Galaxie_(film)\" target=\"_blank\">https://fr.wikipedia.org/wiki/Les_Gardiens_de_la_Galaxie_(film)</a></p>\r\n', '', 'tt2015381', 'Guardians of the Galaxy', 2014),
(410, 53756, 'harry-potter-et-la-coupe-de-feu', '2015-11-21 13:12:15', '2020-01-15 22:48:49', 'Harry Potter et la Coupe de Feu', '<p><a href=\"https://fr.wikipedia.org/wiki/Harry_Potter_et_la_Coupe_de_feu_(film)\" target=\"_blank\">https://fr.wikipedia.org/wiki/Harry_Potter_et_la_Coupe_de_feu_(film)</a></p>\r\n', '', 'tt0330373', 'Harry Potter and the Goblet of Fire', 2005),
(411, 219262, 'batman-v-superman-l-aube-de-la-justice', '2016-07-31 10:42:55', '2020-01-15 22:49:23', 'Batman v Superman : L\'Aube de la justice', '<p><a href=\"https://fr.wikipedia.org/wiki/Batman_v_Superman_:_L%27Aube_de_la_justice\" target=\"_blank\">https://fr.wikipedia.org/wiki/Batman_v_Superman_:_L%27Aube_de_la_justice</a></p>\r\n', 'Batman v Superman : L\'Aube de la Justice', 'tt2975590', 'Batman v Superman: Dawn of Justice', 2016),
(412, 239324, 'batman-the-killing-joke', '2016-08-02 08:43:32', '2020-01-15 22:51:03', 'Batman: The Killing Joke', '<p><a href=\"https://fr.wikipedia.org/wiki/Batman:_The_Killing_Joke_(film)\" target=\"_blank\">https://fr.wikipedia.org/wiki/Batman:_The_Killing_Joke_(film)</a></p>\r\n', '', 'tt4853102', NULL, 2016),
(414, 9196, 'batman-1966', '2016-08-02 21:27:00', '2020-01-16 08:52:42', 'Batman (1966)', '<p><a href=\"https://fr.wikipedia.org/wiki/Batman_(film,_1966)\" target=\"_blank\">https://fr.wikipedia.org/wiki/Batman_(film,_1966)</a></p>\r\n', 'Batman', 'tt0060153', 'Batman The Movie', 1966),
(424, 173720, 'wonder-woman-2017', '2019-05-13 10:59:10', '2020-01-15 22:53:09', 'Wonder Woman', '<p><a href=\"https://fr.wikipedia.org/wiki/Wonder_Woman_(film,_2017)\" target=\"_blank\">https://fr.wikipedia.org/wiki/Wonder_Woman_(film,_2017)</a></p>\r\n', NULL, 'tt0451279', NULL, 2017),
(425, 144185, 'suicide-squad-2016', '2019-05-13 11:05:14', '2020-01-15 22:54:21', 'Suicide Squad', '<p><a href=\"https://fr.wikipedia.org/wiki/Suicide_Squad_(film)\" target=\"_blank\">https://fr.wikipedia.org/wiki/Suicide_Squad_(film)</a></p>\r\n', NULL, 'tt1386697', NULL, 2016),
(426, 126527, 'justice-league-2017', '2019-05-13 11:08:18', '2020-01-15 22:55:32', 'Justice League', '<p><a href=\"https://fr.wikipedia.org/wiki/Justice_League_(film)\" target=\"_blank\">https://fr.wikipedia.org/wiki/Justice_League_(film)</a></p>\r\n', NULL, 'tt0974015', NULL, 2017),
(427, 38512, 'le-seigneur-des-anneaux', '2019-05-27 18:09:43', '2020-01-15 22:56:12', 'Le Seigneur des Anneaux', '<p><a href=\"https://fr.wikipedia.org/wiki/Le_Seigneur_des_anneaux_(film,_1978)\" target=\"_blank\">https://fr.wikipedia.org/wiki/Le_Seigneur_des_anneaux_(film,_1978)</a></p>\r\n', NULL, 'tt0077869', 'The Lord of the Rings', 1978),
(429, 215097, 'star-wars-episode-vii-le-reveil-de-la-force', '2019-05-27 18:26:15', '2020-01-15 22:56:44', 'Star Wars, épisode VII : Le Réveil de la Force', '<p><a href=\"https://fr.wikipedia.org/wiki/Star_Wars,_%C3%A9pisode_VII_:_Le_R%C3%A9veil_de_la_Force\" target=\"_blank\">https://fr.wikipedia.org/wiki/Star_Wars,_%C3%A9pisode_VII_:_Le_R%C3%A9veil_de_la_Force</a></p>\r\n', NULL, 'tt2488496', 'Star Wars: Episode VII - The Force Awakens', 2015),
(430, 146349, 'deadpool', '2019-05-27 18:29:03', '2020-01-15 22:57:15', 'Deadpool', '<p><a href=\"https://fr.wikipedia.org/wiki/Deadpool_(film)\" target=\"_blank\">https://fr.wikipedia.org/wiki/Deadpool_(film)</a></p>\r\n', NULL, 'tt1431045', NULL, 2016),
(431, 218395, 'rogue-one-a-star-wars-story', '2019-05-27 18:33:07', '2020-01-15 22:57:47', 'Rogue One: A Star Wars Story', '<p><a href=\"https://fr.wikipedia.org/wiki/Rogue_One:_A_Star_Wars_Story\" target=\"_blank\">https://fr.wikipedia.org/wiki/Rogue_One:_A_Star_Wars_Story</a></p>\r\n', NULL, 'tt3748528', NULL, 2016),
(432, 198488, 'avengers-l-ere-d-ultron', '2019-05-27 18:35:33', '2020-01-15 22:58:24', 'Avengers : L\'Ère d\'Ultron', '<p><a href=\"https://fr.wikipedia.org/wiki/Avengers_:_L%27%C3%88re_d%27Ultron\" target=\"_blank\">https://fr.wikipedia.org/wiki/Avengers_:_L%27%C3%88re_d%27Ultron</a></p>\r\n', NULL, 'tt2395427', 'Avengers: Age of Ultron', 2015),
(433, 215099, 'star-wars-episode-viii-les-derniers-jedi', '2019-05-27 18:37:44', '2020-01-15 22:59:14', 'Star Wars, épisode VIII : Les Derniers Jedi', '<p><a href=\"https://fr.wikipedia.org/wiki/Star_Wars,_%C3%A9pisode_VIII_:_Les_Derniers_Jedi\" target=\"_blank\">https://fr.wikipedia.org/wiki/Star_Wars,_%C3%A9pisode_VIII_:_Les_Derniers_Jedi</a></p>\r\n', NULL, 'tt2527336', 'Star Wars: The Last Jedi', 2017),
(435, 215143, 'captain-america-civil-war', '2019-05-27 18:47:24', '2020-01-15 23:00:25', 'Captain America: Civil War', '<p><a href=\"https://fr.wikipedia.org/wiki/Captain_America:_Civil_War\" target=\"_blank\">https://fr.wikipedia.org/wiki/Captain_America:_Civil_War</a></p>\r\n', NULL, 'tt3498820', NULL, 2016),
(436, 225116, 'logan', '2019-05-27 18:55:27', '2020-01-15 23:01:00', 'Logan', '<p><a href=\"https://fr.wikipedia.org/wiki/Logan_(film)\" target=\"_blank\">https://fr.wikipedia.org/wiki/Logan_(film)</a></p>\r\n', NULL, 'tt3315342', NULL, 2017),
(437, 226995, 'les-gardiens-de-la-galaxie-vol-2', '2019-05-27 18:58:09', '2020-01-15 23:01:28', 'Les Gardiens de la Galaxie Vol. 2', '<p><a href=\"https://fr.wikipedia.org/wiki/Les_Gardiens_de_la_Galaxie_Vol._2\" target=\"_blank\">https://fr.wikipedia.org/wiki/Les_Gardiens_de_la_Galaxie_Vol._2</a></p>\r\n', NULL, 'tt3896198', 'Guardians of the Galaxy Vol. 2', 2017),
(438, 130438, 'ant-man', '2019-05-27 18:59:55', '2020-01-15 23:01:59', 'Ant-Man', '<p><a href=\"https://fr.wikipedia.org/wiki/Ant-Man_(film)\" target=\"_blank\">https://fr.wikipedia.org/wiki/Ant-Man_(film)</a></p>\r\n', NULL, 'tt0478970', NULL, 2015),
(439, 130533, 'doctor-strange', '2019-05-27 19:01:33', '2020-01-15 23:02:31', 'Doctor Strange', '<p><a href=\"https://fr.wikipedia.org/wiki/Doctor_Strange_(film)\" target=\"_blank\">https://fr.wikipedia.org/wiki/Doctor_Strange_(film)</a></p>\r\n', NULL, 'tt1211837', NULL, 2016),
(440, 223252, 'thor-ragnarok', '2019-05-27 19:03:16', '2020-01-15 23:03:02', 'Thor : Ragnarok', '<p><a href=\"https://fr.wikipedia.org/wiki/Thor_:_Ragnarok\" target=\"_blank\">https://fr.wikipedia.org/wiki/Thor_:_Ragnarok</a></p>\r\n', NULL, 'tt3501632', NULL, 2017),
(441, 208692, 'aquaman', '2019-06-11 10:46:12', '2020-01-15 23:03:35', 'Aquaman', '<p><a href=\"https://fr.wikipedia.org/wiki/Aquaman_(film)\" target=\"_blank\">https://fr.wikipedia.org/wiki/Aquaman_(film)</a></p>\r\n', NULL, 'tt1477834', NULL, 2018),
(442, 52644, 'dodgeball-meme-pas-mal', '2019-06-14 15:52:10', '2020-05-31 19:56:04', 'Dodgeball ! Même pas mal !', '<p><a href=\"https://fr.wikipedia.org/wiki/Dodgeball_!_M%C3%AAme_pas_mal_!\" target=\"_blank\">https://fr.wikipedia.org/wiki/Dodgeball_!_M%C3%AAme_pas_mal_!</a></p>\r\n', NULL, 'tt0364725', 'Dodgeball: A True Underdog Story', 2004),
(443, 0, 'franck-dubosc-romantique', '2019-06-14 15:55:19', '2020-01-15 23:06:55', 'Franck Dubosc Romantique', '<p><a href=\"https://fr.wikipedia.org/wiki/Romantique_(Franck_Dubosc)\" target=\"_blank\">https://fr.wikipedia.org/wiki/Romantique_(Franck_Dubosc)</a></p>\r\n', NULL, NULL, NULL, 2005),
(444, 218386, 'solo-a-star-wars-story', '2020-01-15 23:19:33', '2020-01-15 23:19:33', 'Solo: A Star Wars Story', '<p><a href=\"https://fr.wikipedia.org/wiki/Solo:_A_Star_Wars_Story\" target=\"_blank\">https://fr.wikipedia.org/wiki/Solo:_A_Star_Wars_Story</a></p>\r\n', NULL, 'tt3778644', NULL, 2018),
(445, 245006, 'deadpool-2', '2020-01-15 23:22:34', '2020-01-15 23:22:34', 'Deadpool 2', '<p><a href=\"https://fr.wikipedia.org/wiki/Deadpool_2\" target=\"_blank\">https://fr.wikipedia.org/wiki/Deadpool_2</a></p>\r\n', NULL, 'tt5463162', NULL, 2018),
(446, 209778, 'spider-man-homecoming', '2020-01-15 23:27:20', '2020-01-15 23:28:59', 'Spider-Man: Homecoming', '<p><a href=\"https://fr.wikipedia.org/wiki/Spider-Man:_Homecoming\" target=\"_blank\">https://fr.wikipedia.org/wiki/Spider-Man:_Homecoming</a></p>\r\n', NULL, 'tt2250912', NULL, 2017),
(447, 130336, 'black-panther', '2020-01-15 23:30:23', '2020-01-15 23:32:07', 'Black Panther', '<p><a href=\"https://fr.wikipedia.org/wiki/Black_Panther_(film)\" target=\"_blank\">https://fr.wikipedia.org/wiki/Black_Panther_(film)</a></p>\r\n', NULL, 'tt1825683', NULL, 2018),
(448, 218265, 'avengers-infinity-war', '2020-01-15 23:34:37', '2020-01-15 23:34:37', 'Avengers: Infinity War', '<p><a href=\"https://fr.wikipedia.org/wiki/Avengers:_Infinity_War\" target=\"_blank\">https://fr.wikipedia.org/wiki/Avengers:_Infinity_War</a></p>\r\n', NULL, 'tt4154756', NULL, 2018),
(449, 249074, 'batman-et-harley-quinn', '2020-01-15 23:38:41', '2020-01-15 23:38:41', 'Batman et Harley Quinn', NULL, NULL, 'tt6556890', NULL, 2017),
(450, 226391, 'ant-man-et-la-guepe', '2020-01-15 23:41:11', '2020-01-15 23:41:11', 'Ant-Man et la Guêpe', '<p><a href=\"https://fr.wikipedia.org/wiki/Ant-Man_et_la_Gu%C3%AApe\" target=\"_blank\">https://fr.wikipedia.org/wiki/Ant-Man_et_la_Gu%C3%AApe</a></p>\r\n', NULL, 'tt5095030', 'Ant-Man and the Wasp', 2018),
(451, 141110, 'captain-marvel', '2020-01-15 23:43:13', '2020-01-15 23:43:13', 'Captain Marvel', '<p><a href=\"https://fr.wikipedia.org/wiki/Captain_Marvel_(film)\" target=\"_blank\">https://fr.wikipedia.org/wiki/Captain_Marvel_(film)</a></p>\r\n', NULL, 'tt4154664', NULL, 2019),
(452, 232669, 'avengers-endgame', '2020-01-15 23:45:06', '2020-01-15 23:45:06', 'Avengers: Endgame', '<p><a href=\"https://fr.wikipedia.org/wiki/Avengers:_Endgame\" target=\"_blank\">https://fr.wikipedia.org/wiki/Avengers:_Endgame</a></p>\r\n', NULL, 'tt4154796', NULL, 2019),
(453, 222291, 'spider-man-far-from-home', '2020-01-15 23:46:56', '2020-01-15 23:46:56', 'Spider-Man: Far From Home', '<p><a href=\"https://fr.wikipedia.org/wiki/Spider-Man:_Far_From_Home\" target=\"_blank\">https://fr.wikipedia.org/wiki/Spider-Man:_Far_From_Home</a></p>\r\n', NULL, 'tt6320628', NULL, 2019),
(454, 57825, 'shaun-of-the-dead', '2020-01-17 00:09:10', '2020-01-17 00:09:10', 'Shaun of the Dead', '<p><a href=\"https://fr.wikipedia.org/wiki/Shaun_of_the_Dead\" target=\"_blank\">https://fr.wikipedia.org/wiki/Shaun_of_the_Dead</a></p>\r\n', NULL, 'tt0365748', NULL, 2004),
(455, 0, 'joker', '2020-05-27 22:27:32', '2020-10-13 21:32:12', 'Joker', NULL, NULL, NULL, NULL, 0),
(456, 0, 'spider-man-2002', '2020-05-28 23:02:12', '2020-10-13 21:32:49', 'Spider-Man', NULL, NULL, NULL, NULL, 0),
(457, 0, 'spider-man-2', '2020-05-28 23:02:34', '2020-10-13 21:33:08', 'Spider-Man 2', NULL, NULL, NULL, NULL, 0),
(458, 0, 'spider-man-3', '2020-05-28 23:02:47', '2020-10-13 21:33:39', 'Spider-Man 3', NULL, NULL, NULL, NULL, 0),
(459, 0, 'ace-ventura-detective-pour-chiens-et-chats', '2020-05-28 23:03:45', '2020-10-13 21:33:58', 'Ace Ventura : Détective pour chiens et chats', NULL, NULL, NULL, NULL, 0),
(460, 0, 'x-men-days-of-future-past', '2020-05-28 23:05:10', '2020-10-13 21:34:23', 'X-Men : Days of Future Past', NULL, NULL, NULL, NULL, 0),
(461, 0, 'ace-ventura-en-afrique', '2020-05-28 23:05:34', '2020-10-13 21:34:45', 'Ace Ventura en Afrique', NULL, NULL, NULL, NULL, 0),
(462, 0, 'le-hobbit-un-voyage-inattendu', '2020-05-28 23:07:37', '2020-10-13 21:35:10', 'Le Hobbit : Un voyage inattendu', NULL, NULL, NULL, NULL, 0),
(463, 0, 'le-hobbit-la-desolation-de-smaug', '2020-05-28 23:08:26', '2020-10-13 21:39:38', 'Le Hobbit : La désolation de Smaug', NULL, NULL, NULL, NULL, 0),
(464, 0, 'le-hobbit-la-bataille-des-cinq-armees', '2020-05-28 23:08:43', '2020-10-13 21:46:07', 'Le Hobbit : La bataille des Cinq Armées', NULL, NULL, NULL, NULL, 0),
(465, 0, 'film-300', '2020-05-28 23:09:06', '2020-10-13 21:46:22', '300', NULL, NULL, NULL, NULL, 0),
(466, 0, '300-la-naissance-d-un-empire', '2020-05-28 23:09:44', '2020-10-13 21:46:37', '300 : La Naissance d\'un Empire', NULL, NULL, NULL, NULL, 0),
(467, 0, 'batman-gotham-by-gaslight', '2020-05-28 23:11:35', '2020-10-13 21:47:00', 'Batman : Gotham by Gaslight', NULL, NULL, NULL, NULL, 0),
(468, 0, 'shazam', '2020-05-28 23:11:56', '2020-10-13 21:47:30', 'Shazam', NULL, NULL, NULL, NULL, 0),
(469, 0, 'bruce-tout-puissant', '2020-05-28 23:12:29', '2020-10-13 21:47:49', 'Bruce Tout-Puissant', NULL, NULL, NULL, NULL, 0),
(470, 0, 'charlie-et-la-chocolaterie', '2020-05-28 23:13:14', '2020-10-13 21:48:10', 'Charlie et la chocolaterie', NULL, NULL, NULL, NULL, 0),
(471, 0, 'la-ligue-des-gentlemen-extraordinaires', '2020-05-28 23:13:46', '2020-10-13 21:48:29', 'La Ligue des Gentlemen Extraordinaires', NULL, NULL, NULL, NULL, 0),
(472, 0, 'hancock', '2020-05-28 23:14:07', '2020-10-13 21:48:49', 'Hancock', NULL, NULL, NULL, NULL, 0),
(473, 0, 'mary-a-tout-prix', '2020-05-28 23:14:31', '2020-10-13 21:49:09', 'Mary à Tout Prix', NULL, NULL, NULL, NULL, 0),
(474, 0, 'le-pari', '2020-05-28 23:14:54', '2020-10-13 21:49:27', 'Le Pari', NULL, NULL, NULL, NULL, 0),
(475, 0, 'the-amazing-spider-man', '2020-05-28 23:17:07', '2020-10-13 21:49:50', 'The Amazing Spider-Man', NULL, NULL, NULL, NULL, 0),
(476, 0, 'the-amazing-spider-man-le-destin-d-un-heros', '2020-05-28 23:17:31', '2020-10-13 21:50:21', 'The Amazing Spider-Man : Le destin d\'un héros', NULL, NULL, NULL, NULL, 0),
(477, 0, 'le-masque-de-zorro', '2020-05-28 23:18:05', '2020-10-13 21:50:41', 'Le Masque de Zorro', NULL, NULL, NULL, NULL, 0),
(478, 0, 'la-tour-montparnasse-infernale', '2020-05-28 23:18:31', '2020-10-13 21:51:02', 'La Tour Montparnasse infernale', NULL, NULL, NULL, NULL, 0),
(479, 0, 'la-cite-de-la-peur', '2020-05-28 23:18:58', '2020-10-13 21:51:47', 'La Cité de la peur', NULL, NULL, NULL, NULL, 0),
(480, 0, 'les-4-fantastiques-et-le-surfer-d-argent', '2020-05-28 23:19:19', '2020-10-13 21:52:11', 'Les 4 Fantastiques et Le Surfer d\'argent', NULL, NULL, NULL, NULL, 0),
(481, 0, 'les-4-fantastiques', '2020-05-28 23:19:41', '2020-10-13 21:52:30', 'Les 4 Fantastiques', NULL, NULL, NULL, NULL, 0),
(482, 0, 'ali-g-indahouse', '2020-05-28 23:20:10', '2020-10-13 21:52:59', 'Ali G, Indahouse', NULL, NULL, NULL, NULL, 0),
(483, 0, 'x-men-apocalypse', '2020-05-28 23:20:29', '2020-10-13 21:53:22', 'X-Men : Apocalypse', NULL, NULL, NULL, NULL, 0),
(484, 0, 'x-men', '2020-05-28 23:21:07', '2020-10-13 21:53:40', 'X-Men', NULL, NULL, NULL, NULL, 0),
(485, 0, 'x-men-2', '2020-05-28 23:21:20', '2020-10-13 21:54:10', 'X-Men 2', NULL, NULL, NULL, NULL, 0),
(486, 0, 'x-men-l-affrontement-final', '2020-05-28 23:21:37', '2020-10-13 21:54:33', 'X-Men : L\'affrontement final', NULL, NULL, NULL, NULL, 0),
(487, 0, 'superman-batman-ennemis-publics', '2020-05-28 23:26:34', '2020-10-13 21:55:04', 'Superman Batman : Ennemis Publics', NULL, NULL, NULL, NULL, 0),
(488, 0, 'jumanji', '2020-05-28 23:26:58', '2020-10-13 21:55:27', 'Jumanji', NULL, NULL, NULL, NULL, 0),
(489, 0, 'ultimate-game', '2020-05-28 23:27:17', '2020-10-13 21:55:50', 'Ultimate Game', NULL, NULL, NULL, NULL, 0),
(490, 0, 'matrix', '2020-05-28 23:27:45', '2020-10-13 21:56:10', 'Matrix', NULL, NULL, NULL, NULL, 0),
(491, 0, 'matrix-reloaded', '2020-05-28 23:28:03', '2020-10-13 21:56:29', 'Matrix Reloaded', NULL, NULL, NULL, NULL, 0),
(492, 0, 'matrix-revolutions', '2020-05-28 23:28:20', '2020-10-13 21:56:48', 'Matrix Revolutions', NULL, NULL, NULL, NULL, 0),
(493, 0, 'mad-max-fury-road', '2020-05-28 23:29:55', '2020-10-13 21:57:11', 'Mad Max : Fury Road', NULL, NULL, NULL, NULL, 0),
(494, 0, 'le-roi-lion', '2020-05-28 23:30:46', '2020-10-13 21:57:41', 'Le Roi Lion', NULL, NULL, NULL, NULL, 0),
(495, 0, 'mulan', '2020-05-28 23:31:03', '2020-10-13 21:58:05', 'Mulan', NULL, NULL, NULL, NULL, 0),
(496, 0, 'blanche-neige-et-les-sept-nains', '2020-05-28 23:31:36', '2020-10-13 21:58:26', 'Blanche Neige et Les Sept Nains', NULL, NULL, NULL, NULL, 0),
(497, 0, 'demolition-man', '2020-05-31 19:54:50', '2020-10-13 21:58:43', 'Demolition Man', NULL, NULL, NULL, NULL, 0),
(499, 0, 'elektra', '2020-05-31 19:55:36', '2020-10-13 21:59:02', 'Elektra', NULL, NULL, NULL, NULL, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `video`
--
ALTER TABLE `video`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_DF3734EC989D9B62` (`slug`),
  ADD KEY `IDX_DF3734ECAFBA6FD8` (`created_at`),
  ADD KEY `IDX_DF3734ECBE74E59A` (`updated_at`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `video`
--
ALTER TABLE `video`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=500;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
SQL;


        $this->addSql($sqlVideo);
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE video');
    }
}
