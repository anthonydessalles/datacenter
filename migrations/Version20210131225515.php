<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210131225515 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE video_type (video_id INT NOT NULL, type_id INT NOT NULL, INDEX IDX_8B490AF529C1004E (video_id), INDEX IDX_8B490AF5C54C8C93 (type_id), PRIMARY KEY(video_id, type_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE video_type ADD CONSTRAINT FK_8B490AF529C1004E FOREIGN KEY (video_id) REFERENCES video (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE video_type ADD CONSTRAINT FK_8B490AF5C54C8C93 FOREIGN KEY (type_id) REFERENCES type (id) ON DELETE CASCADE');
        $this->addSql('DROP INDEX IDX_DF3734ECBE74E59A ON video');
        $this->addSql('DROP INDEX IDX_DF3734EC989D9B62 ON video');
        $this->addSql('DROP INDEX IDX_DF3734ECAFBA6FD8 ON video');
        $this->addSql('ALTER TABLE video CHANGE allocine allocine INT DEFAULT NULL, CHANGE year year INT NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE video_type');
        $this->addSql('ALTER TABLE video CHANGE allocine allocine INT DEFAULT 0 NOT NULL, CHANGE year year INT DEFAULT 0 NOT NULL');
        $this->addSql('CREATE INDEX IDX_DF3734ECBE74E59A ON video (updated_at)');
        $this->addSql('CREATE INDEX IDX_DF3734EC989D9B62 ON video (slug)');
        $this->addSql('CREATE INDEX IDX_DF3734ECAFBA6FD8 ON video (created_at)');
    }
}
