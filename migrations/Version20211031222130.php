<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211031222130 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE action DROP FOREIGN KEY FK_47CC8C92ABB51437');
        $this->addSql('DROP INDEX IDX_47CC8C92ABB51437 ON action');
        $this->addSql('ALTER TABLE action DROP action_name_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE action ADD action_name_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE action ADD CONSTRAINT FK_47CC8C92ABB51437 FOREIGN KEY (action_name_id) REFERENCES action_name (id)');
        $this->addSql('CREATE INDEX IDX_47CC8C92ABB51437 ON action (action_name_id)');
    }
}
