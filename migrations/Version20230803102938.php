<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230803102938 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE device_album (device_id INT NOT NULL, album_id INT NOT NULL, INDEX IDX_FF1D8E2494A4C7D4 (device_id), INDEX IDX_FF1D8E241137ABCF (album_id), PRIMARY KEY(device_id, album_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE device_file (device_id INT NOT NULL, file_id INT NOT NULL, INDEX IDX_5A6E32A94A4C7D4 (device_id), INDEX IDX_5A6E32A93CB796C (file_id), PRIMARY KEY(device_id, file_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE device_album ADD CONSTRAINT FK_FF1D8E2494A4C7D4 FOREIGN KEY (device_id) REFERENCES device (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE device_album ADD CONSTRAINT FK_FF1D8E241137ABCF FOREIGN KEY (album_id) REFERENCES album (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE device_file ADD CONSTRAINT FK_5A6E32A94A4C7D4 FOREIGN KEY (device_id) REFERENCES device (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE device_file ADD CONSTRAINT FK_5A6E32A93CB796C FOREIGN KEY (file_id) REFERENCES file (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE device_album DROP FOREIGN KEY FK_FF1D8E2494A4C7D4');
        $this->addSql('ALTER TABLE device_album DROP FOREIGN KEY FK_FF1D8E241137ABCF');
        $this->addSql('ALTER TABLE device_file DROP FOREIGN KEY FK_5A6E32A94A4C7D4');
        $this->addSql('ALTER TABLE device_file DROP FOREIGN KEY FK_5A6E32A93CB796C');
        $this->addSql('DROP TABLE device_album');
        $this->addSql('DROP TABLE device_file');
    }
}
