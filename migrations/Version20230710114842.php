<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230710114842 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE sport_file (sport_id INT NOT NULL, file_id INT NOT NULL, INDEX IDX_FB4018ECAC78BCF8 (sport_id), INDEX IDX_FB4018EC93CB796C (file_id), PRIMARY KEY(sport_id, file_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE sport_file ADD CONSTRAINT FK_FB4018ECAC78BCF8 FOREIGN KEY (sport_id) REFERENCES sport (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE sport_file ADD CONSTRAINT FK_FB4018EC93CB796C FOREIGN KEY (file_id) REFERENCES file (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE sport ADD max_height INT DEFAULT NULL, ADD max_width INT DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE sport_file DROP FOREIGN KEY FK_FB4018ECAC78BCF8');
        $this->addSql('ALTER TABLE sport_file DROP FOREIGN KEY FK_FB4018EC93CB796C');
        $this->addSql('DROP TABLE sport_file');
        $this->addSql('ALTER TABLE sport DROP max_height, DROP max_width');
    }
}
