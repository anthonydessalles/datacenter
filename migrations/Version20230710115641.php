<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230710115641 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE film_file (film_id INT NOT NULL, file_id INT NOT NULL, INDEX IDX_E930DC13567F5183 (film_id), INDEX IDX_E930DC1393CB796C (file_id), PRIMARY KEY(film_id, file_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE film_file ADD CONSTRAINT FK_E930DC13567F5183 FOREIGN KEY (film_id) REFERENCES film (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE film_file ADD CONSTRAINT FK_E930DC1393CB796C FOREIGN KEY (file_id) REFERENCES file (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE film ADD max_height INT DEFAULT NULL, ADD max_width INT DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE film_file DROP FOREIGN KEY FK_E930DC13567F5183');
        $this->addSql('ALTER TABLE film_file DROP FOREIGN KEY FK_E930DC1393CB796C');
        $this->addSql('DROP TABLE film_file');
        $this->addSql('ALTER TABLE film DROP max_height, DROP max_width');
    }
}
