<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190507165837 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE file ADD source_url VARCHAR(255) DEFAULT NULL, ADD mime_type VARCHAR(255) DEFAULT NULL, ADD height INT DEFAULT NULL, ADD width INT DEFAULT NULL, ADD camera_make VARCHAR(255) DEFAULT NULL, ADD camera_model VARCHAR(255) DEFAULT NULL, ADD external_id VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE file DROP source_url, DROP mime_type, DROP height, DROP width, DROP camera_make, DROP camera_model, DROP external_id');
    }
}
