<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210202123000 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $sqlDevice = <<<SQL
INSERT INTO `device` (`id`, `slug`, `created_at`, `updated_at`, `title`, `synopsis`, `number`) VALUES
(51, 'manette-gamecube', '2014-07-19 10:35:27', '2018-12-22 13:20:11', 'Manette GameCube', '<p>C\'est la manette sortie avec la console du même nom qui peut être utilisée sur la Wii et la Wii U avec un adaptateur spécial. Elle est très agréable à prendre en main.</p>\r\n', 3),
(52, 'manette-xbox', '2014-08-01 00:44:49', '2018-12-22 13:19:56', 'Manette Xbox', '<p>La manette de la première console Xbox créée par Microsoft. Elle est relativement imposante et est composée de 10 boutons.</p>\r\n', 3),
(53, 'manette-playstation-1', '2014-08-01 00:48:30', '2018-12-22 13:19:21', 'Manette PlayStation 1', '<p>C\'est la manette de la PlayStation 1. Le modèle original, sorti en 1994, ne possède pas de joysticks. C\'est le modèle DualShock qui en possèdera.</p>\r\n', 4),
(34, 'manette-playstation-2', '2014-08-01 00:51:44', '2018-12-22 13:19:09', 'Manette PlayStation 2', '<p>C\'est la manette de la PlayStation 2 de Sony. Il ressemble comme deux gouttes d\'eau à la Manette DualShock de PlayStation 1. De base, seulement sa couleur est différente car le modèle le plus courant est noir.</p>\r\n', 4),
(35, 'manette-playstation-3', '2014-08-01 00:54:34', '2018-12-22 13:18:56', 'Manette PlayStation 3', '<p>C\'est la manette sortie avec la console du même nom. Le modèle ressemble beaucoup à celui de la génération précédente si ce n\'est que la manette est sans fil, possède un bouton home et un détecteur de mouvement.</p>\r\n', 1),
(36, 'buzzers-buzz-junior', '2014-08-01 13:27:40', '2018-12-22 13:18:42', 'Buzzers Buzz Junior', '<p>Les Buzzers utilisés pour tous les jeux Buzz ! Vendus par 4, ils sont utilisables sur PlayStation 2 et PlayStation 3 et se branchent en USB.</p>\r\n', 1),
(37, 'manette-nintendo-64', '2014-08-01 13:32:26', '2018-12-22 13:18:28', 'Manette Nintendo 64', '<p>La manette de la Nintendo 64 est un modèle unique. C\'est la première à utiliser un joystick et possède la particularité d\'avoir trois manches. De plus, la carte mémoire s\'insère directement dans la manette.</p>\r\n', 4),
(38, 'manette-dreamcast', '2014-08-01 13:51:37', '2018-12-22 13:18:13', 'Manette Dreamcast', '<p>C\'est la manette de la dernière console fariquée par Sega : la Dreamcast. Comme pour la N64, la carte mémoire s\'insère directement dans la manette et possède un petit écran.</p>\r\n', 2),
(39, 'manette-mega-drive', '2014-08-01 13:58:55', '2018-12-22 13:15:52', 'Manette Mega Drive', '<p>La manette de la Mega Drive (modèle original ou Mega Drive II) est une manette classique avec des flèches directionnelles et 4 boutons (Start, A, B, C).</p>\r\n', 2),
(40, 'wiimote', '2014-08-01 14:29:23', '2018-12-22 13:18:01', 'Wiimote', '<p>C\'est la manette principale de la Wii sortie en 2006. Elle est rarement utilisable seule, on lui ajoute donc le Nunchuk.</p>\r\n', 2),
(41, 'nunchuk', '2014-08-01 14:32:32', '2018-12-22 13:17:49', 'Nunchuk', '<p>C\'est l\'extension de la Wiimote, sortie avec la Wii en 2006. Le Nunchuk n\'est pas nécessaire dans tous les jeux mais est obligatoire dans tous les jeux nécessitant le déplacement du personnage.</p>\r\n', 3),
(42, 'manette-wii', '2014-08-01 14:34:05', '2018-12-22 13:17:31', 'Manette Wii', '<p>C\'est une manette classique compatible avec certains jeux de la Wii.</p>\r\n', 1),
(43, 'carte-memoire-dreamcast', '2014-08-01 14:43:59', '2018-12-22 13:17:18', 'Carte Mémoire Dreamcast', '<p>C\'est la carte mémoire utilisée pour sauvegarder les données des jeux de Dreamcast.</p>\r\n', 1),
(44, 'carte-memoire-playstation-2', '2014-08-01 14:48:05', '2018-12-22 13:17:04', 'Carte Mémoire PlayStation 2', '<p>C\'est la carte mémoire utilisable dans tous les systèmes PlayStation 2 pour effectuer des sauvegardes de données dans les jeux.</p>\r\n', 2),
(45, 'carte-memoire-playstation-1', '2014-08-01 14:49:48', '2018-12-22 13:16:51', 'Carte Mémoire PlayStation 1', '<p>La carte mémoire utilisée pour la sauvegarde des données de jeux sur PlayStation 1.</p>\r\n', 2),
(46, 'carte-memoire-gamecube', '2014-08-01 14:54:41', '2018-12-22 13:16:37', 'Carte Mémoire GameCube', '<p>La carte mémoire utilisée avec la console Nintendo GameCube pour effectuer des sauvegardes dans les jeux.</p>\r\n', 2),
(47, 'carte-memoire-nintendo-64', '2014-08-01 15:00:56', '2018-12-22 13:16:22', 'Carte Mémoire Nintendo 64', '<p>La carte mémoire de la Nintendo 64, à insérer directement dans la manette, sert à effectuer des sauvegardes dans les jeux.</p>\r\n', 1),
(48, 'carte-memoire-psp', '2014-08-01 15:06:25', '2017-12-07 00:11:47', 'Carte Mémoire PSP', '<p>La carte de mémoire de la PlayStation Portable permettant d\'enregistrer les sauvegardes, des images, de la musique et des vidéos.</p>\r\n', 3),
(49, 'manette-xbox-360', '2014-08-14 21:13:49', '2017-12-07 00:07:38', 'Manette Xbox 360', '<p>La manette de la console microsoft (existant en modèle filaire ou non) et dont les coloris les plus répandus sont noir et blanc. Elle est très agréable à prendre en main.</p>\r\n', 3),
(50, 'manette-playstation-4', '2020-01-13 22:39:12', '2020-01-13 22:39:12', 'Manette PlayStation 4', NULL, 2);
SQL;

        $this->addSql($sqlDevice);
    }

    public function down(Schema $schema) : void
    {
    }
}
