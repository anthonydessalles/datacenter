<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230109105057 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE compatibility_file (compatibility_id INT NOT NULL, file_id INT NOT NULL, INDEX IDX_44295546E610868E (compatibility_id), INDEX IDX_4429554693CB796C (file_id), PRIMARY KEY(compatibility_id, file_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE compatibility_file ADD CONSTRAINT FK_44295546E610868E FOREIGN KEY (compatibility_id) REFERENCES compatibility (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE compatibility_file ADD CONSTRAINT FK_4429554693CB796C FOREIGN KEY (file_id) REFERENCES file (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE compatibility_file DROP FOREIGN KEY FK_44295546E610868E');
        $this->addSql('ALTER TABLE compatibility_file DROP FOREIGN KEY FK_4429554693CB796C');
        $this->addSql('DROP TABLE compatibility_file');
    }
}
