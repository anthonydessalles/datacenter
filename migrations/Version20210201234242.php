<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210201234242 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $sqlDevice = <<<SQL
-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: db
-- Generation Time: Feb 01, 2021 at 11:42 PM
-- Server version: 5.7.32
-- PHP Version: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `datacenter`
--

-- --------------------------------------------------------

--
-- Table structure for table `device`
--

CREATE TABLE `device` (
  `id` int(11) NOT NULL,
  `slug` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `title` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `synopsis` longtext COLLATE utf8_unicode_ci,
  `achat` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `device`
--

INSERT INTO `device` (`id`, `slug`, `created_at`, `updated_at`, `title`, `synopsis`, `achat`) VALUES
(1, 'pc-rivertech', '2014-07-19 10:46:49', '2019-01-16 12:33:17', 'PC Rivertech', '<p>L\'Ordinateur a été acheté et monté par Internity à Riorges.</p>\r\n', '2007'),
(2, 'hp-pro-3305-microtower-pc', '2014-07-19 11:03:38', '2019-04-07 15:14:31', 'HP Pro 3305 Microtower PC', '<p>L\'ordinateur acheté à mes parents pour me servir initialement de serveur web personnel.</p>\r\n', '2014'),
(3, 'pc-asus-chocolat', '2014-07-19 11:06:07', '2015-09-26 14:01:10', 'Ordinateur Asus', '<p>C\'est l\'ordinateur acheté pendant le BTS pour travailler plus facilement avec les programmes. Actuellement, il tournait sous le système d\'exploitation Windows 8.1.</p>', '2011'),
(4, 'msi-cx61-2pc-802xfr', '2014-11-24 18:56:49', '2017-12-06 23:06:11', 'MSI CX61 2PC-802XFR', '<p>Parce qu\'un petit prix MSI offre de bonnes surprises, découvrez vite l\'ordinateur portable CX61 2PC-802XFR ! Du processeur Intel Core i5 Haswell à la carte graphique dédiée NVIDIA GeForce , en passant par la connectique USB 3.0 et Bluetooth 4.0, tout vous séduira dans ce 15 pouces à la dalle mate ! Livré sans OS, vous faites des économies en choisissant vous même votre système d\'exploitation.</p>\r\n', '21 novembre 2014'),
(5, 'asus-zenpad-8-0', '2017-11-21 18:55:05', '2019-01-16 12:01:59', 'ASUS ZenPad 8.0', '<p>Asus Zenpad 8 Z380M-6B026A 20,32 cm (8 Pouces) Tablette PC (Mediatek 8163 Quad Core, 2 Go de RAM, 16 Go HDD, Mali T720, Android 6) MediaTek 8163 Quad Core Gris foncé</p>\r\n\r\n<table><tbody>\n<tr>\n<td>Marque</td>\r\n            <td>Asus</td>\r\n        </tr>\n<tr>\n<td>Numéro du modèle de l\'article</td>\r\n            <td>90NP00A1-M00570</td>\r\n        </tr>\n<tr>\n<td>séries</td>\r\n            <td>ASUS ZenPad 8.0</td>\r\n        </tr>\n<tr>\n<td>Couleur</td>\r\n            <td>gris foncé</td>\r\n        </tr>\n<tr>\n<td>Système d\'exploitation</td>\r\n            <td>Android</td>\r\n        </tr>\n<tr>\n<td>Marque du processeur</td>\r\n            <td>ARM</td>\r\n        </tr>\n<tr>\n<td>Vitesse du processeur</td>\r\n            <td>1.3 GHz</td>\r\n        </tr>\n<tr>\n<td>Nombre de coeurs</td>\r\n            <td>4</td>\r\n        </tr>\n<tr>\n<td>Taille de la mémoire vive</td>\r\n            <td>2048 MB</td>\r\n        </tr>\n<tr>\n<td>Taille du disque dur</td>\r\n            <td>16 GB</td>\r\n        </tr>\n<tr>\n<td>Taille de l\'écran</td>\r\n            <td>8 pouces</td>\r\n        </tr>\n<tr>\n<td>Résolution de l\'écran</td>\r\n            <td>1280 x 800 pixels</td>\r\n        </tr>\n<tr>\n<td>Résolution maximale d\'affichage</td>\r\n            <td>1280 x 800 pixels</td>\r\n        </tr>\n<tr>\n<td>Marque chipset graphique</td>\r\n            <td>ARM</td>\r\n        </tr>\n<tr>\n<td>GPU</td>\r\n            <td>ARM</td>\r\n        </tr>\n<tr>\n<td>Mémoire vive de la carte graphique</td>\r\n            <td>0.6</td>\r\n        </tr>\n<tr>\n<td>Type de technologie sans fil</td>\r\n            <td>802.11 a/b/g/n</td>\r\n        </tr>\n<tr>\n<td>Bluetooth</td>\r\n            <td>Oui</td>\r\n        </tr>\n<tr>\n<td>Nombre de ports USB 2.0</td>\r\n            <td>1</td>\r\n        </tr>\n<tr>\n<td>Type de connecteur</td>\r\n            <td>Bluetooth</td>\r\n        </tr>\n<tr>\n<td>Durée de vie moyenne (en heures)</td>\r\n            <td>10 heures</td>\r\n        </tr>\n<tr>\n<td>Item dimensions L x W x H</td>\r\n            <td>12,3 x 0,8 x 20,9 cm</td>\r\n        </tr>\n<tr>\n<td>Poids du produit</td>\r\n            <td>349 grammes</td>\r\n        </tr>\n</tbody></table>', '11 novembre 2017'),
(6, 'samsung-galaxy-s-ii', '2017-12-06 22:20:52', '2017-12-06 23:10:25', 'Samsung Galaxy S II', '<p>Le Samsung Galaxy S II est un smartphone haut de gamme créé par Samsung, dévoilé par la firme le 13 février 2011 lors du Mobile World Congress 2011 de Barcelone. Il succède au Galaxy S et précède le Galaxy S III.</p>\r\n', '2012'),
(7, 'thdd-backup-1-2-72-tb', '2014-07-19 10:54:20', '2019-01-16 12:12:44', 'THDD Backup 1 (2,72 TB)', '<p>WD - WD Elements Desktop - Disque dur de bureau USB 3.0 - 3 To</p>\r\n\r\n<table><tbody>\n<tr>\n<td>Marque</td>\r\n            <td>Western Digital</td>\r\n        </tr>\n<tr>\n<td>Numéro du modèle de l\'article</td>\r\n            <td>WDBWLG0030HBK-EESN</td>\r\n        </tr>\n<tr>\n<td>séries</td>\r\n            <td>WD Elements, 3TB</td>\r\n        </tr>\n<tr>\n<td>Couleur</td>\r\n            <td>Noir</td>\r\n        </tr>\n<tr>\n<td>Garantie constructeur</td>\r\n            <td>1 an</td>\r\n        </tr>\n<tr>\n<td>Description du clavier</td>\r\n            <td>Français</td>\r\n        </tr>\n<tr>\n<td>Nombre de coeurs</td>\r\n            <td>1</td>\r\n        </tr>\n<tr>\n<td>Taille du disque dur</td>\r\n            <td>3000 GB</td>\r\n        </tr>\n<tr>\n<td>Type d\'écran</td>\r\n            <td>Plasma</td>\r\n        </tr>\n<tr>\n<td>Type de connectivité</td>\r\n            <td>Sans-fil</td>\r\n        </tr>\n<tr>\n<td>Interface du matériel informatique</td>\r\n            <td>USB 3.0</td>\r\n        </tr>\n<tr>\n<td>Type de connecteur</td>\r\n            <td>USB 3.0</td>\r\n        </tr>\n<tr>\n<td>Item dimensions L x W x H</td>\r\n            <td>4,8 x 13,5 x 16,6 cm</td>\r\n        </tr>\n<tr>\n<td>Poids du produit</td>\r\n            <td>912 grammes</td>\r\n        </tr>\n<tr>\n<td>Divers</td>\r\n            <td>Changement à Chaud</td>\r\n        </tr>\n</tbody></table>', '12 mai 2014'),
(8, 'thdd-backup-3-1-81-tb', '2014-07-19 10:56:44', '2019-01-16 12:28:49', 'THDD Backup 3 (1,81 TB)', '<p>C\'est disque dur externe de chez Western Digital.</p>\r\n', NULL),
(9, 'thdd-backup-2-2-72-tb', '2014-08-01 16:19:05', '2019-01-16 12:26:37', 'THDD Backup 2 (2,72 TB)', '<p>C\'est un ancien modèle de disque dur Western Digital. Actuellement c\'est un disque dur dans un boîtier Enermax.</p>\r\n\r\n<ul>\n<li>Western Digital</li>\r\n    <li>3.0TB</li>\r\n    <li>SATA / 64MB Cache</li>\r\n    <li>WD30EZRX</li>\r\n    <li>WWN : 50014EE6AD88E79A</li>\r\n    <li>DATE : 19 OCT 2012</li>\r\n    <li>DCM : HARCNVJMH</li>\r\n    <li>DCX : 1D05C0107</li>\r\n    <li>5VDC : 0.60A</li>\r\n    <li>12VDC : .45A</li>\r\n    <li>R/N : 771824</li>\r\n    <li>S/N : WMC1T0817654</li>\r\n    <li>MDL : WD30EZRX - 00DC0B0</li>\r\n</ul>', '30 janvier 2013'),
(10, 'thdd-games', '2014-08-06 12:04:02', '2019-01-16 12:32:32', 'THDD Games', '<p>Anciennement un disque dur multimédia Western Digital ce disque dur est désormais dans un boîtier classique Connectland. Il contient actuellement plusieurs partitions contenant chacune des versions numériques de mes jeux.</p>\r\n\r\n<ul>\n<li>Western Digital</li>\r\n    <li>1.0TB</li>\r\n    <li>SATA / 8MB Cache</li>\r\n    <li>WD10EAVS</li>\r\n    <li>S/N : WCAU45271749</li>\r\n    <li>MDL : WD10EAVS - 00D7B0</li>\r\n    <li>WWN : 50014EE2ACFEA5FB</li>\r\n    <li>DATE : 10 DEC 2008</li>\r\n    <li>DCM : HBNNNT2MBB</li>\r\n    <li>LBA : 1953525168</li>\r\n    <li>5VDC : 0.70A</li>\r\n    <li>12VDC : 0.55A</li>\r\n    <li>R/N : 701537</li>\r\n</ul>', NULL),
(11, 'thdd-portable', '2014-11-24 19:13:33', '2019-01-16 12:31:12', 'THDD Portable', '<p>C\'est disque dur externe Seagate. Il contient mes documents courants.</p>\r\n', '22 novembre 2014'),
(12, 'honor-8', '2018-04-16 10:26:26', '2019-01-16 11:58:16', 'Honor 8', '<p>Honor 8 Smartphone débloqué 4G (Ecran: 5,2 pouces - 32 Go - Double Nano-SIM - Android 6.0) Bleu Saphir</p>\r\n\r\n<table><tbody>\n<tr>\n<td>Marque</td>\r\n            <td>HONOR</td>\r\n        </tr>\n<tr>\n<td>Couleur</td>\r\n            <td>Bleu</td>\r\n        </tr>\n<tr>\n<td>Type de produit</td>\r\n            <td>monobloc_tout_tactile</td>\r\n        </tr>\n<tr>\n<td>Système d\'exploitation</td>\r\n            <td>android</td>\r\n        </tr>\n<tr>\n<td>Ecran</td>\r\n            <td>5.2 pouces</td>\r\n        </tr>\n<tr>\n<td>Connexions</td>\r\n            <td>USB;WiFi 4G</td>\r\n        </tr>\n<tr>\n<td>Résolution de l\'appareil photo</td>\r\n            <td>12</td>\r\n        </tr>\n<tr>\n<td>Fonctions</td>\r\n            <td>connecteur_stéréo_jack</td>\r\n        </tr>\n<tr>\n<td>Type de batterie</td>\r\n            <td>Lithium-polymère</td>\r\n        </tr>\n<tr>\n<td>Garantie constructeur</td>\r\n            <td>Garantie Fabricant: 2 ans</td>\r\n        </tr>\n</tbody></table>\n<p></p>\r\n', '24 novembre 2017 (pour Noël)'),
(13, 'raspberry-pi-3', '2018-04-16 10:29:19', '2019-04-11 20:10:43', 'Raspberry Pi 3', '<p>Raspberry Pi 3 Official Desktop Starter Kit (16GB, White)</p>\r\n\r\n<table><tbody>\n<tr>\n<td>Marque</td>\r\n            <td>Raspberry Pi</td>\r\n        </tr>\n<tr>\n<td>Numéro du modèle de l\'article</td>\r\n            <td>RPi3_OffStrKit</td>\r\n        </tr>\n<tr>\n<td>Couleur</td>\r\n            <td>White</td>\r\n        </tr>\n<tr>\n<td>Système d\'exploitation</td>\r\n            <td>Linux</td>\r\n        </tr>\n<tr>\n<td>Plate-forme du matériel informatique</td>\r\n            <td>Linux</td>\r\n        </tr>\n<tr>\n<td>Marque du processeur</td>\r\n            <td>Broadcom</td>\r\n        </tr>\n<tr>\n<td>Type de processeur</td>\r\n            <td>ARM710</td>\r\n        </tr>\n<tr>\n<td>Vitesse du processeur</td>\r\n            <td>1200 MHz</td>\r\n        </tr>\n<tr>\n<td>Nombre de coeurs</td>\r\n            <td>4</td>\r\n        </tr>\n<tr>\n<td>Taille de la mémoire vive</td>\r\n            <td>1 GB</td>\r\n        </tr>\n<tr>\n<td>Interface du disque dur</td>\r\n            <td>USB</td>\r\n        </tr>\n</tbody></table>', '3 mars 2018'),
(14, 'thdd-backup-4-298-gb', '2018-11-30 15:50:24', '2019-01-16 12:30:00', 'THDD Backup 4 (298 GB)', '<p>C\'est le deuxième disque dur physique présent dans le PC Rivertech.</p>\r\n', NULL),
(15, 'thdd-main', '2019-01-16 11:50:31', '2019-01-16 11:59:44', 'THDD Main', '<p>WD My Passport Ultra Disque Dur Externe Portable 4To avec Sauvegarde Automatique pour PC, Xbox One et Playstation 4 - Blanc/Doré</p>\r\n\r\n<table><tbody>\n<tr>\n<td>Marque</td>\r\n            <td>Western Digital</td>\r\n        </tr>\n<tr>\n<td>Numéro du modèle de l\'article</td>\r\n            <td>WDBFKT0040BGD-WESN</td>\r\n        </tr>\n<tr>\n<td>séries</td>\r\n            <td>My Passport Ultra</td>\r\n        </tr>\n<tr>\n<td>Couleur</td>\r\n            <td>Blanc/Doré</td>\r\n        </tr>\n<tr>\n<td>Garantie constructeur</td>\r\n            <td>3 Years</td>\r\n        </tr>\n<tr>\n<td>Description du clavier</td>\r\n            <td>Français</td>\r\n        </tr>\n<tr>\n<td>Taille du disque dur</td>\r\n            <td>4000 GB</td>\r\n        </tr>\n<tr>\n<td>Bluetooth</td>\r\n            <td>Non</td>\r\n        </tr>\n<tr>\n<td>Interface du matériel informatique</td>\r\n            <td>USB 3.0</td>\r\n        </tr>\n<tr>\n<td>Nombre de ports USB 2.0</td>\r\n            <td>1</td>\r\n        </tr>\n<tr>\n<td>Nombre de ports USB 3.0</td>\r\n            <td>1</td>\r\n        </tr>\n<tr>\n<td>Type de connecteur</td>\r\n            <td>usb_2.0</td>\r\n        </tr>\n<tr>\n<td>Compatibilité du périphérique</td>\r\n            <td>disque dur</td>\r\n        </tr>\n<tr>\n<td>Item dimensions L x W x H</td>\r\n            <td>11 x 8,2 x 2,2 cm</td>\r\n        </tr>\n<tr>\n<td>Poids du produit</td>\r\n            <td>245 grammes</td>\r\n        </tr>\n<tr>\n<td>Divers</td>\r\n            <td>Changement à Chaud, Fonction Backup, Autoalimenté</td>\r\n        </tr>\n</tbody></table>', '26 novembre 2018'),
(16, 'samsung-galaxy-a10', '2019-09-17 15:33:49', '2020-01-13 23:02:45', 'Samsung Galaxy A10', '<p><a href=\"https://www.amazon.fr/gp/product/B07KXC1QGW/ref=ppx_yo_dt_b_asin_title_o00_s00?ie=UTF8&amp;psc=1\" target=\"_blank\">https://www.amazon.fr/gp/product/B07KXC1QGW/ref=ppx_yo_dt_b_asin_title_o00_s00?ie=UTF8&amp;psc=1</a></p>\r\n', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `device`
--
ALTER TABLE `device`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_1A6950AF989D9B62` (`slug`),
  ADD KEY `IDX_1A6950AFAFBA6FD8` (`created_at`),
  ADD KEY `IDX_1A6950AFBE74E59A` (`updated_at`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `device`
--
ALTER TABLE `device`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
SQL;


        $this->addSql($sqlDevice);
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE device');
    }
}
