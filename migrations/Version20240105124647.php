<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240105124647 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE stand_up_album DROP FOREIGN KEY FK_F3F0221C1137ABCF');
        $this->addSql('ALTER TABLE stand_up_album DROP FOREIGN KEY FK_F3F0221C3CB08647');
        $this->addSql('ALTER TABLE stand_up_file DROP FOREIGN KEY FK_5E11E3E13CB08647');
        $this->addSql('ALTER TABLE stand_up_file DROP FOREIGN KEY FK_5E11E3E193CB796C');
        $this->addSql('ALTER TABLE stand_up_type DROP FOREIGN KEY FK_5E5082D83CB08647');
        $this->addSql('ALTER TABLE stand_up_type DROP FOREIGN KEY FK_5E5082D8C54C8C93');
        $this->addSql('DROP TABLE stand_up');
        $this->addSql('DROP TABLE stand_up_album');
        $this->addSql('DROP TABLE stand_up_file');
        $this->addSql('DROP TABLE stand_up_type');
    }

    public function down(Schema $schema): void
    {
    }
}
