<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231205211912 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE progression ADD game_id INT DEFAULT NULL, ADD type_id INT NOT NULL');
        $this->addSql('ALTER TABLE progression ADD CONSTRAINT FK_D5B25073E48FD905 FOREIGN KEY (game_id) REFERENCES game (id)');
        $this->addSql('ALTER TABLE progression ADD CONSTRAINT FK_D5B25073C54C8C93 FOREIGN KEY (type_id) REFERENCES type (id)');
        $this->addSql('CREATE INDEX IDX_D5B25073E48FD905 ON progression (game_id)');
        $this->addSql('CREATE INDEX IDX_D5B25073C54C8C93 ON progression (type_id)');
    }

    public function down(Schema $schema): void
    {
    }
}
