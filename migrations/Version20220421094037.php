<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220421094037 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE user_kpi (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, type_id INT NOT NULL, total DOUBLE PRECISION NOT NULL, INDEX IDX_4B843C52A76ED395 (user_id), INDEX IDX_4B843C52C54C8C93 (type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user_kpi ADD CONSTRAINT FK_4B843C52A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_kpi ADD CONSTRAINT FK_4B843C52C54C8C93 FOREIGN KEY (type_id) REFERENCES type (id)');

        // Unique user+type KPI
        $this->addSql('ALTER TABLE user_kpi ADD CONSTRAINT UN_USER_TYPE_KPI UNIQUE (user_id, type_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE user_kpi');
    }
}
