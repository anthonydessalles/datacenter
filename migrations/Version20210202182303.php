<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210202182303 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE device ADD description LONGTEXT DEFAULT NULL');
        $this->addSql('UPDATE device SET description = synopsis');
        $this->addSql('ALTER TABLE device DROP synopsis');
    }

    public function down(Schema $schema) : void
    {
    }
}
