<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211127220300 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE game_link (game_id INT NOT NULL, link_id INT NOT NULL, INDEX IDX_DDB9F5DDE48FD905 (game_id), INDEX IDX_DDB9F5DDADA40271 (link_id), PRIMARY KEY(game_id, link_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE game_album (game_id INT NOT NULL, album_id INT NOT NULL, INDEX IDX_BAB17CCE48FD905 (game_id), INDEX IDX_BAB17CC1137ABCF (album_id), PRIMARY KEY(game_id, album_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE game_link ADD CONSTRAINT FK_DDB9F5DDE48FD905 FOREIGN KEY (game_id) REFERENCES game (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE game_link ADD CONSTRAINT FK_DDB9F5DDADA40271 FOREIGN KEY (link_id) REFERENCES link (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE game_album ADD CONSTRAINT FK_BAB17CCE48FD905 FOREIGN KEY (game_id) REFERENCES game (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE game_album ADD CONSTRAINT FK_BAB17CC1137ABCF FOREIGN KEY (album_id) REFERENCES album (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE type DROP font_awesome');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE game_link');
        $this->addSql('DROP TABLE game_album');
        $this->addSql('ALTER TABLE type ADD font_awesome VARCHAR(45) CHARACTER SET utf8 DEFAULT NULL COLLATE `utf8_unicode_ci`');
    }
}
