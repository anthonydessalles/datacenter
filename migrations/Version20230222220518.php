<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230222220518 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE progression_user (progression_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_9F5C28CBAF229C18 (progression_id), INDEX IDX_9F5C28CBA76ED395 (user_id), PRIMARY KEY(progression_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE progression_user ADD CONSTRAINT FK_9F5C28CBAF229C18 FOREIGN KEY (progression_id) REFERENCES progression (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE progression_user ADD CONSTRAINT FK_9F5C28CBA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE progression RENAME INDEX uniq_6a2ca10c5e237e06 TO UNIQ_D5B250735E237E06');
        $this->addSql('ALTER TABLE progression RENAME INDEX idx_6a2ca10c93cb796c TO IDX_D5B2507393CB796C');
    }

    public function down(Schema $schema): void
    {
    }
}
