<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240313110532 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE profile_score (profile_id INT NOT NULL, score_id INT NOT NULL, INDEX IDX_C54DA883CCFA12B8 (profile_id), INDEX IDX_C54DA88312EB0A51 (score_id), PRIMARY KEY(profile_id, score_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE profile_score ADD CONSTRAINT FK_C54DA883CCFA12B8 FOREIGN KEY (profile_id) REFERENCES profile (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE profile_score ADD CONSTRAINT FK_C54DA88312EB0A51 FOREIGN KEY (score_id) REFERENCES score (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
    }
}
