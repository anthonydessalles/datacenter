<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201016215753 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE game DROP FOREIGN KEY FK_232B318C488626AA');
        $this->addSql('ALTER TABLE game DROP FOREIGN KEY FK_232B318C93CB796C');
        $this->addSql('DROP INDEX IDX_232B318C93CB796C ON game');
        $this->addSql('DROP INDEX IDX_232B318C488626AA ON game');
        $this->addSql('ALTER TABLE game DROP file_id, DROP wallpaper_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE game ADD file_id INT DEFAULT NULL, ADD wallpaper_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE game ADD CONSTRAINT FK_232B318C488626AA FOREIGN KEY (wallpaper_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE game ADD CONSTRAINT FK_232B318C93CB796C FOREIGN KEY (file_id) REFERENCES file (id)');
        $this->addSql('CREATE INDEX IDX_232B318C93CB796C ON game (file_id)');
        $this->addSql('CREATE INDEX IDX_232B318C488626AA ON game (wallpaper_id)');
    }
}
