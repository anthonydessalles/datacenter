<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230803104355 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE serie_file (serie_id INT NOT NULL, file_id INT NOT NULL, INDEX IDX_57FA2222D94388BD (serie_id), INDEX IDX_57FA222293CB796C (file_id), PRIMARY KEY(serie_id, file_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE serie_file ADD CONSTRAINT FK_57FA2222D94388BD FOREIGN KEY (serie_id) REFERENCES serie (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE serie_file ADD CONSTRAINT FK_57FA222293CB796C FOREIGN KEY (file_id) REFERENCES file (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE serie ADD episodes_number INT DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE serie_file DROP FOREIGN KEY FK_57FA2222D94388BD');
        $this->addSql('ALTER TABLE serie_file DROP FOREIGN KEY FK_57FA222293CB796C');
        $this->addSql('DROP TABLE serie_file');
        $this->addSql('ALTER TABLE serie DROP episodes_number');
    }
}
