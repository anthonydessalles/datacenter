<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240305114308 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE book_album DROP FOREIGN KEY FK_375A7B3A1137ABCF');
        $this->addSql('ALTER TABLE book_album DROP FOREIGN KEY FK_375A7B3A16A2B381');
        $this->addSql('ALTER TABLE device_album DROP FOREIGN KEY FK_FF1D8E241137ABCF');
        $this->addSql('ALTER TABLE device_album DROP FOREIGN KEY FK_FF1D8E2494A4C7D4');
        $this->addSql('ALTER TABLE film_album DROP FOREIGN KEY FK_A0F490131137ABCF');
        $this->addSql('ALTER TABLE film_album DROP FOREIGN KEY FK_A0F49013567F5183');
        $this->addSql('ALTER TABLE serie_album DROP FOREIGN KEY FK_F1945AD71137ABCF');
        $this->addSql('ALTER TABLE serie_album DROP FOREIGN KEY FK_F1945AD7D94388BD');
        $this->addSql('ALTER TABLE sport_album DROP FOREIGN KEY FK_8DE40F5AAC78BCF8');
        $this->addSql('ALTER TABLE sport_album DROP FOREIGN KEY FK_8DE40F5A1137ABCF');
        $this->addSql('DROP TABLE book_album');
        $this->addSql('DROP TABLE device_album');
        $this->addSql('DROP TABLE film_album');
        $this->addSql('DROP TABLE serie_album');
        $this->addSql('DROP TABLE sport_album');
    }

    public function down(Schema $schema): void
    {
    }
}
