<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230803102249 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE book_album (book_id INT NOT NULL, album_id INT NOT NULL, INDEX IDX_375A7B3A16A2B381 (book_id), INDEX IDX_375A7B3A1137ABCF (album_id), PRIMARY KEY(book_id, album_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE book_file (book_id INT NOT NULL, file_id INT NOT NULL, INDEX IDX_95027D1816A2B381 (book_id), INDEX IDX_95027D1893CB796C (file_id), PRIMARY KEY(book_id, file_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE book_album ADD CONSTRAINT FK_375A7B3A16A2B381 FOREIGN KEY (book_id) REFERENCES book (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE book_album ADD CONSTRAINT FK_375A7B3A1137ABCF FOREIGN KEY (album_id) REFERENCES album (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE book_file ADD CONSTRAINT FK_95027D1816A2B381 FOREIGN KEY (book_id) REFERENCES book (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE book_file ADD CONSTRAINT FK_95027D1893CB796C FOREIGN KEY (file_id) REFERENCES file (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE book_album DROP FOREIGN KEY FK_375A7B3A16A2B381');
        $this->addSql('ALTER TABLE book_album DROP FOREIGN KEY FK_375A7B3A1137ABCF');
        $this->addSql('ALTER TABLE book_file DROP FOREIGN KEY FK_95027D1816A2B381');
        $this->addSql('ALTER TABLE book_file DROP FOREIGN KEY FK_95027D1893CB796C');
        $this->addSql('DROP TABLE book_album');
        $this->addSql('DROP TABLE book_file');
    }
}
