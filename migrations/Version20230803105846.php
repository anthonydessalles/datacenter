<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230803105846 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE sport_link (sport_id INT NOT NULL, link_id INT NOT NULL, INDEX IDX_4173B70DAC78BCF8 (sport_id), INDEX IDX_4173B70DADA40271 (link_id), PRIMARY KEY(sport_id, link_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE sport_link ADD CONSTRAINT FK_4173B70DAC78BCF8 FOREIGN KEY (sport_id) REFERENCES sport (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE sport_link ADD CONSTRAINT FK_4173B70DADA40271 FOREIGN KEY (link_id) REFERENCES link (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE sport_link DROP FOREIGN KEY FK_4173B70DAC78BCF8');
        $this->addSql('ALTER TABLE sport_link DROP FOREIGN KEY FK_4173B70DADA40271');
        $this->addSql('DROP TABLE sport_link');
    }
}
