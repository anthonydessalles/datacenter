<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231202220741 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE score_file (score_id INT NOT NULL, file_id INT NOT NULL, INDEX IDX_A687B76612EB0A51 (score_id), INDEX IDX_A687B76693CB796C (file_id), PRIMARY KEY(score_id, file_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE score_file ADD CONSTRAINT FK_A687B76612EB0A51 FOREIGN KEY (score_id) REFERENCES score (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE score_file ADD CONSTRAINT FK_A687B76693CB796C FOREIGN KEY (file_id) REFERENCES file (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE score DROP FOREIGN KEY FK_3299375193CB796C');
        $this->addSql('DROP INDEX IDX_3299375193CB796C ON score');
        $this->addSql('ALTER TABLE score ADD tournament VARCHAR(255) DEFAULT NULL, ADD description LONGTEXT DEFAULT NULL, CHANGE file_id duration INT DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
    }
}
