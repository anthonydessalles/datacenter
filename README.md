Datacenter
=====================
Powered by [Symfony](https://symfony.com/) and [Docker](https://www.docker.com/).

Source : https://github.com/maxpou/docker-symfony 

Build
-----

### Dev
    make start
    make stop
The website is accessible at http://localhost/.

### Deploy
    git add -A
    git commit -m "v..."
    make deploy
The website is accessible at http://datacenter.anthony-dessalles.com.

### JWT
    mkdir config/jwt
    openssl genrsa -out config/jwt/private.pem -aes256 4096
    openssl rsa -pubout -in config/jwt/private.pem -out config/jwt/public.pem
    chmod -R 644 config/jwt/*
Source : https://emirkarsiyakali.com/implementing-jwt-authentication-to-your-api-platform-application-885f014d3358
