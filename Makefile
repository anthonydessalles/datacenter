FIG=docker-compose
HAS_DOCKER:=$(shell command -v $(FIG) 2> /dev/null)
ifdef HAS_DOCKER
	USERID=$(shell id -u)
	GROUPID=$(shell id -g)
	EXEC=$(FIG) exec -u $(USERID):$(GROUPID) php
else
	EXEC=
endif

start:
	docker-compose build
	docker-compose up -d

stop:
	docker-compose down

deploy:
	git push origin master
	git push gandi master
	ssh 63639@git.sd6.gpaas.net deploy datacenter.anthony-dessalles.com.git

exec:
	docker-compose exec php bash

sftp:
	sftp 63639@git.sd6.gpaas.net

generate:
	rm -rf prod_folder
	mkdir prod_folder
	mkdir prod_folder/bin
	mkdir prod_folder/var
	mkdir prod_folder/var/cache
	mkdir prod_folder/var/cache/prod
	mkdir prod_folder/config
	mkdir prod_folder/config/packages
	mkdir prod_folder/config/routes
	cp -R bin/console prod_folder/bin/console
	cp -R config/packages prod_folder/config
	cp -R config/routes prod_folder/config
	cp -R config/bundles.php prod_folder/config/bundles.php
	cp -R config/preload.php prod_folder/config/preload.php
	cp -R config/routes.yaml prod_folder/config/routes.yaml
	cp -R config/services.yaml prod_folder/config/services.yaml
	cp -R htdocs prod_folder/htdocs
	cp -R public prod_folder/public
	cp -R src prod_folder/src
	cp -R templates prod_folder/templates
	cp -R translations prod_folder/translations
	cp -R composer.json prod_folder/composer.json

map:
	rm -f map.txt
	$(EXEC) date >> map.txt
	$(EXEC) composer show >> map.txt
	$(EXEC) php bin/console >> map.txt
	$(EXEC) php bin/console debug:router >> map.txt
