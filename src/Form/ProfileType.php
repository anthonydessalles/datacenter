<?php

namespace App\Form;

use App\Entity\Link;
use App\Entity\Playlist;
use App\Entity\Profile;
use App\Entity\Sport;
use App\Entity\Type;
use App\Entity\Score;
use App\Entity\Recording;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

class ProfileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)
            ->add('ofTheYear', NumberType::class, [
                'required' => false,
            ])
            ->add('types', Select2EntityType::class, [
                'multiple' => true,
                'remote_route' => 'ajax_types',
                'class' => Type::class,
                'primary_key' => 'id',
                'text_property' => 'slug',
                'placeholder' => 'Select a type'
            ])
            ->add('scores', Select2EntityType::class, [
                'multiple' => true,
                'remote_route' => 'ajax_scores',
                'class' => Score::class,
                'primary_key' => 'id',
                'text_property' => 'id',
                'placeholder' => 'Select a score'
            ])
            ->add('recordings', Select2EntityType::class, [
                'multiple' => true,
                'remote_route' => 'ajax_recordings',
                'class' => Recording::class,
                'primary_key' => 'id',
                'text_property' => 'name',
                'placeholder' => 'Select a recording'
            ])
            ->add('sports', Select2EntityType::class, [
                'multiple' => true,
                'remote_route' => 'ajax_sports',
                'class' => Sport::class,
                'primary_key' => 'id',
                'text_property' => 'title',
                'placeholder' => 'Select a sport'
            ])
            ->add('playlists', Select2EntityType::class, [
                'multiple' => true,
                'remote_route' => 'ajax_playlists',
                'class' => Playlist::class,
                'primary_key' => 'id',
                'text_property' => 'title',
                'placeholder' => 'Select a playlist'
            ])
            ->add('links', Select2EntityType::class, [
                'multiple' => true,
                'remote_route' => 'ajax_links',
                'class' => Link::class,
                'primary_key' => 'id',
                'text_property' => 'url',
                'placeholder' => 'Select a link'
            ])
            ->add('save', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Profile::class,
        ));
    }
}
