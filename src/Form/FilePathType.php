<?php

namespace App\Form;

use App\Entity\File;
use App\Entity\Game;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FilePathType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $path = $options['path'];

        $builder
            ->add('date', DateTimeType::class, [
                'date_widget' => 'single_text',
                'time_widget' => 'text',
                'required' => true,
            ])
            ->add('files', EntityType::class, [
                'class' => File::class,
                'multiple' => true,
                'query_builder' => function (EntityRepository $er) use ($path) {
                    return $er->createQueryBuilder('f')
                        ->where('f.path = :path')
                        ->setParameter('path', $path)
                        ->orderBy('f.filename', 'ASC');
                },
            ])
            ->add('save', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired('path');
    }
}