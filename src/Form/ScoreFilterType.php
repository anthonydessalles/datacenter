<?php

namespace App\Form;

use App\Entity\Type;
use App\Entity\Game;
use App\Entity\Score;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use App\Repository\ScoreRepository;

class ScoreFilterType extends AbstractType
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var ScoreRepository $scoreRepository */
        $scoreRepository = $this->entityManager->getRepository(Score::class);

        $tournaments = [];
        foreach ($scoreRepository->getScoreTournaments() as $data) {
            $tournaments[$data['tournament']] = $data['tournament'];
        }

        $descriptions = [];
        foreach ($scoreRepository->getScoreDescriptions() as $data) {
            $descriptions[$data['description']] = $data['description'];
        }

        $difficulties = [];
        foreach ($scoreRepository->getScoreDifficulties() as $data) {
            $difficulties[$data['difficulty']] = $data['difficulty'];
        }

        $builder
            ->add('game', EntityType::class, [
                'class' => Game::class,
                'multiple' => false,
                'required' => false,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('g')
                        ->orderBy('g.name', 'ASC');
                },
            ])
            ->add('type', EntityType::class, [
                'class' => Type::class,
                'multiple' => false,
                'required' => false,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('t')
                        ->orderBy('t.slug', 'ASC');
                },
            ])
            ->add('tournament', ChoiceType::class, [
                'choices'  => $tournaments,
                'required' => false,
            ])
            ->add('description', ChoiceType::class, [
                'choices'  => $descriptions,
                'required' => false,
            ])
            ->add('difficulty', ChoiceType::class, [
                'choices'  => $difficulties,
                'required' => false,
            ])
            ->add('filter', SubmitType::class)
        ;
    }
}
