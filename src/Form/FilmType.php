<?php

namespace App\Form;

use App\Entity\Film;
use App\Entity\Type;
use Cocur\Slugify\Slugify;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;
use App\Entity\File;

class FilmType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class)
            ->add('year', NumberType::class)
            ->add('ofTheYear', NumberType::class, [
                'required' => false,
            ])
            ->add('enTitle', TextType::class, [
                'required' => false,
            ])
            ->add('frTitle', TextType::class, [
                'required' => false,
            ])
            ->add('allocine', NumberType::class, [
                'required' => false,
            ])
            ->add('imdb', TextType::class, [
                'required' => false,
            ])
            ->add('types', Select2EntityType::class, [
                'multiple' => true,
                'remote_route' => 'ajax_types',
                'class' => Type::class,
                'primary_key' => 'id',
                'text_property' => 'slug',
                'placeholder' => 'Select a type'
            ])
            ->add('files', Select2EntityType::class, [
                'multiple' => true,
                'remote_route' => 'ajax_files',
                'class' => File::class,
                'primary_key' => 'id',
                'text_property' => 'filename',
                'placeholder' => 'Select a file'
            ])
            ->add('save', SubmitType::class)
            ->addEventListener(
                FormEvents::POST_SUBMIT,
                [$this, 'onPostSubmit']
            )
        ;
    }

    public function onPostSubmit(FormEvent $event)
    {
        /** @var Film $film */
        $film = $event->getData();

        $slugify = new Slugify();
        $film->setSlug($slugify->slugify($film->getTitle()));

        $film->setUpdatedAt(new \DateTime('now'));

        if (null === $film->getCreatedAt()) {
            $film->setCreatedAt(new \DateTime('now'));
        }

        $film->setMaxHeight(null);
        $film->setMaxWidth(null);
        $formats = [];
        foreach($film->getTypes() as $type){
            if (Type::DVD == $type || Type::BLU_RAY == $type ) {
                $formats[] = $type;
            }
        }

        if (in_array(Type::DVD, $formats)) {
            $film->setMaxHeight(576);
            $film->setMaxWidth(720);
        }

        if (in_array(Type::BLU_RAY, $formats)) {
            $film->setMaxHeight(1080);
            $film->setMaxWidth(1920);
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Film::class,
        ));
    }
}
