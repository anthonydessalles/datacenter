<?php

namespace App\Form;

use App\Entity\Link;
use App\Entity\Album;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use App\Entity\Type;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

class TypeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('slug', TextType::class)
            ->add('family', TextType::class, [
                'required' => false,
            ])
            ->add('links', Select2EntityType::class, [
                'multiple' => true,
                'remote_route' => 'ajax_links',
                'class' => Link::class,
                'primary_key' => 'id',
                'text_property' => 'url',
                'placeholder' => 'Select a link'
            ])
            ->add('albums', Select2EntityType::class, [
                'multiple' => true,
                'remote_route' => 'ajax_albums',
                'class' => Album::class,
                'primary_key' => 'id',
                'text_property' => 'title',
                'placeholder' => 'Select an album'
            ])
            ->add('save', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Type::class,
        ));
    }
}
