<?php

namespace App\Form;

use App\Entity\File;
use App\Entity\Playlist;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;
use App\Entity\Progression;
use App\Entity\Game;
use App\Entity\Type;

class ProgressionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)
            ->add('confrontation', TextType::class, [
                'required'  => false,
            ])
            ->add('difficulty', TextType::class, [
                'required'  => false,
            ])
            ->add('description', TextareaType::class, [
            	'required'  => false,
        	])
            ->add('game', Select2EntityType::class, [
                'required' => false,
                'multiple' => false,
                'remote_route' => 'ajax_games',
                'class' => Game::class,
                'primary_key' => 'id',
                'text_property' => 'name',
                'placeholder' => 'Select a game'
            ])
            ->add('type', Select2EntityType::class, [
                'required' => true,
                'multiple' => false,
                'remote_route' => 'ajax_types',
                'class' => Type::class,
                'primary_key' => 'id',
                'text_property' => 'slug',
                'placeholder' => 'Select a type'
            ])
            ->add('users', Select2EntityType::class, [
                'multiple' => true,
                'remote_route' => 'ajax_users',
                'class' => User::class,
                'primary_key' => 'id',
                'text_property' => 'username',
                'placeholder' => 'Select a user'
            ])
            ->add('file', Select2EntityType::class, [
                'required' => false,
                'multiple' => false,
                'remote_route' => 'ajax_files',
                'class' => File::class,
                'primary_key' => 'id',
                'text_property' => 'filename',
                'placeholder' => 'Select a file'
            ])
            ->add('videos', Select2EntityType::class, [
                'required' => false,
                'multiple' => true,
                'remote_route' => 'ajax_videos',
                'class' => File::class,
                'primary_key' => 'id',
                'text_property' => 'filename',
                'placeholder' => 'Select videos'
            ])
            ->add('save', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Progression::class,
        ));
    }
}
