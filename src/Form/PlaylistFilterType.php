<?php

namespace App\Form;

use App\Entity\Game;
use App\Entity\Playlist;
use App\Entity\Source;
use App\Repository\PlaylistRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class PlaylistFilterType extends AbstractType
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var PlaylistRepository $playlistRepository */
        $playlistRepository = $this->entityManager->getRepository(Playlist::class);

        $status = [];
        foreach ($playlistRepository->getPlaylistsStatus() as $data) {
            $status[$data['status']] = $data['status'];
        }

        $builder
            ->add('status', ChoiceType::class, [
                'choices'  => $status,
            ])
            ->add('game', EntityType::class, [
                'class' => Game::class,
                'multiple' => false,
                'required' => false,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('g')
                        ->orderBy('g.name', 'ASC');
                },
            ])
            ->add('source', EntityType::class, [
                'class' => Source::class,
                'multiple' => false,
                'required' => false,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('s')
                        ->orderBy('s.name', 'ASC');
                },
            ])
            ->add('filter', SubmitType::class)
        ;
    }
}