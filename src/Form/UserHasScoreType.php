<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Entity\Score;
use App\Entity\User;
use App\Repository\ScoreRepository;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use App\Entity\File;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

class UserHasScoreType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('isHome', CheckboxType::class, [
                'required' => false,
            ])
            ->add('isWin', CheckboxType::class, [
                'required' => false,
            ])
            ->add('isDraw', CheckboxType::class, [
                'required' => false,
            ])
            ->add('isLose', CheckboxType::class, [
                'required' => false,
            ])
            ->add('points', NumberType::class, [
                'required' => false,
            ])
            ->add('assists', NumberType::class, [
                'required' => false,
            ])
            ->add('saves', NumberType::class, [
                'required' => false,
            ])
            ->add('gameTime', NumberType::class, [
                'required' => false,
            ])
            ->add('distance', NumberType::class, [
                'required' => false,
            ])
            ->add('sprints', NumberType::class, [
                'required' => false,
            ])
            ->add('activity', NumberType::class, [
                'required' => false,
            ])
            ->add('maxSprint', NumberType::class, [
                'required' => false,
            ])
            ->add('averageSprint', NumberType::class, [
                'required' => false,
            ])
            ->add('maxShot', NumberType::class, [
                'required' => false,
            ])
            ->add('runningTime', TextType::class, [
                'required' => false,
            ])
            ->add('score', EntityType::class, [
                'class' => Score::class,
                'choice_label' => function(Score $score, $key, $index) {
                    $scoreName = $score->getActivity()
                      ? $score->getActivity()->getName() . ' ' . $score->getActivity()->getStart()->format('d/m/Y H:i')
                      : $score->getHome() . ' ' . $score->getAway();

                    return $scoreName;
                },
            ])
            ->add('user', EntityType::class, [
            	'class' => User::class,
            	'choice_label' => 'username',
        	])
            ->add('file', Select2EntityType::class, [
                'required' => false,
                'multiple' => false,
                'remote_route' => 'ajax_files',
                'class' => File::class,
                'primary_key' => 'id',
                'text_property' => 'filename',
                'placeholder' => 'Select a file'
            ])
            ->add('save', SubmitType::class)
        ;
    }
}
