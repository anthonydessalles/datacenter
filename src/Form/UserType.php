<?php

namespace App\Form;

use App\Entity\Album;
use App\Entity\Timeline;
use App\Entity\File;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use App\Entity\User;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)
            ->add('surname', TextType::class)
            ->add('username', TextType::class)
            ->add('gamername', TextType::class, [
                'required' => false,
            ])
            ->add('email', TextType::class, [
                'required' => false,
            ])
            ->add('role', TextType::class, [
                'required' => false,
            ])
            ->add('association', TextType::class, [
                'required' => false,
            ])
            ->add('youtubeEmbedId', TextType::class, [
                'required' => false,
            ])
            ->add('slides', TextType::class, [
                'required' => false,
            ])
            ->add('albums', Select2EntityType::class, [
                'multiple' => true,
                'remote_route' => 'ajax_albums',
                'class' => Album::class,
                'primary_key' => 'id',
                'text_property' => 'title',
                'placeholder' => 'Select an album'
            ])
            ->add('timelines', Select2EntityType::class, [
                'multiple' => true,
                'remote_route' => 'ajax_timelines',
                'class' => Timeline::class,
                'primary_key' => 'id',
                'text_property' => 'name',
                'placeholder' => 'Select a timeline'
            ])
            ->add('files', Select2EntityType::class, [
                'multiple' => true,
                'remote_route' => 'ajax_files',
                'class' => File::class,
                'primary_key' => 'id',
                'text_property' => 'filename',
                'placeholder' => 'Select a file'
            ])
            ->add('save', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => User::class,
        ));
    }
}
