<?php

namespace App\Form;

use App\Entity\Book;
use App\Entity\Device;
use App\Entity\Link;
use App\Entity\Type;
use Cocur\Slugify\Slugify;
use Doctrine\DBAL\Types\BooleanType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;
use App\Entity\File;

class DeviceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class)
            ->add('purchase', TextType::class, [
                'required' => false,
            ])
            ->add('price', NumberType::class, [
                'required' => false,
            ])
            ->add('number', NumberType::class)
            ->add('year', NumberType::class, [
                'required' => false,
            ])
            ->add('generation', NumberType::class, [
                'required' => false,
            ])
            ->add('description', TextareaType::class, [
                'required' => false,
            ])
            ->add('bugs', TextType::class, [
                'required' => false,
            ])
            ->add('types', Select2EntityType::class, [
                'multiple' => true,
                'remote_route' => 'ajax_types',
                'class' => Type::class,
                'primary_key' => 'id',
                'text_property' => 'slug',
                'placeholder' => 'Select a type'
            ])
            ->add('links', Select2EntityType::class, [
                'multiple' => true,
                'remote_route' => 'ajax_links',
                'class' => Link::class,
                'primary_key' => 'id',
                'text_property' => 'url',
                'placeholder' => 'Select a link'
            ])
            ->add('files', Select2EntityType::class, [
                'multiple' => true,
                'remote_route' => 'ajax_files',
                'class' => File::class,
                'primary_key' => 'id',
                'text_property' => 'filename',
                'placeholder' => 'Select a file'
            ])
            ->add('isEnabled', CheckboxType::class, [
                'required' => false,
            ])
            ->add('save', SubmitType::class)
            ->addEventListener(
                FormEvents::POST_SUBMIT,
                [$this, 'onPostSubmit']
            )
        ;
    }

    public function onPostSubmit(FormEvent $event)
    {
        /** @var Device $device */
        $device = $event->getData();

        $slugify = new Slugify();
        $device->setSlug($slugify->slugify($device->getTitle()));

        $device->setUpdatedAt(new \DateTime('now'));

        if (null === $device->getCreatedAt()) {
            $device->setCreatedAt(new \DateTime('now'));
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Device::class,
        ));
    }
}