<?php

namespace App\Form;

use App\Entity\Sport;
use App\Entity\Type;
use Cocur\Slugify\Slugify;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;
use App\Entity\File;
use App\Entity\Link;

class SportType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class)
            ->add('year', NumberType::class)
            ->add('enTitle', TextType::class, [
                'required' => false,
            ])
            ->add('frTitle', TextType::class, [
                'required' => false,
            ])
            ->add('types', Select2EntityType::class, [
                'multiple' => true,
                'remote_route' => 'ajax_types',
                'class' => Type::class,
                'primary_key' => 'id',
                'text_property' => 'slug',
                'placeholder' => 'Select a type'
            ])
            ->add('links', Select2EntityType::class, [
                'multiple' => true,
                'remote_route' => 'ajax_links',
                'class' => Link::class,
                'primary_key' => 'id',
                'text_property' => 'url',
                'placeholder' => 'Select a link'
            ])
            ->add('files', Select2EntityType::class, [
                'multiple' => true,
                'remote_route' => 'ajax_files',
                'class' => File::class,
                'primary_key' => 'id',
                'text_property' => 'filename',
                'placeholder' => 'Select a file'
            ])
            ->add('save', SubmitType::class)
            ->addEventListener(
                FormEvents::POST_SUBMIT,
                [$this, 'onPostSubmit']
            )
        ;
    }

    public function onPostSubmit(FormEvent $event)
    {
        /** @var Sport $sport */
        $sport = $event->getData();

        $slugify = new Slugify();
        $sport->setSlug($slugify->slugify($sport->getTitle()));

        $sport->setUpdatedAt(new \DateTime('now'));

        if (null === $sport->getCreatedAt()) {
            $sport->setCreatedAt(new \DateTime('now'));
        }

        $sport->setMaxHeight(null);
        $sport->setMaxWidth(null);
        $formats = [];
        foreach($sport->getTypes() as $type){
            if (Type::DVD == $type || Type::BLU_RAY == $type ) {
                $formats[] = $type;
            }
        }

        if (in_array(Type::DVD, $formats)) {
            $sport->setMaxHeight(576);
            $sport->setMaxWidth(720);
        }

        if (in_array(Type::BLU_RAY, $formats)) {
            $sport->setMaxHeight(1080);
            $sport->setMaxWidth(1920);
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Sport::class,
        ));
    }
}
