<?php

namespace App\Form;

use App\Entity\Album;
use App\Entity\Place;
use App\Entity\Source;
use App\Entity\Type;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Entity\Activity;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

class ActivityType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)
            ->add('start', DateTimeType::class, [
                'date_widget' => 'text',
                'time_widget' => 'text',
            ])
            ->add('end', DateTimeType::class, [
                'date_widget' => 'text',
                'time_widget' => 'text',
            ])
            ->add('description', TextareaType::class, [
                'required'  => false,
            ])
            ->add('type', EntityType::class, [
              	'class' => Type::class,
                  'query_builder' => function (EntityRepository $er) {
                      return $er->createQueryBuilder('t')->orderBy('t.slug', 'ASC');
                  },
              	'choice_label' => 'slug',
                  'required' => false,
          	])
            ->add('place', EntityType::class, [
              	'class' => Place::class,
                  'query_builder' => function (EntityRepository $er) {
                      return $er->createQueryBuilder('p')->orderBy('p.name', 'ASC');
                  },
              	'choice_label' => 'name',
                  'required' => false,
          	])
            ->add('users', Select2EntityType::class, [
                'multiple' => true,
                'remote_route' => 'ajax_users',
                'class' => User::class,
                'primary_key' => 'id',
                'text_property' => 'username',
                'placeholder' => 'Select a user'
            ])
            ->add('albums', Select2EntityType::class, [
                'multiple' => true,
                'remote_route' => 'ajax_albums',
                'class' => Album::class,
                'primary_key' => 'id',
                'text_property' => 'title',
                'placeholder' => 'Select an album'
            ])
            ->add('source', EntityType::class, [
                'class' => Source::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('s')->orderBy('s.name', 'ASC');
                },
                'choice_label' => 'name',
                'required' => true,
            ])
            ->add('score', ScoreType::class, [
                'required'  => false,
            ])
            ->add('save', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Activity::class,
        ]);
    }
}
