<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use App\Entity\Score;
use App\Entity\Game;
use App\Entity\Type;
use App\Entity\File;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

class ScoreType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('home', TextType::class, [
                'required'  => false,
            ])
            ->add('away', TextType::class, [
                'required'  => false,
            ])
            ->add('homeAwayDifference', NumberType::class, [
                'required'  => false,
            ])
            ->add('homePoints', NumberType::class, [
                'required'  => false,
            ])
            ->add('awayPoints', NumberType::class, [
                'required'  => false,
            ])
            ->add('homePlayers', NumberType::class, [
                'required'  => false,
            ])
            ->add('awayPlayers', NumberType::class, [
                'required'  => false,
            ])
            ->add('duration', TextType::class, [
                'required'  => false,
            ])
            ->add('tournament', TextType::class, [
                'required'  => false,
            ])
            ->add('description', TextType::class, [
                'required'  => false,
            ])
            ->add('difficulty', TextType::class, [
                'required'  => false,
            ])
            ->add('game', Select2EntityType::class, [
                'required' => false,
                'multiple' => false,
                'remote_route' => 'ajax_games',
                'class' => Game::class,
                'primary_key' => 'id',
                'text_property' => 'name',
                'placeholder' => 'Select a game'
            ])
            ->add('type', Select2EntityType::class, [
                'required' => false,
                'multiple' => false,
                'remote_route' => 'ajax_types',
                'class' => Type::class,
                'primary_key' => 'id',
                'text_property' => 'slug',
                'placeholder' => 'Select a type'
            ])
            ->add('files', Select2EntityType::class, [
                'multiple' => true,
                'remote_route' => 'ajax_files',
                'class' => File::class,
                'primary_key' => 'id',
                'text_property' => 'filename',
                'placeholder' => 'Select a file'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Score::class,
        ));
    }
}
