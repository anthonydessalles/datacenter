<?php

namespace App\Form;

use App\Entity\Album;
use App\Entity\File;
use App\Entity\Media;
use App\Entity\Playlist;
use App\Entity\Type;
use App\Entity\User;
use App\Entity\Screenshot;
use Cocur\Slugify\Slugify;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use App\Entity\Game;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

class GameType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('igdbId', NumberType::class, [
                'required' => false,
            ])
            ->add('igdbUrl', TextType::class, [
                'required' => false,
            ])
            ->add('name', TextType::class)
            ->add('frenchName', TextType::class, [
                'required' => false,
            ])
            ->add('date', DateTimeType::class, [
                'date_widget' => 'single_text',
                'time_widget' => 'text',
            ])
            ->add('difficulties', TextType::class, [
                'required' => false,
            ])
            ->add('ofTheYear', NumberType::class, [
                'required' => false,
            ])
            ->add('types', Select2EntityType::class, [
                'multiple' => true,
                'remote_route' => 'ajax_types',
                'class' => Type::class,
                'primary_key' => 'id',
                'text_property' => 'slug',
                'placeholder' => 'Select a type'
            ])
            ->add('suggestion', Select2EntityType::class, [
                'required' => false,
                'multiple' => false,
                'remote_route' => 'ajax_games',
                'class' => Game::class,
                'primary_key' => 'id',
                'text_property' => 'name',
                'placeholder' => 'Select a game'
            ])
            ->add('albums', Select2EntityType::class, [
                'multiple' => true,
                'remote_route' => 'ajax_albums',
                'class' => Album::class,
                'primary_key' => 'id',
                'text_property' => 'title',
                'placeholder' => 'Select an album'
            ])
            ->add('files', Select2EntityType::class, [
                'multiple' => true,
                'remote_route' => 'ajax_files',
                'class' => File::class,
                'primary_key' => 'id',
                'text_property' => 'filename',
                'placeholder' => 'Select a file'
            ])
            ->add('playlists', Select2EntityType::class, [
                'multiple' => true,
                'remote_route' => 'ajax_playlists',
                'class' => Playlist::class,
                'primary_key' => 'id',
                'text_property' => 'title',
                'placeholder' => 'Select a playlist'
            ])
            ->add('screenshots', Select2EntityType::class, [
                'multiple' => true,
                'remote_route' => 'ajax_screenshots',
                'class' => Screenshot::class,
                'primary_key' => 'id',
                'text_property' => 'title',
                'placeholder' => 'Select a screenshot'
            ])
            ->add('users', Select2EntityType::class, [
                'multiple' => true,
                'remote_route' => 'ajax_users',
                'class' => User::class,
                'primary_key' => 'id',
                'text_property' => 'username',
                'placeholder' => 'Select a user'
            ])
            ->add('isEnabled', CheckboxType::class, [
                'required' => false,
            ])
            ->add('save', SubmitType::class)
            ->addEventListener(
                FormEvents::POST_SUBMIT,
                [$this, 'onPostSubmit']
            )
        ;
    }

    public function onPostSubmit(FormEvent $event)
    {
        /** @var Game $game */
        $game = $event->getData();

        $slugify = new Slugify();
        $game->setSlug($slugify->slugify($game->getName()));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Game::class,
        ));
    }
}
