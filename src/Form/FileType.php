<?php

namespace App\Form;

use App\Entity\File;
use App\Entity\Source;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class FileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('position', IntegerType::class, [
                'required' => false,
            ])
            ->add('filename', TextType::class)
            ->add('path', TextType::class, [
                'required' => false,
            ])
            ->add('date', DateTimeType::class, [
                'date_widget' => 'text',
                'time_widget' => 'text',
            ])
            ->add('embedHtml', TextType::class, [
                'required' => false,
            ])
            ->add('description', TextType::class, [
                'required' => false,
            ])
            ->add('size', TextType::class, [
                'required' => false,
            ])
            ->add('external_id', TextType::class, [
                'required' => false,
            ])
            ->add('height', IntegerType::class, [
                'required' => false,
            ])
            ->add('width', IntegerType::class, [
                'required' => false,
            ])
            ->add('duration', IntegerType::class, [
                'required' => false,
            ])
            ->add('source_url', TextType::class, [
                'required' => false,
            ])
            ->add('source', EntityType::class, [
                'class' => Source::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('s')->orderBy('s.name', 'ASC');
                },
                'choice_label' => 'name',
                'required' => true,
            ])
            ->add('save', SubmitType::class)
            ->addEventListener(
                FormEvents::POST_SUBMIT,
                [$this, 'onPostSubmit']
            )
        ;
    }

    public function onPostSubmit(FormEvent $event)
    {
        /** @var File $file */
        $file = $event->getData();

        $file->setUpdatedAt(new \DateTime('now'));

        if (null === $file->getCreatedAt()) {
            $file->setCreatedAt(new \DateTime('now'));
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => File::class,
        ));
    }
}
