<?php

namespace App\Form;

use App\Entity\File;
use App\Entity\Game;
use App\Entity\Source;
use App\Repository\FileRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class FileFilterType extends AbstractType
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var FileRepository $fileRepository */
        $fileRepository = $this->entityManager->getRepository(File::class);

        $paths = [];
        foreach ($fileRepository->getFilesPaths() as $data) {
            $paths[$data['path']] = $data['path'];
        }

        $builder
            ->add('source', EntityType::class, [
                'class' => Source::class,
                'multiple' => false,
                'required' => false,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('s')
                        ->orderBy('s.name', 'ASC');
                },
            ])
            ->add('game', EntityType::class, [
                'class' => Game::class,
                'multiple' => false,
                'required' => false,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('g')
                        ->orderBy('g.name', 'ASC');
                },
            ])
            ->add('path', ChoiceType::class, [
                'choices'  => $paths,
                'required' => false,
            ])
            ->add('max', IntegerType::class, [
                'data'  => 3000,
                'required' => true,
            ])
            ->add('filter', SubmitType::class)
        ;
    }
}
