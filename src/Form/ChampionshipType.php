<?php

namespace App\Form;

use App\Entity\Championship;
use App\Entity\Game;
use App\Entity\Link;
use App\Entity\Profile;
use App\Entity\Type;
use App\Entity\File;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

class ChampionshipType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class)
            ->add('holder', TextType::class)
            ->add('mode', TextType::class, [
                'required' => false,
            ])
            ->add('game', EntityType::class, [
                'class' => Game::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('t')->orderBy('t.name', 'ASC');
                },
                'choice_label' => 'name',
                'required' => true,
            ])
            ->add('type', EntityType::class, [
                'class' => Type::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('t')->orderBy('t.slug', 'ASC');
                },
                'choice_label' => 'slug',
                'required' => true,
            ])
            ->add('link', EntityType::class, [
                'class' => Link::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('t')->orderBy('t.url', 'ASC');
                },
                'choice_label' => 'url',
                'required' => true,
            ])
            ->add('profile', EntityType::class, [
                'class' => Profile::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('t')->orderBy('t.name', 'ASC');
                },
                'choice_label' => 'name',
                'required' => true,
            ])
            ->add('file', Select2EntityType::class, [
                'required' => false,
                'multiple' => false,
                'remote_route' => 'ajax_files',
                'class' => File::class,
                'primary_key' => 'id',
                'text_property' => 'filename',
                'placeholder' => 'Select a file'
            ])
            ->add('save', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Championship::class,
        ));
    }
}
