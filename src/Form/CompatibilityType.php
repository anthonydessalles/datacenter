<?php

namespace App\Form;

use App\Entity\Compatibility;
use App\Entity\Device;
use App\Entity\Game;
use App\Entity\Type;
use App\Entity\User;
use App\Entity\File;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Security\Core\Security;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

class CompatibilityType extends AbstractType
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('storage', ChoiceType::class, [
                'choices'  => [
                    'Internal' => 'Internal',
                    'External' => 'External',
                    'Disk' => 'Disk',
                    'Cartridge' => 'Cartridge',
                ],
            ])
            ->add('status', ChoiceType::class, [
                'choices'  => [
                    'Perfect (flawless with no audio or graphical glitches)' => 'Perfect',
                    'Great (minor graphical or audio glitches, playable)' => 'Great',
                    'Okay (major graphical or audio glitches, playable)' => 'Okay',
                    'Bad (major graphical or audio glitches, unable to play until the end)' => 'Bad',
                ],
            ])
            ->add('software', TextType::class, [
                'required' => false,
            ])
            ->add('game', Select2EntityType::class, [
                'required' => true,
                'multiple' => false,
                'remote_route' => 'ajax_games',
                'class' => Game::class,
                'primary_key' => 'id',
                'text_property' => 'name',
                'placeholder' => 'Select a game'
            ])
            ->add('type', Select2EntityType::class, [
                'required' => true,
                'multiple' => false,
                'remote_route' => 'ajax_types',
                'class' => Type::class,
                'primary_key' => 'id',
                'text_property' => 'slug',
                'placeholder' => 'Select a type'
            ])
            ->add('device', Select2EntityType::class, [
                'required' => true,
                'multiple' => false,
                'remote_route' => 'ajax_devices',
                'class' => Device::class,
                'primary_key' => 'id',
                'text_property' => 'title',
                'placeholder' => 'Select a device'
            ])
            ->add('files', Select2EntityType::class, [
                'multiple' => true,
                'remote_route' => 'ajax_files',
                'class' => File::class,
                'primary_key' => 'id',
                'text_property' => 'filename',
                'placeholder' => 'Select a file'
            ])
            ->add('save', SubmitType::class)
            ->addEventListener(
                FormEvents::POST_SUBMIT,
                [$this, 'onPostSubmit']
            )
        ;
    }

    public function onPostSubmit(FormEvent $event)
    {
        $now = new \DateTime('now');

        /** @var Compatibility $compatibility */
        $compatibility = $event->getData();

        /** @var User $user */
        $user = $this->security->getUser();

        $compatibility->setUpdatedAt($now);
        $compatibility->setUpdatedBy($user);

        if (null === $compatibility->getCreatedAt()) {
            $compatibility->setCreatedAt($now);
        }

        if (null === $compatibility->getCreator()) {
            $compatibility->setCreator($user);
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Compatibility::class,
        ));
    }
}
