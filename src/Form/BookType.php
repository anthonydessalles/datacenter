<?php

namespace App\Form;

use App\Entity\Book;
use App\Entity\Type;
use Cocur\Slugify\Slugify;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;
use App\Entity\File;
use App\Entity\Film;

class BookType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class)
            ->add('releaseDate', TextType::class)
            ->add('pagination', NumberType::class)
            ->add('isbn', TextType::class, [
                'required' => false,
            ])
            ->add('synopsis', TextType::class, [
                'required' => false,
            ])
            ->add('price', NumberType::class)
            ->add('types', Select2EntityType::class, [
                'multiple' => true,
                'remote_route' => 'ajax_types',
                'class' => Type::class,
                'primary_key' => 'id',
                'text_property' => 'slug',
                'placeholder' => 'Select a type'
            ])
            ->add('files', Select2EntityType::class, [
                'multiple' => true,
                'remote_route' => 'ajax_files',
                'class' => File::class,
                'primary_key' => 'id',
                'text_property' => 'filename',
                'placeholder' => 'Select a file'
            ])
            ->add('films', Select2EntityType::class, [
                'multiple' => true,
                'remote_route' => 'ajax_films',
                'class' => Film::class,
                'primary_key' => 'id',
                'text_property' => 'title',
                'placeholder' => 'Select a film'
            ])
            ->add('save', SubmitType::class)
            ->addEventListener(
                FormEvents::POST_SUBMIT,
                [$this, 'onPostSubmit']
            )
        ;
    }

    public function onPostSubmit(FormEvent $event)
    {
        /** @var Book $book */
        $book = $event->getData();

        $slugify = new Slugify();
        $book->setSlug($slugify->slugify($book->getTitle()));

        $book->setUpdatedAt(new \DateTime('now'));

        if (null === $book->getCreatedAt()) {
            $book->setCreatedAt(new \DateTime('now'));
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Book::class,
        ));
    }
}