<?php

namespace App\Form;

use App\Entity\Game;
use App\Entity\Type;
use App\Entity\Championship;
use App\Entity\Source;
use App\Repository\ChampionshipRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class ChampionshipFilterType extends AbstractType
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var ChampionshipRepository $championshipRepository */
        $championshipRepository = $this->entityManager->getRepository(Championship::class);

        $titles = [];
        foreach ($championshipRepository->getChampionshipsTitles() as $data) {
            $titles[$data['title']] = $data['title'];
        }

        $holders = [];
        foreach ($championshipRepository->getChampionshipsHolders() as $data) {
            $holders[$data['holder']] = $data['holder'];
        }

        $builder
            ->add('title', ChoiceType::class, [
                'choices'  => $titles,
                'required' => false,
            ])
            ->add('holder', ChoiceType::class, [
                'choices'  => $holders,
                'required' => false,
            ])
            ->add('game', EntityType::class, [
                'class' => Game::class,
                'multiple' => false,
                'required' => false,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('g')
                        ->orderBy('g.name', 'ASC');
                },
            ])
            ->add('type', EntityType::class, [
                'class' => Type::class,
                'multiple' => false,
                'required' => false,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('t')
                        ->orderBy('t.slug', 'ASC');
                },
            ])
            ->add('filter', SubmitType::class)
        ;
    }
}
