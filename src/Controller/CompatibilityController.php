<?php

namespace App\Controller;

use App\Entity\Compatibility;
use App\Form\CompatibilityType;
use App\Repository\CompatibilityRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CompatibilityController extends AbstractController
{
    private function handlingForm(Compatibility $compatibility, Request $request)
    {
        $form = $this->createForm(CompatibilityType::class, $compatibility);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($compatibility);
            $em->flush();

            return $this->redirectToRoute('compatibilities');
        }

        return [
            'form' => $form->createView(),
            'compatibility' => $compatibility,
        ];
    }

    /**
     * @Route("/compatibility/add", name="add_compatibility")
     * @Template("compatibilities/compatibility_form.html.twig")
     */
    public function addCompatibilityAction(Request $request)
    {
        return $this->handlingForm(new Compatibility(), $request);
    }

    /**
     * @Route("/compatibilities/{id}/edit", name="edit_compatibility")
     * @Template("compatibilities/compatibility_form.html.twig")
     */
    public function editCompatibilityAction(Compatibility $compatibility, Request $request)
    {
        return $this->handlingForm($compatibility, $request);
    }

    /**
     * @Route("/compatibilities", name="compatibilities")
     * @Template("compatibilities/compatibilities.html.twig")
     */
    public function compatibilitiesAction(CompatibilityRepository $compatiblityRepository): array
    {
        return [
            'compatibilities' => $compatiblityRepository->getCompatibilities(),
            'count' => count($compatiblityRepository->getCompatibilities()),
        ];
    }
}
