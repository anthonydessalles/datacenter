<?php

namespace App\Controller;

use App\Entity\Book;
use App\Form\BookType;
use App\Repository\BookRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class BookController extends AbstractController
{
    private function handlingForm(Book $book, Request $request)
    {
        $form = $this->createForm(BookType::class, $book);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($book);
            $em->flush();

            return $this->redirectToRoute('books');
        }

        return [
            'form' => $form->createView(),
            'book' => $book,
        ];
    }

    /**
     * @Route("/book/add", name="add_book")
     * @Template("books/book_form.html.twig")
     */
    public function addBookAction(Request $request)
    {
        return $this->handlingForm(new Book(), $request);
    }

    /**
     * @Route("/books/{id}/edit", name="edit_book")
     * @Template("books/book_form.html.twig")
     */
    public function editBookAction(Book $book, Request $request)
    {
        return $this->handlingForm($book, $request);
    }

    /**
     * @Route("/books", name="books")
     * @Template("books/books.html.twig")
     * @param BookRepository $bookRepository
     * @return array
     */
    public function booksAction(BookRepository $bookRepository)
    {
        return [
            'books' => $bookRepository->getBooks(),
            'count' => count($bookRepository->getBooks()),
        ];
    }
}
