<?php

namespace App\Controller;

use App\Entity\Place;
use App\Form\PlaceType;
use App\Repository\PlaceRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class PlaceController extends AbstractController
{
    private function handlingForm(Place $place, Request $request)
    {
        $form = $this->createForm(PlaceType::class, $place);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($place);
            $em->flush();

            return $this->redirectToRoute('places');
        }

        return [
            'form' => $form->createView(),
            'place' => $place,
        ];
    }

    /**
     * @Route("/places/add", name="add_place")
     * @Template("places/place_form.html.twig")
     */
    public function addPlaceAction(Request $request)
    {
        return $this->handlingForm(new Place(), $request);
    }

    /**
     * @Route("/places/{id}/edit", name="edit_place")
     * @Template("places/place_form.html.twig")
     */
    public function editPlaceAction(Place $place, Request $request)
    {
        return $this->handlingForm($place, $request);
    }

    /**
     * @Route("/places/{id}", name="place")
     * @Template("places/place.html.twig")
     */
    public function placeAction(Place $place): array
    {
        return [
            'place' => $place,
        ];
    }

    /**
     * @Route("/places", name="places")
     * @Template("places/places.html.twig")
     */
    public function placesAction(PlaceRepository $placeRepository): array
    {
        return [
            'places' => $placeRepository->findAll(),
            'count' => count($placeRepository->findAll()),
        ];
    }
}
