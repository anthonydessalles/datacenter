<?php

namespace App\Controller;

use App\Entity\Album;
use App\Repository\ActivityRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Activity;
use App\Form\ActivityType;
use App\Form\ActivityFilterType;

class ActivityController extends AbstractController
{
    /**
     * @Route("/activities/add", name="add_activity")
     * @Template("activities/activity_form.html.twig")
     */
    public function addActivityAction(Request $request)
    {
        $activity = new Activity();
        $start = $end = new \DateTime();
        $activity->setStart($start);
        $activity->setEnd($end);

        $form = $this->createForm(ActivityType::class, $activity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Activity $activity */
            $activity = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($activity);
            $em->flush();

            return $this->redirectToRoute('activity', [
                'id' => $activity->getId(),
            ]);
        }

        return [
            'form' => $form->createView(),
            'activity' => $activity,
        ];
    }

    /**
     * @Route("/activities/album/{id}/add", name="add_album_activity")
     * @Template("activities/activity_form.html.twig")
     */
    public function addAlbumActivity(Album $album, Request $request, ActivityRepository $activityRepository)
    {
        $activity = new Activity();
        $activity->addAlbum($album);
        $title = $album->getTitle();
        $brackets = explode('(', $title);
        $start = $end = new \DateTime();

        if (isset($brackets[1])) {
            $albumDate = substr($brackets[1], 0, -1);
            $start = $end = new \DateTime($albumDate);
        }

        $activityTitle = trim($brackets[0]);
        $activitiesWithName = $activityRepository->findAllByName($activityTitle);

        $activity->setName($activityTitle);
        $activity->setStart($start);
        $activity->setEnd($end);

        $form = $this->createForm(ActivityType::class, $activity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Activity $activity */
            $activity = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($activity);
            $em->flush();

            return $this->redirectToRoute('activity', [
                'id' => $activity->getId(),
            ]);
        }

        return [
            'form' => $form->createView(),
            'activity' => $activity,
            'activitiesWithName' => $activitiesWithName,
        ];
    }

    /**
     * @Route("/activities", name="activities")
     * @Template("activities/activities.html.twig")
     * @param ActivityRepository $activityRepository
     * @return array
     */
    public function activitiesAction(Request $request, ActivityRepository $activityRepository)
    {
        $form = $this->createForm(ActivityFilterType::class);
        $form->handleRequest($request);
        $activities = $activityRepository->findAllOrderByStart('DESC', 0, 25);
        $count = count($activityRepository->findAllOrderByStart('DESC'));
        $deactivateInfiniteScroll = false;

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $source = $data['source'];
            $max = $data['max'];
            $activities = $activityRepository->getFilteredActivities($source, $max);
            $count = count($activities);
            $deactivateInfiniteScroll = true;
        }

        return [
            'activities' => $activities,
            'count' => $count,
            'table' => Activity::ACTIVITIES,
            'form' => $form->createView(),
            'deactivateInfiniteScroll' => $deactivateInfiniteScroll,
        ];
    }

    /**
     * @Route("/activities/{id}", name="activity")
     * @Template("activities/activity.html.twig")
     * @param Activity $activity
     * @return array
     */
    public function activityAction(Activity $activity)
    {
        return [
            'activity' => $activity,
        ];
    }

    /**
     * @Route("/activities/{id}/users", name="activity_users")
     * @Template("activities/activity_users.html.twig")
     * @param Activity $activity
     * @return array
     */
    public function activityUsersAction(Activity $activity)
    {
        $usersEmails = $usersEmailsAssociations = [];

        foreach ($activity->getUsers() as $user) {
            if ($user->getEmail()) {
                $usersEmails[] = $user->getEmail();
            }

            if ($user->getEmail() && $user->getAssociation()) {
                $usersEmailsAssociations[$user->getAssociation()][] = $user->getEmail();
            }
        }
        sort($usersEmails);

        return [
            'activity' => $activity,
            'usersEmails' => $usersEmails,
            'usersEmailsAssociations' => $usersEmailsAssociations,
        ];
    }

    /**
     * @Route("/activities/{id}/edit", name="edit_activity")
     * @Template("activities/activity_form.html.twig")
     * @param Activity $activity
     * @param Request $request
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function editActivityAction(Activity $activity, Request $request)
    {
        $form = $this->createForm(ActivityType::class, $activity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Activity $activity */
            $activity = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($activity);
            $em->flush();

            return $this->redirectToRoute('activity', [
                'id' => $activity->getId(),
            ]);
        }

        return [
            'form' => $form->createView(),
            'activity' => $activity,
        ];
    }
}
