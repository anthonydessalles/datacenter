<?php

namespace App\Controller;

use App\Entity\Location;
use App\Form\LocationType;
use App\Repository\LocationRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class LocationController extends AbstractController
{
    private function handlingForm(Location $location, Request $request)
    {
        $form = $this->createForm(LocationType::class, $location);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($location);
            $em->flush();

            return $this->redirectToRoute('locations');
        }

        return [
            'form' => $form->createView(),
            'location' => $location,
        ];
    }

    /**
     * @Route("/locations/add", name="add_location")
     * @Template("locations/location_form.html.twig")
     */
    public function addLocationAction(Request $request)
    {
        return $this->handlingForm(new Location(), $request);
    }

    /**
     * @Route("/locations/{id}/edit", name="edit_location")
     * @Template("locations/location_form.html.twig")
     */
    public function editLocationAction(Location $location, Request $request)
    {
        return $this->handlingForm($location, $request);
    }

    /**
     * @Route("/locations/{id}", name="location")
     * @Template("locations/location.html.twig")
     */
    public function locationAction(Location $location): array
    {
        return [
            'location' => $location,
        ];
    }

    /**
     * @Route("/locations", name="locations")
     * @Template("locations/locations.html.twig")
     */
    public function locationsAction(LocationRepository $locationRepository): array
    {
        return [
            'locations' => $locationRepository->findAll(),
            'count' => count($locationRepository->findAll()),
        ];
    }
}
