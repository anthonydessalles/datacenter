<?php

namespace App\Controller;

use App\Entity\Profile;
use App\Form\ProfileType;
use App\Repository\ProfileRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class ProfileController extends AbstractController
{
    /**
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    private function handlingForm(Profile $profile, Request $request)
    {
        $form = $this->createForm(ProfileType::class, $profile);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($profile);
            $em->flush();

            return $this->redirectToRoute('profiles');
        }

        return [
            'form' => $form->createView(),
            'profile' => $profile,
        ];
    }

    /**
     * @Route("/profiles/add", name="add_profile")
     * @Template("profiles/profile_form.html.twig")
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Exception
     */
    public function addProfileAction(Request $request)
    {
        $profile = new Profile();

        return $this->handlingForm($profile, $request);
    }

    /**
     * @Route("/profiles/{id}/edit", name="edit_profile")
     * @Template("profiles/profile_form.html.twig")
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function editProfileAction(Profile $profile, Request $request)
    {
        return $this->handlingForm($profile, $request);
    }

    /**
     * @Route("/profiles/{id}", name="profile")
     * @Template("profiles/profile.html.twig")
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function profileAction(Profile $profile)
    {
        return [
            'profile' => $profile,
        ];
    }

    /**
     * @Route("/profiles", name="profiles")
     * @Template("profiles/profiles.html.twig")
     */
    public function profilesAction(ProfileRepository $profileRepository): array
    {
        return [
            'profiles' => $profileRepository->getProfiles(),
            'count' => count($profileRepository->getProfiles()),
            'enriched' => count($profileRepository->getEnrichedProfiles()),
        ];
    }
}
