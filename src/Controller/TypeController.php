<?php

namespace App\Controller;

use App\Entity\Type;
use App\Form\TypeType;
use App\Repository\TypeRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class TypeController extends AbstractController
{
    public function handlingForm(Type $type, Request $request)
    {
        $form = $this->createForm(TypeType::class, $type);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($type);
            $em->flush();

            return $this->redirectToRoute('types');
        }

        return [
            'form' => $form->createView(),
            'type' => $type,
        ];
    }

    /**
     * @Route("/types/add", name="add_type")
     * @Template("types/type_form.html.twig")
     */
    public function addTypeAction(Request $request)
    {
        return $this->handlingForm(new Type(), $request);
    }

    /**
     * @Route("/types/{id}", name="type")
     * @Template("types/type.html.twig")
     */
    public function typeAction(Type $type): array
    {
        return [
            'type' => $type,
        ];
    }

    /**
     * @Route("/types/{id}/edit", name="edit_type")
     * @Template("types/type_form.html.twig")
     */
    public function editTypeAction(Type $type, Request $request)
    {
        return $this->handlingForm($type, $request);
    }

    /**
     * @Route("/types", name="types")
     * @Template("types/types.html.twig")
     */
    public function typesAction(TypeRepository $typeRepository): array
    {
        return [
            'types' => $typeRepository->findAll(),
            'count' => count($typeRepository->findAll()),
        ];
    }
}
