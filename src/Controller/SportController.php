<?php

namespace App\Controller;

use App\Entity\Sport;
use App\Form\SportType;
use App\Repository\SportRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SportController extends AbstractController
{
    private function handlingForm(Sport $sport, Request $request)
    {
        $form = $this->createForm(SportType::class, $sport);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($sport);
            $em->flush();

            return $this->redirectToRoute('sports');
        }

        return [
            'form' => $form->createView(),
            'sport' => $sport,
        ];
    }

    /**
     * @Route("/sport/add", name="add_sport")
     * @Template("sports/sport_form.html.twig")
     */
    public function addSportAction(Request $request)
    {
        return $this->handlingForm(new Sport(), $request);
    }

    /**
     * @Route("/sports/{id}/edit", name="edit_sport")
     * @Template("sports/sport_form.html.twig")
     */
    public function editSportAction(Sport $sport, Request $request)
    {
        return $this->handlingForm($sport, $request);
    }

    /**
     * @Route("/sports", name="sports")
     * @Template("sports/sports.html.twig")
     */
    public function sportsAction(SportRepository $sportRepository): array
    {
        return [
            'sports' => $sportRepository->getSports(),
            'count' => count($sportRepository->getSports()),
        ];
    }
}
