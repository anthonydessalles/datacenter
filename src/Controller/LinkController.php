<?php

namespace App\Controller;

use App\Entity\Link;
use App\Form\LinkType;
use App\Repository\LinkRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class LinkController extends AbstractController
{
    /**
     * @Route("/link/add", name="add_link")
     * @Template("links/link_form.html.twig")
     * @param Request $request
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function addLinkAction(Request $request)
    {
        $link = new Link();
        $form = $this->createForm(LinkType::class, $link);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($link);
            $em->flush();

            return $this->redirectToRoute('links');
        }

        return [
            'form' => $form->createView(),
            'link' => $link,
        ];
    }

    /**
     * @Route("/links", name="links")
     * @Template("links/links.html.twig")
     * @param LinkRepository $linkRepository
     * @return array
     */
    public function linksAction(LinkRepository $linkRepository)
    {
        return [
            'links' => $linkRepository->findAll(),
            'count' => count($linkRepository->findAll()),
        ];
    }
}