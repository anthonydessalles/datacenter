<?php

namespace App\Controller;

use App\Entity\Timeline;
use App\Form\TimelineType;
use App\Repository\TimelineRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class TimelineController extends AbstractController
{
    private function handlingForm(Timeline $timeline, Request $request)
    {
        $form = $this->createForm(TimelineType::class, $timeline);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($timeline);
            $em->flush();

            return $this->redirectToRoute('timelines');
        }

        return [
            'form' => $form->createView(),
            'timeline' => $timeline,
        ];
    }

    /**
     * @Route("/timelines/add", name="add_timeline")
     * @Template("timelines/timeline_form.html.twig")
     */
    public function addTimelineAction(Request $request)
    {
        $timeline = new Timeline();
        $timeline->setStart(new \DateTime());
        $timeline->setEnd(new \DateTime());

        return $this->handlingForm($timeline, $request);
    }

    /**
     * @Route("/timelines/{id}/edit", name="edit_timeline")
     * @Template("timelines/timeline_form.html.twig")
     */
    public function editTimelineAction(Timeline $timeline, Request $request)
    {
        return $this->handlingForm($timeline, $request);
    }

    /**
     * @Route("/timelines", name="timelines")
     * @Template("timelines/timelines.html.twig")
     * @param TimelineRepository $timelineRepository
     * @return array
     */
    public function timelinesAction(TimelineRepository $timelineRepository)
    {
        return [
            'timelines' => $timelineRepository->getTimelines(),
            'count' => count($timelineRepository->getTimelines()),
        ];
    }
}
