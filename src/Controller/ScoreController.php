<?php

namespace App\Controller;

use App\Repository\ScoreRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Form\ScoreFilterType;
use App\Form\ScoreType;
use App\Entity\Score;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

class ScoreController extends AbstractController
{
    private function handlingForm(Score $score, Request $request): RedirectResponse|array
    {
        $form = $this->createForm(ScoreType::class, $score);
        $form->add('save', SubmitType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($score);
            $em->flush();

            return $this->redirectToRoute('scores');
        }

        return [
            'form' => $form->createView(),
            'score' => $score
        ];
    }

    /**
     * @Route("/scores/add", name="add_score")
     * @Template("scores/score_form.html.twig")
     */
    public function addProgressionAction(Request $request): RedirectResponse|array
    {
        return $this->handlingForm(new Score(), $request);
    }

    /**
     * @Route("/scores/{id}/edit", name="edit_score")
     * @Template("scores/score_form.html.twig")
     */
    public function editScoreAction(Score $score, Request $request): RedirectResponse|array
    {
        return $this->handlingForm($score, $request);
    }

    /**
     * @Route("/scores", name="scores")
     * @Template("scores/scores.html.twig")
     */
    public function scoresAction(Request $request, ScoreRepository $scoreRepository): array
    {
        $form = $this->createForm(ScoreFilterType::class);
        $form->handleRequest($request);
        $scores = $scoreRepository->findAll();
        $count = count($scoreRepository->findAll());

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $scores = $scoreRepository->getFilteredScores($data['type'], $data['game'], $data['tournament'], $data['description'], $data['difficulty']);
            $count = count($scores);
        }

        return [
            'scores' => $scores,
            'count' => $count,
            'form' => $form->createView(),
        ];
    }
}
