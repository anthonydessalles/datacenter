<?php

namespace App\Controller;

use App\Repository\UserRepository;
use Google_Client;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AuthController extends AbstractController
{
    public function register(Request $request, UserPasswordEncoderInterface $encoder)
    {
        $em = $this->getDoctrine()->getManager();

        $username = $request->request->get('_username');
        $password = $request->request->get('_password');

        $user = new User();
        $user->setUsername($username);
        $user->setPassword($encoder->encodePassword($user, $password));
        $user->setName($username);
        $user->setSurname($username);
        $user->setRole('ROLE_USER');
        $em->persist($user);
        $em->flush();

        return new Response(sprintf('User %s successfully created', $user->getUsername()));
    }

    public function api()
    {
        return new Response(sprintf('Logged in as %s', $this->getUser()->getUsername()));
    }

    public function tokenSignin(Request $request, JWTTokenManagerInterface $jwtManager, UserRepository $userRepository)
    {
        $id_token = $request->get('idtoken');
        $client = new Google_Client([
            'client_id' => $this->getParameter('google_client_id'),
        ]);
        $payload = $client->verifyIdToken($id_token);

        if (!$payload) {
            return new Response('Invalid Token');
        }

        $userEmail = $payload['email'];
        $user = $userRepository->findOneBy([
            'email' => $userEmail
        ]);

        if (!$user) {
            return new Response('User not found');
        }

        return new JsonResponse([
            'token' => $jwtManager->create($user),
            'user' => $user->getUsername(),
        ]);
    }
}
