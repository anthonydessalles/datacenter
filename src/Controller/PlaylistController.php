<?php

namespace App\Controller;

use App\Form\PlaylistFilterType;
use App\Repository\PlaylistRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class PlaylistController extends AbstractController
{
    /**
     * @Route("/playlists", name="playlists")
     * @Template("playlists/playlists.html.twig")
     * @param PlaylistRepository $playlistRepository
     * @return array
     */
    public function playlistsAction(Request $request, PlaylistRepository $playlistRepository)
    {
        $form = $this->createForm(PlaylistFilterType::class);
        $form->handleRequest($request);
        $playlists = $playlistRepository->findAll();
        $count = count($playlistRepository->findAll());

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $status = $data['status'];
            $game = $data['game'];
            $source = $data['source'];
            $playlists = $playlistRepository->getFilteredPlaylists($status, $game, $source);
            $count = count($playlists);
        }

        return [
            'playlists' => $playlists,
            'count' => $count,
            'form' => $form->createView(),
        ];
    }
}
