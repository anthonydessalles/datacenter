<?php

namespace App\Controller;

use App\Manager\StatisticsManager;
use App\Repository\UserRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\User;
use App\Form\UserType;
use Symfony\Component\Routing\Annotation\Route;
use App\Manager\UserKpiManager;

class UserController extends AbstractController
{
    /**
     * @Route("/users/add", name="add_user")
     * @Template("users/user_form.html.twig")
     * @param Request $request
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function addUserAction(Request $request)
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('users');
        }

        return [
            'form' => $form->createView(),
            'user' => $user,
        ];
    }

    /**
     * @Route("/users/{id}/edit", name="edit_user")
     * @Template("users/user_form.html.twig")
     * @param User $user
     * @param Request $request
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function editUserAction(User $user, Request $request)
    {
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var User $user */
            $user = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('user', [
                'id' => $user->getId(),
            ]);
        }

        return [
            'form' => $form->createView(),
            'user' => $user,
        ];
    }

    /**
     * @Route("/users/{id}", name="user")
     * @Template("users/user.html.twig")
     * @param User $user
     * @return array
     */
    public function userAction(User $user, UserKpiManager $userKpiManager)
    {
        return [
            'user' => $user,
            'userScoresAvg' => $userKpiManager->getUserScoresAvg($user, true),
        ];
    }

    /**
     * @Route("/users", name="users")
     * @Template("users/users.html.twig")
     * @param UserRepository $userRepository
     * @return array
     */
    public function usersAction(UserRepository $userRepository)
    {
        return [
            'users' => $userRepository->findAllOrderByUsername(),
            'count' => count($userRepository->findAllOrderByUsername()),
        ];
    }
}
