<?php

namespace App\Controller;

use App\Entity\File;
use App\Form\GameFilterType;
use App\Manager\GameManager;
use App\Repository\GameRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Game;
use App\Form\GameType;
use Symfony\Component\Routing\Annotation\Route;

class GameController extends AbstractController
{
    /**
     * @param Game $game
     * @param Request $request
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    private function handlingForm(Game $game, Request $request)
    {
        $form = $this->createForm(GameType::class, $game);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($game);
            $em->flush();

            return $this->redirectToRoute('games');
        }

        return [
            'form' => $form->createView(),
            'game' => $game,
        ];
    }

    /**
     * @Route("/game/add", name="add_game")
     * @Template("games/game_form.html.twig")
     */
    public function addGameAction(Request $request)
    {
        $game = new Game();
        $game->setDate(new \DateTime());

        return $this->handlingForm($game, $request);
    }

    /**
     * @Route("/games/{id}/edit", name="edit_game")
     * @Template("games/game_form.html.twig")
     */
    public function editGameAction(Game $game, Request $request)
    {
        return $this->handlingForm($game, $request);
    }

    /**
     * @Route("/games/{id}/{year}-{month}-{day}-{slug}.md", name="game_markdown")
     * @Template("games/game.md.twig")
     */
    public function gameMarkdowndAction(Game $game, GameManager $gameManager)
    {
        return $gameManager->getGameMarkdownDetails($game);
    }

    /**
     * @Route("/games", name="games")
     * @Template("games/games.html.twig")
     */
    public function gamesAction(GameRepository $gameRepository, Request $request): array
    {
        $form = $this->createForm(GameFilterType::class);
        $form->handleRequest($request);
        $games = $gameRepository->getGames(0, 25);
        $count = count($gameRepository->getGames());
        $deactivateInfiniteScroll = false;

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $games = $gameRepository->getFilteredGames($data['type'], $data['max']);
            $count = count($games);
            $deactivateInfiniteScroll = true;
        }

        return [
            'games' => $games,
            'count' => $count,
            'enriched' => count($gameRepository->getEnrichedGames()),
            'table' => Game::GAMES,
            'form' => $form->createView(),
            'deactivateInfiniteScroll' => $deactivateInfiniteScroll,
        ];
    }
}
