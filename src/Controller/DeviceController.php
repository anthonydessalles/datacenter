<?php

namespace App\Controller;

use App\Entity\Device;
use App\Form\DeviceType;
use App\Repository\DeviceRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DeviceController extends AbstractController
{
    private function handlingForm(Device $device, Request $request)
    {
        $form = $this->createForm(DeviceType::class, $device);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($device);
            $em->flush();

            return $this->redirectToRoute('devices');
        }

        return [
            'form' => $form->createView(),
            'device' => $device,
        ];
    }

    /**
     * @Route("/device/add", name="add_device")
     * @Template("devices/device_form.html.twig")
     */
    public function addVideoAction(Request $request)
    {
        return $this->handlingForm(new Device(), $request);
    }

    /**
     * @Route("/devices/{id}/edit", name="edit_device")
     * @Template("devices/device_form.html.twig")
     */
    public function editVideoAction(Device $device, Request $request)
    {
        return $this->handlingForm($device, $request);
    }

    /**
     * @Route("/devices", name="devices")
     * @Template("devices/devices.html.twig")
     * @param DeviceRepository $deviceRepository
     * @return array
     */
    public function devicesAction(DeviceRepository $deviceRepository)
    {
        return [
            'devices' => $deviceRepository->getDevices(),
            'count' => count($deviceRepository->getDevices()),
        ];
    }
}
