<?php

namespace App\Controller;

use App\Form\CustomSearchType;
use App\Repository\ActivityRepository;
use App\Repository\AlbumRepository;
use App\Repository\BookRepository;
use App\Repository\FileRepository;
use App\Repository\FilmRepository;
use App\Repository\GameRepository;
use App\Repository\SerieRepository;
use App\Repository\SportRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class SearchController extends AbstractController
{
    /**
     * @Route("/search", name="search")
     * @Template("search/search.html.twig")
     */
    public function searchAction(
        Request $request,
        ActivityRepository $activityRepository,
        AlbumRepository $albumRepository,
        BookRepository $bookRepository,
        FileRepository $fileRepository,
        FilmRepository $filmRepository,
        GameRepository $gameRepository,
        SerieRepository $serieRepository,
        SportRepository $sportRepository
    )
    {
        $form = $this->createForm(CustomSearchType::class);
        $form->handleRequest($request);
        $pageData['form'] = $form->createView();

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $name = $data['name'];

            $pageData['activities'] = $activityRepository->findAllByName($name);
            $pageData['albums'] = $albumRepository->getAlbumsFromTitle($name);
            $pageData['books'] = $bookRepository->getBooksFromName($name);
            $pageData['files'] = $fileRepository->getFilesFromFilename($name);
            $pageData['films'] = $filmRepository->getFilmsFromName($name);
            $pageData['games'] = $gameRepository->getGamesFromName($name);
            $pageData['series'] = $serieRepository->getSeriesFromName($name);
            $pageData['sports'] = $sportRepository->getSportsFromName($name);
        }

        return $pageData;
    }
}
