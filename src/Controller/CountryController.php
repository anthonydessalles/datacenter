<?php

namespace App\Controller;

use App\Entity\Country;
use App\Form\CountryType;
use App\Repository\CountryRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CountryController extends AbstractController
{
    private function handlingForm(Country $country, Request $request)
    {
        $form = $this->createForm(CountryType::class, $country);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($country);
            $em->flush();

            return $this->redirectToRoute('countries');
        }

        return [
            'form' => $form->createView(),
            'country' => $country,
        ];
    }

    /**
     * @Route("/countries/add", name="add_country")
     * @Template("countries/country_form.html.twig")
     */
    public function addTypeAction(Request $request)
    {
        return $this->handlingForm(new Country(), $request);
    }

    /**
     * @Route("/countries", name="countries")
     * @Template("countries/countries.html.twig")
     */
    public function countriesAction(CountryRepository $countryRepository): array
    {
        return [
            'countries' => $countryRepository->findAll(),
            'count' => count($countryRepository->findAll()),
        ];
    }

    /**
     * @Route("/countries/{id}", name="country")
     * @Template("countries/country.html.twig")
     */
    public function countryAction(Country $country): array
    {
        return [
            'country' => $country,
        ];
    }

    /**
     * @Route("/countries/{id}/edit", name="edit_country")
     * @Template("countries/country_form.html.twig")
     */
    public function editTimelineAction(Country $country, Request $request)
    {
        return $this->handlingForm($country, $request);
    }
}
