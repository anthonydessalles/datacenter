<?php

namespace App\Controller;

use App\Entity\Film;
use App\Form\FilmFilterType;
use App\Form\FilmType;
use App\Repository\FilmRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class FilmController extends AbstractController
{
    private function handlingForm(Film $film, Request $request)
    {
        $form = $this->createForm(FilmType::class, $film);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($film);
            $em->flush();

            return $this->redirectToRoute('films');
        }

        return [
            'form' => $form->createView(),
            'film' => $film,
        ];
    }

    /**
     * @Route("/film/add", name="add_film")
     * @Template("films/film_form.html.twig")
     */
    public function addFilmAction(Request $request)
    {
        return $this->handlingForm(new Film(), $request);
    }

    /**
     * @Route("/films/{id}/edit", name="edit_film")
     * @Template("films/film_form.html.twig")
     */
    public function editFilmAction(Film $film, Request $request)
    {
        return $this->handlingForm($film, $request);
    }

    /**
     * @Route("/films", name="films")
     * @Template("films/films.html.twig")
     */
    public function filmsAction(Request $request, FilmRepository $filmRepository): array
    {
        $form = $this->createForm(FilmFilterType::class);
        $form->handleRequest($request);
        $films = $filmRepository->getFilms();

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $films = $filmRepository->getFilteredFilms($data['year'], $data['type'], $data['album'], $data['max']);
        }

        return [
            'films' => $films,
            'count' => count($films),
            'form' => $form->createView(),
        ];
    }
}
