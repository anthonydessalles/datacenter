<?php

namespace App\Controller;

use App\Entity\Album;
use App\Form\AlbumType;
use App\Repository\AlbumRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AlbumController extends AbstractController
{
    /**
     * @Route("/albums", name="albums")
     * @Template("albums/albums.html.twig")
     * @param AlbumRepository $albumRepository
     * @return array
     */
    public function albumsAction(AlbumRepository $albumRepository)
    {
        return [
            'albums' => $albumRepository->findAllOrderByName(),
            'count' => count($albumRepository->findAllOrderByName()),
            'enriched' => count($albumRepository->getEnrichedAlbums()),
        ];
    }

    /**
     * @Route("/albums/{id}/edit", name="edit_album")
     * @Template("albums/album_form.html.twig")
     * @param Album $album
     * @param Request $request
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function editAlbumAction(Album $album, Request $request)
    {
        $form = $this->createForm(AlbumType::class, $album);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Album $album */
            $album = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($album);
            $em->flush();

            return $this->redirectToRoute('albums');
        }

        return [
            'form' => $form->createView(),
            'album' => $album,
        ];
    }
}
