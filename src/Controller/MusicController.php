<?php

namespace App\Controller;

use App\Entity\Music;
use App\Form\MusicType;
use App\Repository\MusicRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class MusicController extends AbstractController
{
    private function handlingForm(Music $music, Request $request)
    {
        $form = $this->createForm(MusicType::class, $music);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($music);
            $em->flush();

            return $this->redirectToRoute('music');
        }

        return [
            'form' => $form->createView(),
            'music' => $music,
        ];
    }

    /**
     * @Route("/music/add", name="add_music")
     * @Template("music/music_form.html.twig")
     */
    public function addMusicAction(Request $request)
    {
        return $this->handlingForm(new Music(), $request);
    }

    /**
     * @Route("/music/{id}/edit", name="edit_music")
     * @Template("music/music_form.html.twig")
     */
    public function editMusicAction(Music $music, Request $request)
    {
        return $this->handlingForm($music, $request);
    }

    /**
     * @Route("/music", name="music")
     * @Template("music/music.html.twig")
     */
    public function musicAction(Request $request, MusicRepository $musicRepository): array
    {
        $music = $musicRepository->findAll();

        return [
            'music' => $music,
            'count' => count($music),
        ];
    }
}
