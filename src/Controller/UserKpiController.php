<?php

namespace App\Controller;

use App\Repository\UserKpiRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class UserKpiController extends AbstractController
{
    /**
     * @Route("/user-kpis", name="user_kpis")
     * @Template("user_kpis/user_kpis.html.twig")
     */
    public function userScoresAction(UserKpiRepository $userKpiRepository): array
    {
        return [
            'user_kpis' => $userKpiRepository->findAll(),
            'count' => count($userKpiRepository->findAll()),
        ];
    }
}
