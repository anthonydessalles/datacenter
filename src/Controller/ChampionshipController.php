<?php

namespace App\Controller;

use App\Entity\Championship;
use App\Form\ChampionshipType;
use App\Repository\ChampionshipRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\ChampionshipFilterType;

class ChampionshipController extends AbstractController
{
    private function handlingForm(Championship $championship, Request $request)
    {
        $form = $this->createForm(ChampionshipType::class, $championship);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($championship);
            $em->flush();

            return $this->redirectToRoute('championships');
        }

        return [
            'form' => $form->createView(),
            'championship' => $championship,
        ];
    }

    /**
     * @Route("/championship/add", name="add_championship")
     * @Template("championships/championship_form.html.twig")
     */
    public function addChampionshipAction(Request $request)
    {
        return $this->handlingForm(new Championship(), $request);
    }

    /**
     * @Route("/championships/{id}/edit", name="edit_championship")
     * @Template("championships/championship_form.html.twig")
     */
    public function editChampionshipAction(Championship $championship, Request $request)
    {
        return $this->handlingForm($championship, $request);
    }

    /**
     * @Route("/championships", name="championships")
     * @Template("championships/championships.html.twig")
     */
    public function championshipsAction(Request $request, ChampionshipRepository $championshipRepository): array
    {
        $form = $this->createForm(ChampionshipFilterType::class);
        $form->handleRequest($request);
        $championships = $championshipRepository->getChampionships();
        $count = count($championshipRepository->getChampionships());

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $title = $data['title'];
            $holder = $data['holder'];
            $game = $data['game'];
            $type = $data['type'];
            $championships = $championshipRepository->getFilteredChampionships($title, $holder, $game, $type);
            $count = count($championships);
        }

        return [
            'championships' => $championships,
            'count' => $count,
            'form' => $form->createView(),
        ];
    }
}
