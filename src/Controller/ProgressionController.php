<?php

namespace App\Controller;

use App\Entity\File;
use App\Form\MediasType;
use App\Repository\ProgressionRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Progression;
use App\Form\ProgressionType;
use Symfony\Component\Routing\Annotation\Route;

class ProgressionController extends AbstractController
{
    /**
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    private function handlingForm(Progression $progression, Request $request)
    {
        $form = $this->createForm(ProgressionType::class, $progression);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($progression);
            $em->flush();

            return $this->redirectToRoute('progressions');
        }

        return [
            'form' => $form->createView(),
            'progression' => $progression
        ];
    }

    /**
     * @Route("/progressions/add", name="add_progression")
     * @Template("progressions/progression_form.html.twig")
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function addProgressionAction(Request $request)
    {
        return $this->handlingForm(new Progression(), $request);
    }

    /**
     * @Route("/progressions/{id}/edit", name="edit_progression")
     * @Template("progressions/progression_form.html.twig")
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editProgressionAction(Progression $progression, Request $request)
    {
        return $this->handlingForm($progression, $request);
    }

    /**
     * @Route("/progressions", name="progressions")
     * @Template("progressions/progressions.html.twig")
     */
    public function progressionsAction(ProgressionRepository $progressionRepository): array
    {
        return [
            'progressions' => $progressionRepository->findAll(),
            'count' => count($progressionRepository->findAll()),
        ];
    }
}
