<?php

namespace App\Controller;

use App\Repository\RecordingRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class RecordingController extends AbstractController
{
    /**
     * @Route("/recordings", name="recordings")
     * @Template("recordings/recordings.html.twig")
     */
    public function recordingsAction(RecordingRepository $recordingRepository): array
    {
        return [
            'recordings' => $recordingRepository->findAll(),
            'count' => count($recordingRepository->findAll()),
        ];
    }
}
