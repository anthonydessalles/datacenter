<?php

namespace App\Controller;

use App\Repository\ScreenshotRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ScreenshotController extends AbstractController
{
    /**
     * @Route("/screenshots", name="screenshots")
     * @Template("screenshots/screenshots.html.twig")
     */
    public function screenshotsAction(ScreenshotRepository $screenshotRepository): array
    {
        return [
            'screenshots' => $screenshotRepository->findAll(),
            'count' => count($screenshotRepository->findAll()),
        ];
    }
}
