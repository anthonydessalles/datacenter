<?php

namespace App\Controller;

use App\Entity\Activity;
use App\Entity\Album;
use App\Entity\Device;
use App\Entity\File;
use App\Entity\Game;
use App\Entity\Link;
use App\Entity\Playlist;
use App\Entity\Progression;
use App\Entity\Score;
use App\Entity\Timeline;
use App\Entity\Type;
use App\Entity\User;
use App\Entity\Championship;
use App\Entity\Sport;
use App\Entity\Film;
use App\Entity\Recording;
use App\Entity\Screenshot;
use App\Repository\ActivityRepository;
use App\Repository\FileRepository;
use App\Repository\GameRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AjaxController extends AbstractController
{
    /**
     * @Route("/ajax/albums", name="ajax_albums")
     */
    public function ajaxAlbumsAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $albums = null;
        foreach ($em->getRepository(Album::class)->getAlbumsFromTitle($request->get('q')) as $album) {
            $albums[] = [
                'id' => $album->getId(),
                'text' => $album->getTitle()
            ];
        }

        return $this->json($albums);
    }

    /**
     * @Route("/ajax/championships", name="ajax_championships")
     */
    public function ajaxChampionshipsAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $championships = null;
        foreach ($em->getRepository(Championship::class)->getChampionshipFromId($request->get('q')) as $championship) {
            $championships[] = [
                'id' => $championship->getId(),
                'text' => $championship->getId(),
            ];
        }

        return $this->json($championships);
    }

    /**
     * @Route("/ajax/devices", name="ajax_devices")
     */
    public function ajaxDevicesAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $devices = null;
        foreach ($em->getRepository(Device::class)->getDevicesFromTitle($request->get('q')) as $device) {
            $devices[] = [
                'id' => $device->getId(),
                'text' => $device->getTitle()
            ];
        }

        return $this->json($devices);
    }

    /**
     * @Route("/ajax/files", name="ajax_files")
     */
    public function ajaxFilesAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $files = null;
        foreach ($em->getRepository(File::class)->getFilesFromFilename($request->get('q')) as $file) {
            $files[] = [
                'id' => $file->getId(),
                'text' => $file->getFilename()
            ];
        }

        return $this->json($files);
    }

    /**
     * @Route("/ajax/films", name="ajax_films")
     */
    public function ajaxFilmsAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $files = null;
        foreach ($em->getRepository(Film::class)->getFilmsFromName($request->get('q')) as $file) {
            $files[] = [
                'id' => $file->getId(),
                'text' => $file->getTitle()
            ];
        }

        return $this->json($files);
    }

    /**
     * @Route("/ajax/games", name="ajax_games")
     */
    public function ajaxGamesAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $games = null;
        foreach ($em->getRepository(Game::class)->getGamesFromName($request->get('q')) as $game) {
            $games[] = [
                'id' => $game->getId(),
                'text' => $game->getName()
            ];
        }

        return $this->json($games);
    }

    /**
     * @Route("/ajax/links", name="ajax_links")
     */
    public function ajaxLinksAction(Request $request)
    {
        $links = null;
        foreach ($this->getDoctrine()->getRepository(Link::class)->getLinksFromUrl($request->get('q')) as $link) {
            $links[] = [
                'id' => $link->getId(),
                'text' => $link->getUrl()
            ];
        }

        return $this->json($links);
    }

    /**
     * @Route("/ajax/playlists", name="ajax_playlists")
     */
    public function ajaxPlaylistsAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $playlists = null;
        foreach ($em->getRepository(Playlist::class)->getPlaylistsFromTitle($request->get('q')) as $playlist) {
            $playlists[] = [
                'id' => $playlist->getId(),
                'text' => $playlist->getTitle(),
            ];
        }

        return $this->json($playlists);
    }

    /**
     * @Route("/ajax/progressions", name="ajax_progressions")
     */
    public function ajaxProgressionsAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $progressions = null;
        foreach ($em->getRepository(Progression::class)->getProgressionsFromName($request->get('q')) as $progression) {
            $progressions[] = [
                'id' => $progression->getId(),
                'text' => $progression->getName(),
            ];
        }

        return $this->json($progressions);
    }

    /**
     * @Route("/ajax/recordings", name="ajax_recordings")
     */
    public function ajaxRecordingsAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $recordings = null;
        foreach ($em->getRepository(Recording::class)->getRecordingsFromName($request->get('q')) as $recording) {
            $recordings[] = [
                'id' => $recording->getId(),
                'text' => $recording->getName(),
            ];
        }

        return $this->json($recordings);
    }

    /**
     * @Route("/ajax/scores", name="ajax_scores")
     */
    public function ajaxScoresAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $scores = null;
        foreach ($em->getRepository(Score::class)->getScoreFromId($request->get('q')) as $score) {
            $scores[] = [
                'id' => $score->getId(),
                'text' => $score->getId(),
            ];
        }

        return $this->json($scores);
    }

    /**
     * @Route("/ajax/screenshots", name="ajax_screenshots")
     */
    public function ajaxScreenshotsAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $screenshots = null;
        foreach ($em->getRepository(Screenshot::class)->getScreenshotsFromTitle($request->get('q')) as $screenshot) {
            $screenshots[] = [
                'id' => $screenshot->getId(),
                'text' => $screenshot->getTitle(),
            ];
        }

        return $this->json($screenshots);
    }

    /**
     * @Route("/ajax/sports", name="ajax_sports")
     */
    public function ajaxSportsAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $sports = null;
        foreach ($em->getRepository(Sport::class)->getSportsFromName($request->get('q')) as $sport) {
            $sports[] = [
                'id' => $sport->getId(),
                'text' => $sport->getTitle(),
            ];
        }

        return $this->json($sports);
    }

    /**
     * @Route("/ajax/timelines", name="ajax_timelines")
     */
    public function ajaxTimelinesAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $timelines = null;
        foreach ($em->getRepository(Timeline::class)->getTimelinesFromSlug($request->get('q')) as $timeline) {
            $timelines[] = [
                'id' => $timeline->getId(),
                'text' => $timeline->getName()
            ];
        }

        return $this->json($timelines);
    }

    /**
     * @Route("/ajax/types", name="ajax_types")
     */
    public function ajaxTypesAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $types = null;
        foreach ($em->getRepository(Type::class)->getTypesFromSlug($request->get('q')) as $type) {
            $types[] = [
                'id' => $type->getId(),
                'text' => $type->getSlug()
            ];
        }

        return $this->json($types);
    }

    /**
     * @Route("/ajax/users", name="ajax_users")
     */
    public function ajaxUsersAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $users = null;
        foreach ($em->getRepository(User::class)->getUsersFromUsername($request->get('q')) as $user) {
            $users[] = [
                'id' => $user->getId(),
                'text' => $user->getUsername()
            ];
        }

        return $this->json($users);
    }

    /**
     * @Route("/ajax/videos", name="ajax_videos")
     */
    public function ajaxVideosAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $files = null;
        foreach ($em->getRepository(File::class)->getVideoFilesFromFilename($request->get('q')) as $file) {
            $files[] = [
                'id' => $file->getId(),
                'text' => $file->getFilename()
            ];
        }

        return $this->json($files);
    }

    /**
     * @Route("/ajax/infinite-scroll/{offset}/{table}", name="ajax_infinite_scroll")
     * @return JsonResponse
     */
    public function ajaxInfiniteScrollAction(
        $offset,
        $table,
        FileRepository $fileRepository,
        ActivityRepository $activityRepository,
        GameRepository $gameRepository
    ) {
        $limit = 100;
        $responseOptions = [];

        switch ($table) {
            case Activity::ACTIVITIES:
                $activities = $activityRepository->findAllOrderByStart('DESC', $offset, $limit);
                $count = count($activities);
                $responseOptions = [
                    'view' => $this->renderView(Activity::ACTIVITIES . '/' . Activity::ACTIVITIES . '_data.html.twig', [
                        'data' => $activities,
                    ]),
                    'count' => $count,
                    'stop' => $count < $limit
                ];
                break;
            case File::FILES:
                $files = $fileRepository->getFiles($offset, $limit);
                $count = count($files);
                $responseOptions = [
                    'view' => $this->renderView(File::FILES . '/' . File::FILES . '_data.html.twig', [
                        'data' => $files,
                    ]),
                    'count' => $count,
                    'stop' => $count < $limit
                ];
                break;
            case Game::GAMES:
                $games = $gameRepository->getGames($offset, $limit);
                $count = count($games);
                $responseOptions = [
                    'view' => $this->renderView(Game::GAMES . '/' . Game::GAMES . '_data.html.twig', [
                        'data' => $games,
                    ]),
                    'count' => $count,
                    'stop' => $count < $limit
                ];
                break;
        }

        $response = new JsonResponse();

        return $response->setData($responseOptions);
    }
}
