<?php

namespace App\Controller;

use App\Entity\Serie;
use App\Form\SerieType;
use App\Repository\SerieRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SerieController extends AbstractController
{
    private function handlingForm(Serie $serie, Request $request)
    {
        $form = $this->createForm(SerieType::class, $serie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($serie);
            $em->flush();

            return $this->redirectToRoute('series');
        }

        return [
            'form' => $form->createView(),
            'serie' => $serie,
        ];
    }

    /**
     * @Route("/serie/add", name="add_serie")
     * @Template("series/serie_form.html.twig")
     */
    public function addSerieAction(Request $request)
    {
        return $this->handlingForm(new Serie(), $request);
    }

    /**
     * @Route("/series/{id}/edit", name="edit_serie")
     * @Template("series/serie_form.html.twig")
     */
    public function editSerieAction(Serie $serie, Request $request)
    {
        return $this->handlingForm($serie, $request);
    }

    /**
     * @Route("/series", name="series")
     * @Template("series/series.html.twig")
     */
    public function seriesAction(SerieRepository $serieRepository): array
    {
        return [
            'series' => $serieRepository->getSeries(),
            'count' => count($serieRepository->getSeries()),
        ];
    }
}
