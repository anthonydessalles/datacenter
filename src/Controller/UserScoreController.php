<?php

namespace App\Controller;

use App\Entity\UserHasScore;
use App\Form\UserHasScoreType;
use App\Repository\UserHasScoreRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class UserScoreController extends AbstractController
{
    private function handlingForm(UserHasScore $userHasScore, Request $request)
    {
        $form = $this->createForm(UserHasScoreType::class, $userHasScore);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($userHasScore);
            $em->flush();

            return $this->redirectToRoute('user_scores');
        }

        return [
            'form' => $form->createView(),
            'user_score' => $userHasScore,
        ];
    }

    /**
     * @Route("/user-scores/add", name="add_user_score")
     * @Template("user_scores/user_score_form.html.twig")
     * @param Request $request
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function addUserScoreAction(Request $request)
    {
        return $this->handlingForm(new UserHasScore(), $request);
    }

    /**
     * @Route("/user-scores/{id}/edit", name="edit_user_score")
     * @Template("user_scores/user_score_form.html.twig")
     */
    public function editVideoAction(UserHasScore $userHasScore, Request $request)
    {
        return $this->handlingForm($userHasScore, $request);
    }

    /**
     * @Route("/user-scores", name="user_scores")
     * @Template("user_scores/user_scores.html.twig")
     * @param UserHasScoreRepository $userScoreRepository
     * @return array
     */
    public function userScoresAction(UserHasScoreRepository $userScoreRepository)
    {
        return [
            'user_scores' => $userScoreRepository->findAll(),
            'count' => count($userScoreRepository->findAll()),
        ];
    }
}