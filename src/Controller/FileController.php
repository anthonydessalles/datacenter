<?php

namespace App\Controller;

use App\Form\FileFilterType;
use App\Form\FilePathType;
use App\Form\FileType;
use App\Repository\FileRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use App\Entity\File;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class FileController extends AbstractController
{
    /**
     * @Route("/files/add", name="add_file")
     * @Template("files/file_form.html.twig")
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function addFileAction(Request $request)
    {
        $file = new File();
        $file->setDate(new \DateTime());
        $form = $this->createForm(FileType::class, $file);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($file);
            $em->flush();

            return $this->redirectToRoute('files');
        }

        return [
            'form' => $form->createView(),
            'file' => $file,
        ];
    }

    /**
     * @Route("/files/file/{id}/path/batch", name="batch_file_path")
     * @Template("files/batch_file_form.html.twig")
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function batchFilePathAction(File $file, Request $request)
    {
        $path = $file->getPath();
        $form = $this->createForm(FilePathType::class, null, [
            'path' => $path,
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $data = $form->getData();
            $date = $data['date'];

            foreach ($data['files'] as $key => $file) {
                $file->setDate($date);
                $em->persist($file);
                $em->flush();
            }

            return $this->redirectToRoute('files');
        }

        return [
            'form' => $form->createView(),
            'path' => $path,
        ];
    }

    /**
     * @Route("/files/{id}/edit", name="edit_file")
     * @Template("files/file_form.html.twig")
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function editFileAction(File $file, Request $request)
    {
        $form = $this->createForm(FileType::class, $file);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($file);
            $em->flush();

            return $this->redirectToRoute('files');
        }

        return [
            'form' => $form->createView(),
            'file' => $file,
        ];
    }

    /**
     * @Route("/files", name="files")
     * @Template("files/files.html.twig")
     */
    public function filesAction(Request $request, FileRepository $fileRepository): array
    {
        $form = $this->createForm(FileFilterType::class);
        $form->handleRequest($request);
        $files = $fileRepository->getFiles(0, 25);
        $count = count($fileRepository->getFiles());
        $deactivateInfiniteScroll = false;

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $files = $fileRepository->getFilteredFiles($data['path'], $data['game'], $data['source'], $data['max']);
            $count = count($files);
            $deactivateInfiniteScroll = true;
        }

        return [
            'files' => $files,
            'count' => $count,
            'table' => File::FILES,
            'form' => $form->createView(),
            'deactivateInfiniteScroll' => $deactivateInfiniteScroll,
        ];
    }
}
