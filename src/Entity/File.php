<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use App\Entity\Progression;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

/**
 * File
 *
 * @ORM\Table(name="file")
 * @ORM\Entity(repositoryClass="App\Repository\FileRepository")
 * @ApiResource(
 *     normalizationContext={"groups"={"read"}},
 *     attributes={"order"={"filename": "ASC"}}
 * )
 * @ApiFilter(SearchFilter::class, properties={"source"="exact"})
 * @ApiFilter(SearchFilter::class, properties={"users.username", "exact"})
 */
class File
{
    const FILES = 'files';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"read"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="filename", type="string", length=255, unique=true)
     * @Groups({"read"})
     */
    private $filename;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=255, nullable=true)
     * @Groups({"read"})
     */
    private $path;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=true)
     */
    private $date;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\OneToMany(targetEntity="Progression", mappedBy="file")
     */
    private $progressions;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Source", inversedBy="files")
     * @Groups({"read"})
     */
    private $source;

    /**
     * @ORM\Column(name="source_url", type="string", length=255, nullable=true)
     * @Groups({"read"})
     */
    private $sourceUrl;

    /**
     * @ORM\Column(name="mime_type", type="string", length=255, nullable=true)
     */
    private $mimeType;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"read"})
     */
    private $height;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"read"})
     */
    private $width;

    /**
     * @ORM\Column(name="camera_make", type="string", length=255, nullable=true)
     */
    private $cameraMake;

    /**
     * @ORM\Column(name="camera_model", type="string", length=255, nullable=true)
     */
    private $cameraModel;

    /**
     * @ORM\Column(name="external_id", type="string", length=255, nullable=true)
     */
    private $externalId;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Game", mappedBy="files")
     */
    private $games;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"read"})
     */
    private $embedHtml;

    /**
     * @ORM\OneToMany(targetEntity=Score::class, mappedBy="file")
     */
    private $scores;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"read"})
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"read"})
     */
    private $size;

    /**
     * @ORM\ManyToMany(targetEntity=Compatibility::class, mappedBy="files")
     */
    private $compatibilities;

    /**
     * @ORM\OneToMany(targetEntity=Championship::class, mappedBy="file")
     */
    private $championships;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"read"})
     */
    private $duration;

    /**
     * @ORM\ManyToMany(targetEntity=Sport::class, mappedBy="files")
     */
    private $sports;

    /**
     * @ORM\ManyToMany(targetEntity=Film::class, mappedBy="files")
     */
    private $films;

    /**
     * @ORM\ManyToMany(targetEntity=Book::class, mappedBy="files")
     */
    private $books;

    /**
     * @ORM\ManyToMany(targetEntity=Device::class, mappedBy="files")
     */
    private $devices;

    /**
     * @ORM\ManyToMany(targetEntity=Serie::class, mappedBy="files")
     */
    private $series;

    /**
     * @ORM\OneToMany(targetEntity=UserHasScore::class, mappedBy="file")
     */
    private $userHasScores;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, mappedBy="files")
     * @Groups({"read", "list"})
     * @MaxDepth(1)
     */
    private $users;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"read"})
     */
    private $position;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set filename
     *
     * @param string $filename
     *
     * @return File
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;

        return $this;
    }

    /**
     * Get filename
     *
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * Set path
     *
     * @param string $path
     *
     * @return File
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return File
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return File
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    public function __construct()
    {
        $this->progressions = new ArrayCollection();
        $this->games = new ArrayCollection();
        $this->scores = new ArrayCollection();
        $this->compatibilities = new ArrayCollection();
        $this->championships = new ArrayCollection();
        $this->sports = new ArrayCollection();
        $this->films = new ArrayCollection();
        $this->books = new ArrayCollection();
        $this->devices = new ArrayCollection();
        $this->series = new ArrayCollection();
        $this->userHasScores = new ArrayCollection();
        $this->users = new ArrayCollection();
    }

    /**
     * @return Collection|Progression[]
     */
    public function getProgressions(): Collection
    {
        return $this->progressions;
    }

    public function addProgression(Progression $progression): self
    {
        if (!$this->progressions->contains($progression)) {
            $this->progressions[] = $progression;
            $progression->setFile($this);
        }

        return $this;
    }

    public function removeProgression(Progression $progression): self
    {
        if ($this->progressions->contains($progression)) {
            $this->progressions->removeElement($progression);
            $progression->setFile(null);
        }

        return $this;
    }

    public function getSource(): ?Source
    {
        return $this->source;
    }

    public function setSource(?Source $source): self
    {
        $this->source = $source;

        return $this;
    }

    public function __toString()
    {
        return $this->getFilename() . " (" . $this->getPath() . ")";
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     * @return File
     */
    public function setDate(\DateTime $date): File
    {
        $this->date = $date;
        return $this;
    }

    public function getSourceUrl(): ?string
    {
        return $this->sourceUrl;
    }

    public function setSourceUrl(?string $sourceUrl): self
    {
        $this->sourceUrl = $sourceUrl;

        return $this;
    }

    public function getMimeType(): ?string
    {
        return $this->mimeType;
    }

    public function setMimeType(?string $mimeType): self
    {
        $this->mimeType = $mimeType;

        return $this;
    }

    public function getHeight(): ?int
    {
        return $this->height;
    }

    public function setHeight(?int $height): self
    {
        $this->height = $height;

        return $this;
    }

    public function getWidth(): ?int
    {
        return $this->width;
    }

    public function setWidth(?int $width): self
    {
        $this->width = $width;

        return $this;
    }

    public function getCameraMake(): ?string
    {
        return $this->cameraMake;
    }

    public function setCameraMake(?string $cameraMake): self
    {
        $this->cameraMake = $cameraMake;

        return $this;
    }

    public function getCameraModel(): ?string
    {
        return $this->cameraModel;
    }

    public function setCameraModel(?string $cameraModel): self
    {
        $this->cameraModel = $cameraModel;

        return $this;
    }

    public function getExternalId(): ?string
    {
        return $this->externalId;
    }

    public function setExternalId(?string $externalId): self
    {
        $this->externalId = $externalId;

        return $this;
    }

    /**
     * @return Collection|Game[]
     */
    public function getGames(): Collection
    {
        return $this->games;
    }

    public function addGame(Game $game): self
    {
        if (!$this->games->contains($game)) {
            $this->games[] = $game;
            $game->addFile($this);
        }

        return $this;
    }

    public function removeGame(Game $game): self
    {
        if ($this->games->contains($game)) {
            $this->games->removeElement($game);
            $game->removeFile($this);
        }

        return $this;
    }

    public function getEmbedHtml(): ?string
    {
        return $this->embedHtml;
    }

    public function setEmbedHtml(?string $embedHtml): self
    {
        $this->embedHtml = $embedHtml;

        return $this;
    }

    /**
     * @return Collection<int, Score>
     */
    public function getScores(): Collection
    {
        return $this->scores;
    }

    public function addScore(Score $score): self
    {
        if (!$this->scores->contains($score)) {
            $this->scores[] = $score;
            $score->setFile($this);
        }

        return $this;
    }

    public function removeScore(Score $score): self
    {
        if ($this->scores->removeElement($score)) {
            // set the owning side to null (unless already changed)
            if ($score->getFile() === $this) {
                $score->setFile(null);
            }
        }

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getSize(): ?string
    {
        return $this->size;
    }

    public function setSize(?string $size): self
    {
        $this->size = $size;

        return $this;
    }

    /**
     * @return Collection<int, Compatibility>
     */
    public function getCompatibilities(): Collection
    {
        return $this->compatibilities;
    }

    public function addCompatibility(Compatibility $compatibility): self
    {
        if (!$this->compatibilities->contains($compatibility)) {
            $this->compatibilities[] = $compatibility;
            $compatibility->addFile($this);
        }

        return $this;
    }

    public function removeCompatibility(Compatibility $compatibility): self
    {
        if ($this->compatibilities->removeElement($compatibility)) {
            $compatibility->removeFile($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, Championship>
     */
    public function getChampionships(): Collection
    {
        return $this->championships;
    }

    public function addChampionship(Championship $championship): self
    {
        if (!$this->championships->contains($championship)) {
            $this->championships[] = $championship;
            $championship->setFile($this);
        }

        return $this;
    }

    public function removeChampionship(Championship $championship): self
    {
        if ($this->championships->removeElement($championship)) {
            // set the owning side to null (unless already changed)
            if ($championship->getFile() === $this) {
                $championship->setFile(null);
            }
        }

        return $this;
    }

    public function getDuration(): ?int
    {
        return $this->duration;
    }

    public function setDuration(?int $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * @return Collection<int, Sport>
     */
    public function getSports(): Collection
    {
        return $this->sports;
    }

    public function addSport(Sport $sport): self
    {
        if (!$this->sports->contains($sport)) {
            $this->sports[] = $sport;
            $sport->addFile($this);
        }

        return $this;
    }

    public function removeSport(Sport $sport): self
    {
        if ($this->sports->removeElement($sport)) {
            $sport->removeFile($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, Film>
     */
    public function getFilms(): Collection
    {
        return $this->films;
    }

    public function addFilm(Film $film): self
    {
        if (!$this->films->contains($film)) {
            $this->films[] = $film;
            $film->addFile($this);
        }

        return $this;
    }

    public function removeFilm(Film $film): self
    {
        if ($this->films->removeElement($film)) {
            $film->removeFile($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, Book>
     */
    public function getBooks(): Collection
    {
        return $this->books;
    }

    public function addBook(Book $book): self
    {
        if (!$this->books->contains($book)) {
            $this->books[] = $book;
            $book->addFile($this);
        }

        return $this;
    }

    public function removeBook(Book $book): self
    {
        if ($this->books->removeElement($book)) {
            $book->removeFile($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, Device>
     */
    public function getDevices(): Collection
    {
        return $this->devices;
    }

    public function addDevice(Device $device): self
    {
        if (!$this->devices->contains($device)) {
            $this->devices[] = $device;
            $device->addFile($this);
        }

        return $this;
    }

    public function removeDevice(Device $device): self
    {
        if ($this->devices->removeElement($device)) {
            $device->removeFile($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, Serie>
     */
    public function getSeries(): Collection
    {
        return $this->series;
    }

    public function addSeries(Serie $series): self
    {
        if (!$this->series->contains($series)) {
            $this->series[] = $series;
            $series->addFile($this);
        }

        return $this;
    }

    public function removeSeries(Serie $series): self
    {
        if ($this->series->removeElement($series)) {
            $series->removeFile($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, UserHasScore>
     */
    public function getUserHasScores(): Collection
    {
        return $this->userHasScores;
    }

    public function addUserHasScore(UserHasScore $userHasScore): self
    {
        if (!$this->userHasScores->contains($userHasScore)) {
            $this->userHasScores[] = $userHasScore;
            $userHasScore->setFile($this);
        }

        return $this;
    }

    public function removeUserHasScore(UserHasScore $userHasScore): self
    {
        if ($this->userHasScores->removeElement($userHasScore)) {
            // set the owning side to null (unless already changed)
            if ($userHasScore->getFile() === $this) {
                $userHasScore->setFile(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        $this->users->removeElement($user);

        return $this;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(?int $position): self
    {
        $this->position = $position;

        return $this;
    }
}
