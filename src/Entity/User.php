<?php

namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\OrderBy;
use Symfony\Component\Security\Core\User\UserInterface;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Annotation\ApiProperty;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ApiResource(
 *     normalizationContext={"groups"={"read"}, "enable_max_depth"="true"},
 *     attributes={"order"={"username": "ASC"}}
 * )
 * @ApiFilter(SearchFilter::class, properties={"email"="exact"})
 */
class User implements UserInterface, \Serializable
{
    /**
     * @var int
     * @ApiProperty(identifier=false)
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"read"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=45)
     * @Groups({"read"})
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="surname", type="string", length=45)
     * @Groups({"read"})
     */
    private $surname;

    /**
     * @var string
     *
     * @ApiProperty(identifier=true)
     * @ORM\Column(name="username", type="string", length=45, unique=true)
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=88, nullable=true)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="salt", type="string", length=23, nullable=true)
     */
    private $salt;

    /**
     * @var string
     *
     * @ORM\Column(name="role", type="string", length=50, nullable=true)
     */
    private $role;

    /**
     * @ORM\OneToMany(targetEntity="UserHasScore", mappedBy="user")
     */
    private $userScores;

    /**
     * @ORM\ManyToMany(targetEntity="Game", mappedBy="users")
     * @OrderBy({"name" = "ASC"})
     */
    private $games;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"read"})
     */
    private $gamername;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Activity", mappedBy="users")
     * @OrderBy({"start" = "ASC"})
     */
    private $activities;

    /**
     * @ORM\Column(type="string", length=255, nullable=true, unique=true)
     * @Groups({"read"})
     */
    private $email;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $lastSentData;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $association;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"read"})
     */
    private $youtubeEmbedId;

    /**
     * @ORM\ManyToMany(targetEntity=Album::class, inversedBy="users")
     */
    private $albums;

    /**
     * @ORM\ManyToMany(targetEntity=Timeline::class, inversedBy="users")
     */
    private $timelines;

    /**
     * @ORM\OneToMany(targetEntity=UserKpi::class, mappedBy="user")
     * @OrderBy({"total" = "DESC"})
     * @Groups({"read"})
     * @MaxDepth(1)
     */
    private $userKpis;

    /**
     * @ORM\OneToMany(targetEntity=Compatibility::class, mappedBy="creator")
     */
    private $compatibilities;

    /**
     * @ORM\ManyToMany(targetEntity=Progression::class, mappedBy="users")
     */
    private $progressions;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"read"})
     */
    private $slides;

    /**
     * @ORM\ManyToMany(targetEntity=File::class)
     */
    private $files;

    public function __construct() {
        $this->userScores = new ArrayCollection();
        $this->games = new ArrayCollection();
        $this->activities = new ArrayCollection();
        $this->albums = new ArrayCollection();
        $this->timelines = new ArrayCollection();
        $this->userKpis = new ArrayCollection();
        $this->compatibilities = new ArrayCollection();
        $this->progressions = new ArrayCollection();
        $this->files = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set salt
     *
     * @param string $salt
     *
     * @return User
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * Get salt
     *
     * @return string
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * Set role
     *
     * @param string $role
     *
     * @return User
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Add userScore
     *
     * @param \App\Entity\UserHasScore $userScore
     *
     * @return Place
     */
    public function addUserScore(\App\Entity\UserHasScore $userScore)
    {
        $this->userScores[] = $userScore;

        return $this;
    }

    /**
     * Remove userScore
     *
     * @param \App\Entity\UserHasScore $userScore
     */
    public function removeUserScore(\App\Entity\UserHasScore $userScore)
    {
        $this->userScores->removeElement($userScore);
    }

    /**
     * Get userScores
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserScores()
    {
        return $this->userScores;
    }

    /**
     * Set surname
     *
     * @param string $surname
     *
     * @return User
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * Get surname
     *
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Add game
     *
     * @param \App\Entity\Game $game
     *
     * @return User
     */
    public function addGame(\App\Entity\Game $game)
    {
        $this->games[] = $game;

        return $this;
    }

    /**
     * Remove game
     *
     * @param \App\Entity\Game $game
     */
    public function removeGame(\App\Entity\Game $game)
    {
        $this->games->removeElement($game);
    }

    /**
     * Get games
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGames()
    {
        return $this->games;
    }

    public function getRoles()
    {
        return [$this->getRole()];
    }

    public function eraseCredentials()
    {
    }


    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            $this->salt,
        ));
    }

    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            $this->salt
        ) = unserialize($serialized);
    }

    public function getGamername(): ?string
    {
        return $this->gamername;
    }

    public function setGamername(?string $gamername): self
    {
        $this->gamername = $gamername;

        return $this;
    }

    /**
     * @return Collection|Activity[]
     */
    public function getActivities(): Collection
    {
        return $this->activities;
    }

    public function addActivity(Activity $activity): self
    {
        if (!$this->activities->contains($activity)) {
            $this->activities[] = $activity;
            $activity->addUser($this);
        }

        return $this;
    }

    public function removeActivity(Activity $activity): self
    {
        if ($this->activities->contains($activity)) {
            $this->activities->removeElement($activity);
            $activity->removeUser($this);
        }

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getLastSentData(): ?\DateTimeInterface
    {
        return $this->lastSentData;
    }

    public function setLastSentData(?\DateTimeInterface $lastSentData): self
    {
        $this->lastSentData = $lastSentData;

        return $this;
    }

    public function getAssociation(): ?string
    {
        return $this->association;
    }

    public function setAssociation(?string $association): self
    {
        $this->association = $association;

        return $this;
    }

    public function getYoutubeEmbedId(): ?string
    {
        return $this->youtubeEmbedId;
    }

    public function setYoutubeEmbedId(?string $youtubeEmbedId): self
    {
        $this->youtubeEmbedId = $youtubeEmbedId;

        return $this;
    }

    /**
     * @return Collection|Album[]
     */
    public function getAlbums(): Collection
    {
        return $this->albums;
    }

    public function addAlbum(Album $album): self
    {
        if (!$this->albums->contains($album)) {
            $this->albums[] = $album;
        }

        return $this;
    }

    public function removeAlbum(Album $album): self
    {
        $this->albums->removeElement($album);

        return $this;
    }

    /**
     * @return Collection|Timeline[]
     */
    public function getTimelines(): Collection
    {
        return $this->timelines;
    }

    public function addTimeline(Timeline $timeline): self
    {
        if (!$this->timelines->contains($timeline)) {
            $this->timelines[] = $timeline;
        }

        return $this;
    }

    public function removeTimeline(Timeline $timeline): self
    {
        $this->timelines->removeElement($timeline);

        return $this;
    }

    /**
     * @return Collection|UserKpi[]
     */
    public function getUserKpis(): Collection
    {
        return $this->userKpis;
    }

    public function addUserKpi(UserKpi $userKpi): self
    {
        if (!$this->userKpis->contains($userKpi)) {
            $this->userKpis[] = $userKpi;
            $userKpi->setUser($this);
        }

        return $this;
    }

    public function removeUserKpi(UserKpi $userKpi): self
    {
        if ($this->userKpis->removeElement($userKpi)) {
            // set the owning side to null (unless already changed)
            if ($userKpi->getUser() === $this) {
                $userKpi->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Compatibility[]
     */
    public function getCompatibilities(): Collection
    {
        return $this->compatibilities;
    }

    public function addCompatibility(Compatibility $compatibility): self
    {
        if (!$this->compatibilities->contains($compatibility)) {
            $this->compatibilities[] = $compatibility;
            $compatibility->setCreator($this);
        }

        return $this;
    }

    public function removeCompatibility(Compatibility $compatibility): self
    {
        if ($this->compatibilities->removeElement($compatibility)) {
            // set the owning side to null (unless already changed)
            if ($compatibility->getCreator() === $this) {
                $compatibility->setCreator(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Progression>
     */
    public function getProgressions(): Collection
    {
        return $this->progressions;
    }

    public function addProgression(Progression $progression): self
    {
        if (!$this->progressions->contains($progression)) {
            $this->progressions[] = $progression;
            $progression->addUser($this);
        }

        return $this;
    }

    public function removeProgression(Progression $progression): self
    {
        if ($this->progressions->removeElement($progression)) {
            $progression->removeUser($this);
        }

        return $this;
    }

    public function getSlides(): ?int
    {
        return $this->slides;
    }

    public function setSlides(?int $slides): self
    {
        $this->slides = $slides;

        return $this;
    }

    /**
     * @return Collection<int, File>
     */
    public function getFiles(): Collection
    {
        return $this->files;
    }

    public function addFile(File $file): self
    {
        if (!$this->files->contains($file)) {
            $this->files[] = $file;
        }

        return $this;
    }

    public function removeFile(File $file): self
    {
        $this->files->removeElement($file);

        return $this;
    }
}
