<?php

namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Doctrine\ORM\Mapping\OrderBy;

/**
 * Score
 *
 * @ORM\Table(name="score")
 * @ORM\Entity(repositoryClass="App\Repository\ScoreRepository")
 * @ApiResource(
 *     normalizationContext={"groups"={"read"}, "enable_max_depth"="true"}
 * )
 */
class Score
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"read"})
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="home_away_difference", type="float", nullable=true)
     */
    private $homeAwayDifference;

    /**
     * @var float
     *
     * @ORM\Column(name="home_points", type="float", nullable=true)
     * @Groups({"read"})
     */
    private $homePoints;

    /**
     * @var float
     *
     * @ORM\Column(name="away_points", type="float", nullable=true)
     * @Groups({"read"})
     */
    private $awayPoints;

    /**
     * @var int
     *
     * @ORM\Column(name="home_players", type="integer", nullable=true)
     * @Groups({"read"})
     */
    private $homePlayers;

    /**
     * @var int
     *
     * @ORM\Column(name="away_players", type="integer", nullable=true)
     * @Groups({"read"})
     */
    private $awayPlayers;

    /**
     * One Score has One Activity.
     * @ORM\OneToOne(targetEntity="Activity", inversedBy="score", cascade={"persist"})
     * @ORM\JoinColumn(name="activity_id", referencedColumnName="id")
     * @Groups({"read"})
     */
    private $activity;

    /**
     * @ORM\OneToMany(targetEntity="UserHasScore", mappedBy="score")
     */
    private $scoreUsers;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"read"})
     */
    private $home;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"read"})
     */
    private $away;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"read"})
     */
    private $difficulty;

    /**
     * @ORM\ManyToOne(targetEntity=Game::class, inversedBy="scores")
     * @Groups({"read"})
     */
    private $game;

    /**
     * @ORM\ManyToOne(targetEntity=Type::class)
     * @Groups({"read"})
     */
    private $type;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"read"})
     */
    private $duration;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $tournament;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"read"})
     */
    private $description;

    /**
     * @ORM\ManyToMany(targetEntity=File::class)
     * @Groups({"read"})
     * @MaxDepth(1)
     * @OrderBy({"filename" = "ASC"})
     */
    private $files;

    /**
     * @ORM\ManyToMany(targetEntity=Profile::class, mappedBy="scores")
     */
    private $profiles;

    public function __construct() {
        $this->scoreUsers = new ArrayCollection();
        $this->files = new ArrayCollection();
        $this->profiles = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set homeAwayDifference
     *
     * @param float $homeAwayDifference
     *
     * @return Score
     */
    public function setHomeAwayDifference($homeAwayDifference)
    {
        $this->homeAwayDifference = $homeAwayDifference;

        return $this;
    }

    /**
     * Get homeAwayDifference
     *
     * @return float
     */
    public function getHomeAwayDifference()
    {
        return $this->homeAwayDifference;
    }

    /**
     * Set homePoints
     *
     * @param float $homePoints
     *
     * @return Score
     */
    public function setHomePoints($homePoints)
    {
        $this->homePoints = $homePoints;

        return $this;
    }

    /**
     * Get homePoints
     *
     * @return float
     */
    public function getHomePoints()
    {
        return $this->homePoints;
    }

    /**
     * Set awayPoints
     *
     * @param float $awayPoints
     *
     * @return Score
     */
    public function setAwayPoints($awayPoints)
    {
        $this->awayPoints = $awayPoints;

        return $this;
    }

    /**
     * Get awayPoints
     *
     * @return float
     */
    public function getAwayPoints()
    {
        return $this->awayPoints;
    }

    /**
     * Set homePlayers
     *
     * @param integer $homePlayers
     *
     * @return Score
     */
    public function setHomePlayers($homePlayers)
    {
        $this->homePlayers = $homePlayers;

        return $this;
    }

    /**
     * Get homePlayers
     *
     * @return int
     */
    public function getHomePlayers()
    {
        return $this->homePlayers;
    }

    /**
     * Set awayPlayers
     *
     * @param integer $awayPlayers
     *
     * @return Score
     */
    public function setAwayPlayers($awayPlayers)
    {
        $this->awayPlayers = $awayPlayers;

        return $this;
    }

    /**
     * Get awayPlayers
     *
     * @return int
     */
    public function getAwayPlayers()
    {
        return $this->awayPlayers;
    }

    /**
     * Add userScore
     *
     * @param \App\Entity\UserHasScore $userScore
     *
     * @return Place
     */
    public function addScoreUsers(\App\Entity\UserHasScore $userScore)
    {
        $this->scoreUsers[] = $userScore;

        return $this;
    }

    /**
     * Remove userScore
     *
     * @param \App\Entity\UserHasScore $userScore
     */
    public function removeScoreUsers(\App\Entity\UserHasScore $userScore)
    {
        $this->scoreUsers->removeElement($userScore);
    }

    /**
     * Get userScores
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getScoreUsers()
    {
        return $this->scoreUsers;
    }

    /**
     * Set activity
     *
     * @param \App\Entity\Activity $activity
     *
     * @return Score
     */
    public function setActivity(\App\Entity\Activity $activity = null)
    {
        $this->activity = $activity;

        return $this;
    }

    /**
     * Get activity
     *
     * @return \App\Entity\Activity
     */
    public function getActivity()
    {
        return $this->activity;
    }

    /**
     * Add scoreUser
     *
     * @param \App\Entity\UserHasScore $scoreUser
     *
     * @return Score
     */
    public function addScoreUser(\App\Entity\UserHasScore $scoreUser)
    {
        $this->scoreUsers[] = $scoreUser;

        return $this;
    }

    /**
     * Remove scoreUser
     *
     * @param \App\Entity\UserHasScore $scoreUser
     */
    public function removeScoreUser(\App\Entity\UserHasScore $scoreUser)
    {
        $this->scoreUsers->removeElement($scoreUser);
    }

    public function getHome(): ?string
    {
        return $this->home;
    }

    public function setHome(?string $home): self
    {
        $this->home = $home;

        return $this;
    }

    public function getAway(): ?string
    {
        return $this->away;
    }

    public function setAway(?string $away): self
    {
        $this->away = $away;

        return $this;
    }

    public function getDifficulty(): ?string
    {
        return $this->difficulty;
    }

    public function setDifficulty(?string $difficulty): self
    {
        $this->difficulty = $difficulty;

        return $this;
    }

    public function getGame(): ?Game
    {
        return $this->game;
    }

    public function setGame(?Game $game): self
    {
        $this->game = $game;

        return $this;
    }

    public function getType(): ?Type
    {
        return $this->type;
    }

    public function setType(?Type $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getDuration(): ?int
    {
        return $this->duration;
    }

    public function setDuration(?int $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    public function getTournament(): ?string
    {
        return $this->tournament;
    }

    public function setTournament(?string $tournament): self
    {
        $this->tournament = $tournament;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection<int, File>
     */
    public function getFiles(): Collection
    {
        return $this->files;
    }

    public function addFile(File $file): self
    {
        if (!$this->files->contains($file)) {
            $this->files[] = $file;
        }

        return $this;
    }

    public function removeFile(File $file): self
    {
        $this->files->removeElement($file);

        return $this;
    }

    /**
     * @return Collection<int, Profile>
     */
    public function getProfiles(): Collection
    {
        return $this->profiles;
    }

    public function addProfile(Profile $profile): self
    {
        if (!$this->profiles->contains($profile)) {
            $this->profiles[] = $profile;
            $profile->addScore($this);
        }

        return $this;
    }

    public function removeProfile(Profile $profile): self
    {
        if ($this->profiles->removeElement($profile)) {
            $profile->removeScore($this);
        }

        return $this;
    }
}
