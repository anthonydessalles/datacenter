<?php

namespace App\Entity;

use App\Repository\FilmRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Serializer\Filter\GroupFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\ExistsFilter;
use Doctrine\ORM\Mapping\OrderBy;

/**
 * @ORM\Entity(repositoryClass=FilmRepository::class)
 * @ApiResource(
 *     normalizationContext={"groups"={"read"}, "enable_max_depth"="true"},
 *     attributes={"order"={"title": "ASC"}}
 * )
 * @ApiFilter(GroupFilter::class, arguments={"parameterName"="groups", "overrideDefaultGroups"="true"})
 * @ApiFilter(SearchFilter::class, properties={"types.id"="exact", "title"="partial", "year"="exact"})
 * @ApiFilter(ExistsFilter::class, properties={"ofTheYear"})
 */
class Film
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"read", "list"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"read", "list"})
     */
    private $slug;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $allocine;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"read", "list"})
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $frTitle;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imdb;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $enTitle;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"read", "list"})
     */
    private $year;

    /**
     * @ORM\ManyToMany(targetEntity=Type::class, inversedBy="films")
     * @Groups({"read"})
     * @MaxDepth(1)
     * @OrderBy({"slug" = "ASC"})
     */
    private $types;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $max_height;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $max_width;

    /**
     * @ORM\ManyToMany(targetEntity=File::class, inversedBy="films")
     * @Groups({"read"})
     * @MaxDepth(1)
     */
    private $files;

    /**
     * @ORM\Column(type="integer", nullable=true, unique=true)
     * @Groups({"read", "list"})
     */
    private $ofTheYear;

    public function __construct()
    {
        $this->types = new ArrayCollection();
        $this->files = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getAllocine(): ?int
    {
        return $this->allocine;
    }

    public function setAllocine(?int $allocine): self
    {
        $this->allocine = $allocine;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getFrTitle(): ?string
    {
        return $this->frTitle;
    }

    public function setFrTitle(?string $frTitle): self
    {
        $this->frTitle = $frTitle;

        return $this;
    }

    public function getImdb(): ?string
    {
        return $this->imdb;
    }

    public function setImdb(?string $imdb): self
    {
        $this->imdb = $imdb;

        return $this;
    }

    public function getEnTitle(): ?string
    {
        return $this->enTitle;
    }

    public function setEnTitle(?string $enTitle): self
    {
        $this->enTitle = $enTitle;

        return $this;
    }

    public function getYear(): ?int
    {
        return $this->year;
    }

    public function setYear(int $year): self
    {
        $this->year = $year;

        return $this;
    }

    /**
     * @return Collection|Type[]
     */
    public function getTypes(): Collection
    {
        return $this->types;
    }

    public function addType(Type $type): self
    {
        if (!$this->types->contains($type)) {
            $this->types[] = $type;
        }

        return $this;
    }

    public function removeType(Type $type): self
    {
        $this->types->removeElement($type);

        return $this;
    }

    public function getMaxHeight(): ?int
    {
        return $this->max_height;
    }

    public function setMaxHeight(?int $max_height): self
    {
        $this->max_height = $max_height;

        return $this;
    }

    public function getMaxWidth(): ?int
    {
        return $this->max_width;
    }

    public function setMaxWidth(?int $max_width): self
    {
        $this->max_width = $max_width;

        return $this;
    }

    /**
     * @return Collection<int, File>
     */
    public function getFiles(): Collection
    {
        return $this->files;
    }

    public function addFile(File $file): self
    {
        if (!$this->files->contains($file)) {
            $this->files[] = $file;
        }

        return $this;
    }

    public function removeFile(File $file): self
    {
        $this->files->removeElement($file);

        return $this;
    }

    public function getOfTheYear(): ?int
    {
        return $this->ofTheYear;
    }

    public function setOfTheYear(?int $ofTheYear): self
    {
        $this->ofTheYear = $ofTheYear;

        return $this;
    }
}
