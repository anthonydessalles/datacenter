<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Serializer\Filter\GroupFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\ORM\Mapping\OrderBy;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SportRepository")
 * @ApiResource(
 *     normalizationContext={"groups"={"read"}, "enable_max_depth"="true"},
 *     attributes={"order"={"title": "ASC"}}
 * )
 * @ApiFilter(GroupFilter::class, arguments={"parameterName"="groups", "overrideDefaultGroups"="true"})
 * @ApiFilter(SearchFilter::class, properties={"types.id"="exact", "title"="partial"})
 */
class Sport
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"read", "list"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=128, nullable=false)
     * @Groups({"read", "list"})
     */
    private $slug;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    private $updatedAt;

    /**
     * @var string|null
     *
     * @ORM\Column(name="title", type="string", length=256, nullable=true)
     * @Groups({"read", "list"})
     */
    private $title;

    /**
     * @var string|null
     *
     * @ORM\Column(name="fr_title", type="string", length=256, nullable=true)
     */
    private $frTitle;

    /**
     * @var string|null
     *
     * @ORM\Column(name="en_title", type="string", length=256, nullable=true)
     */
    private $enTitle;

    /**
     * @var int
     *
     * @ORM\Column(name="year", type="integer", nullable=false)
     * @Groups({"read", "list"})
     */
    private $year = '0';

    /**
     * @ORM\ManyToMany(targetEntity=Type::class, inversedBy="videos")
     * @Groups({"read"})
     * @MaxDepth(1)
     * @OrderBy({"slug" = "ASC"})
     */
    private $types;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $max_height;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $max_width;

    /**
     * @ORM\ManyToMany(targetEntity=File::class, inversedBy="sports")
     * @Groups({"read"})
     * @MaxDepth(1)
     */
    private $files;

    /**
     * @ORM\ManyToMany(targetEntity=Link::class, inversedBy="sports")
     * @Groups({"read"})
     * @MaxDepth(1)
     * @OrderBy({"url" = "ASC"})
     */
    private $links;

    public function __construct()
    {
        $this->types = new ArrayCollection();
        $this->files = new ArrayCollection();
        $this->links = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getYear(): ?int
    {
        return $this->year;
    }

    public function setYear(int $year): self
    {
        $this->year = $year;

        return $this;
    }

    /**
     * @return Collection|Type[]
     */
    public function getTypes(): Collection
    {
        return $this->types;
    }

    public function addType(Type $type): self
    {
        if (!$this->types->contains($type)) {
            $this->types[] = $type;
        }

        return $this;
    }

    public function removeType(Type $type): self
    {
        $this->types->removeElement($type);

        return $this;
    }

    public function getFrTitle(): ?string
    {
        return $this->frTitle;
    }

    public function setFrTitle(?string $frTitle): Sport
    {
        $this->frTitle = $frTitle;
        return $this;
    }

    public function getEnTitle(): ?string
    {
        return $this->enTitle;
    }

    public function setEnTitle(?string $enTitle): Sport
    {
        $this->enTitle = $enTitle;
        return $this;
    }

    public function getMaxHeight(): ?int
    {
        return $this->max_height;
    }

    public function setMaxHeight(?int $max_height): self
    {
        $this->max_height = $max_height;

        return $this;
    }

    public function getMaxWidth(): ?int
    {
        return $this->max_width;
    }

    public function setMaxWidth(?int $max_width): self
    {
        $this->max_width = $max_width;

        return $this;
    }

    /**
     * @return Collection<int, File>
     */
    public function getFiles(): Collection
    {
        return $this->files;
    }

    public function addFile(File $file): self
    {
        if (!$this->files->contains($file)) {
            $this->files[] = $file;
        }

        return $this;
    }

    public function removeFile(File $file): self
    {
        $this->files->removeElement($file);

        return $this;
    }

    /**
     * @return Collection<int, Link>
     */
    public function getLinks(): Collection
    {
        return $this->links;
    }

    public function addLink(Link $link): self
    {
        if (!$this->links->contains($link)) {
            $this->links[] = $link;
        }

        return $this;
    }

    public function removeLink(Link $link): self
    {
        $this->links->removeElement($link);

        return $this;
    }
}
