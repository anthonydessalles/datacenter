<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OrderBy;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\ExistsFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

/**
 * Type
 *
 * @ORM\Table(name="type")
 * @ORM\Entity(repositoryClass="App\Repository\TypeRepository")
 * @ApiResource(
 *     normalizationContext={"groups"={"read"}},
 *     attributes={"order"={"slug": "ASC"}}
 * )
 * @ApiFilter(ExistsFilter::class, properties={"family"})
 * @ApiFilter(SearchFilter::class, properties={"slug"="partial"})
 */
class Type
{
    const DVD = 'dvd';
    const BLU_RAY = 'blu-ray';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"read", "list"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255, unique=true)
     * @Groups({"read", "list"})
     */
    private $slug;

    /**
     * @ORM\OneToMany(targetEntity="Activity", mappedBy="type")
     * @OrderBy({"start" = "ASC"})
     */
    private $activities;

    /**
     * @ORM\ManyToMany(targetEntity="Game", mappedBy="types")
     * @OrderBy({"name" = "ASC"})
     */
    private $games;

    /**
     * @ORM\ManyToMany(targetEntity=Sport::class, mappedBy="types")
     * @OrderBy({"title" = "ASC"})
     */
    private $sports;

    /**
     * @ORM\ManyToMany(targetEntity=Book::class, mappedBy="types")
     * @OrderBy({"title" = "ASC"})
     */
    private $books;

    /**
     * @ORM\ManyToMany(targetEntity=Device::class, mappedBy="types")
     * @OrderBy({"title" = "ASC"})
     */
    private $devices;

    /**
     * @ORM\OneToMany(targetEntity=Championship::class, mappedBy="type")
     */
    private $championships;

    /**
     * @ORM\OneToMany(targetEntity=UserKpi::class, mappedBy="type")
     */
    private $userKpis;

    /**
     * @ORM\ManyToMany(targetEntity=Film::class, mappedBy="types")
     */
    private $films;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"read"})
     */
    private $family;

    /**
     * @ORM\ManyToMany(targetEntity=Link::class, inversedBy="types")
     * @Groups({"read"})
     * @MaxDepth(1)
     * @OrderBy({"url" = "ASC"})
     */
    private $links;

    /**
     * @ORM\ManyToMany(targetEntity=Serie::class, mappedBy="types")
     */
    private $series;

    /**
     * @ORM\OneToMany(targetEntity=Compatibility::class, mappedBy="type")
     */
    private $compatibilities;

    /**
     * @ORM\ManyToMany(targetEntity=Album::class)
     * @Groups({"read"})
     * @MaxDepth(1)
     * @OrderBy({"title" = "ASC"})
     */
    private $albums;

    /**
     * @ORM\OneToMany(targetEntity=Progression::class, mappedBy="type")
     */
    private $progressions;

    /**
     * @ORM\ManyToMany(targetEntity=Profile::class, mappedBy="types")
     * @OrderBy({"name" = "ASC"})
     */
    private $profiles;

    /**
     * @ORM\ManyToMany(targetEntity=Music::class, mappedBy="types")
     */
    private $music;

    public function __construct() {
        $this->activities = new \Doctrine\Common\Collections\ArrayCollection();
        $this->games = new \Doctrine\Common\Collections\ArrayCollection();
        $this->sports = new ArrayCollection();
        $this->books = new ArrayCollection();
        $this->devices = new ArrayCollection();
        $this->championships = new ArrayCollection();
        $this->userKpis = new ArrayCollection();
        $this->films = new ArrayCollection();
        $this->links = new ArrayCollection();
        $this->series = new ArrayCollection();
        $this->compatibilities = new ArrayCollection();
        $this->albums = new ArrayCollection();
        $this->progressions = new ArrayCollection();
        $this->profiles = new ArrayCollection();
        $this->music = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Type
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Add activity
     *
     * @param \App\Entity\Activity $activity
     *
     * @return Type
     */
    public function addActivity(\App\Entity\Activity $activity)
    {
        $this->activities[] = $activity;

        return $this;
    }

    /**
     * Remove activity
     *
     * @param \App\Entity\Activity $activity
     */
    public function removeActivity(\App\Entity\Activity $activity)
    {
        $this->activities->removeElement($activity);
    }

    /**
     * Get activities
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getActivities()
    {
        return $this->activities;
    }

    /**
     * Add game
     *
     * @param \App\Entity\Game $game
     *
     * @return Type
     */
    public function addGame(\App\Entity\Game $game)
    {
        $this->games[] = $game;

        return $this;
    }

    /**
     * Remove game
     *
     * @param \App\Entity\Game $game
     */
    public function removeGame(\App\Entity\Game $game)
    {
        $this->games->removeElement($game);
    }

    /**
     * Get games
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGames()
    {
        return $this->games;
    }

    /**
     * @return Collection|Sport[]
     */
    public function getSports(): Collection
    {
        return $this->sports;
    }

    public function addSport(Sport $sport): self
    {
        if (!$this->sports->contains($sport)) {
            $this->sports[] = $sport;
            $sport->addType($this);
        }

        return $this;
    }

    public function removeSport(Sport $sport): self
    {
        if ($this->sports->removeElement($sport)) {
            $sport->removeType($this);
        }

        return $this;
    }

    /**
     * @return Collection|Book[]
     */
    public function getBooks(): Collection
    {
        return $this->books;
    }

    public function addBook(Book $book): self
    {
        if (!$this->books->contains($book)) {
            $this->books[] = $book;
            $book->addType($this);
        }

        return $this;
    }

    public function removeBook(Book $book): self
    {
        if ($this->books->removeElement($book)) {
            $book->removeType($this);
        }

        return $this;
    }

    /**
     * @return Collection|Device[]
     */
    public function getDevices(): Collection
    {
        return $this->devices;
    }

    public function addDevice(Device $device): self
    {
        if (!$this->devices->contains($device)) {
            $this->devices[] = $device;
            $device->addType($this);
        }

        return $this;
    }

    public function removeDevice(Device $device): self
    {
        if ($this->devices->removeElement($device)) {
            $device->removeType($this);
        }

        return $this;
    }

    /**
     * @return Collection|Championship[]
     */
    public function getChampionships(): Collection
    {
        return $this->championships;
    }

    public function addChampionship(Championship $championship): self
    {
        if (!$this->championships->contains($championship)) {
            $this->championships[] = $championship;
            $championship->setType($this);
        }

        return $this;
    }

    public function removeChampionship(Championship $championship): self
    {
        if ($this->championships->removeElement($championship)) {
            // set the owning side to null (unless already changed)
            if ($championship->getType() === $this) {
                $championship->setType(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserKpi[]
     */
    public function getUserKpis(): Collection
    {
        return $this->userKpis;
    }

    public function addUserKpi(UserKpi $userKpi): self
    {
        if (!$this->userKpis->contains($userKpi)) {
            $this->userKpis[] = $userKpi;
            $userKpi->setType($this);
        }

        return $this;
    }

    public function removeUserKpi(UserKpi $userKpi): self
    {
        if ($this->userKpis->removeElement($userKpi)) {
            // set the owning side to null (unless already changed)
            if ($userKpi->getType() === $this) {
                $userKpi->setType(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Film[]
     */
    public function getFilms(): Collection
    {
        return $this->films;
    }

    public function addFilm(Film $film): self
    {
        if (!$this->films->contains($film)) {
            $this->films[] = $film;
            $film->addType($this);
        }

        return $this;
    }

    public function removeFilm(Film $film): self
    {
        if ($this->films->removeElement($film)) {
            $film->removeType($this);
        }

        return $this;
    }

    public function getFamily(): ?string
    {
        return $this->family;
    }

    public function setFamily(?string $family): self
    {
        $this->family = $family;

        return $this;
    }

    /**
     * @return Collection|Link[]
     */
    public function getLinks(): Collection
    {
        return $this->links;
    }

    public function addLink(Link $link): self
    {
        if (!$this->links->contains($link)) {
            $this->links[] = $link;
        }

        return $this;
    }

    public function removeLink(Link $link): self
    {
        $this->links->removeElement($link);

        return $this;
    }

    /**
     * @return Collection|Serie[]
     */
    public function getSeries(): Collection
    {
        return $this->series;
    }

    public function addSeries(Serie $series): self
    {
        if (!$this->series->contains($series)) {
            $this->series[] = $series;
            $series->addType($this);
        }

        return $this;
    }

    public function removeSeries(Serie $series): self
    {
        if ($this->series->removeElement($series)) {
            $series->removeType($this);
        }

        return $this;
    }

    public function __toString(): string
    {
        return $this->getSlug();
    }

    /**
     * @return Collection|Compatibility[]
     */
    public function getCompatibilities(): Collection
    {
        return $this->compatibilities;
    }

    public function addCompatibility(Compatibility $compatibility): self
    {
        if (!$this->compatibilities->contains($compatibility)) {
            $this->compatibilities[] = $compatibility;
            $compatibility->setType($this);
        }

        return $this;
    }

    public function removeCompatibility(Compatibility $compatibility): self
    {
        if ($this->compatibilities->removeElement($compatibility)) {
            // set the owning side to null (unless already changed)
            if ($compatibility->getType() === $this) {
                $compatibility->setType(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Album>
     */
    public function getAlbums(): Collection
    {
        return $this->albums;
    }

    public function addAlbum(Album $album): self
    {
        if (!$this->albums->contains($album)) {
            $this->albums[] = $album;
        }

        return $this;
    }

    public function removeAlbum(Album $album): self
    {
        $this->albums->removeElement($album);

        return $this;
    }

    /**
     * @return Collection<int, Progression>
     */
    public function getProgressions(): Collection
    {
        return $this->progressions;
    }

    public function addProgression(Progression $progression): self
    {
        if (!$this->progressions->contains($progression)) {
            $this->progressions[] = $progression;
            $progression->setType($this);
        }

        return $this;
    }

    public function removeProgression(Progression $progression): self
    {
        if ($this->progressions->removeElement($progression)) {
            // set the owning side to null (unless already changed)
            if ($progression->getType() === $this) {
                $progression->setType(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Profile>
     */
    public function getProfiles(): Collection
    {
        return $this->profiles;
    }

    public function addProfile(Profile $profile): self
    {
        if (!$this->profiles->contains($profile)) {
            $this->profiles[] = $profile;
            $profile->addType($this);
        }

        return $this;
    }

    public function removeProfile(Profile $profile): self
    {
        if ($this->profiles->removeElement($profile)) {
            $profile->removeType($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, Music>
     */
    public function getMusic(): Collection
    {
        return $this->music;
    }

    public function addMusic(Music $music): self
    {
        if (!$this->music->contains($music)) {
            $this->music[] = $music;
            $music->addType($this);
        }

        return $this;
    }

    public function removeMusic(Music $music): self
    {
        if ($this->music->removeElement($music)) {
            $music->removeType($this);
        }

        return $this;
    }
}
