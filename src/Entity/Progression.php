<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\File;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\ORM\Mapping\OrderBy;

/**
 * Progression
 *
 * @ORM\Table(name="progression")
 * @ORM\Entity(repositoryClass="App\Repository\ProgressionRepository")
 * @ApiResource(
 *     normalizationContext={"groups"={"read", "list"}, "enable_max_depth"="true"},
 *     attributes={"order"={"name": "ASC"}}
 * )
 * @ApiFilter(SearchFilter::class, properties={"users.username", "exact"})
 */
class Progression
{
    const PROGRESSIONS = 'progressions';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"read", "list"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true))
     * @Groups({"read", "list"})
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="confrontation", type="string", length=255, nullable=true)
     */
    private $confrontation;

    /**
     * @var string
     *
     * @ORM\Column(name="difficulty", type="string", length=255, nullable=true)
     * @Groups({"read", "list"})
     */
    private $difficulty;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     * @Groups({"read", "list"})
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="File", inversedBy="progressions")
     * @ORM\JoinColumn(name="file_id", referencedColumnName="id")
     * @Groups({"read", "list"})
     */
    private $file;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, inversedBy="progressions")
     * @Groups({"read", "list"})
     * @MaxDepth(1)
     */
    private $users;

    /**
     * @ORM\ManyToOne(targetEntity=Game::class, inversedBy="progressions")
     * @Groups({"read", "list"})
     */
    private $game;

    /**
     * @ORM\ManyToOne(targetEntity=Type::class, inversedBy="progressions")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"read", "list"})
     */
    private $type;

    /**
     * @ORM\ManyToMany(targetEntity=File::class)
     * @Groups({"read", "list"})
     * @MaxDepth(1)
     * @OrderBy({"position" = "ASC", "filename" = "ASC"})
     */
    private $videos;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->videos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setName(string $name): Progression
    {
        $this->name = $name;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setConfrontation(string $confrontation): Progression
    {
        $this->confrontation = $confrontation;

        return $this;
    }

    public function getConfrontation(): ?string
    {
        return $this->confrontation;
    }

    public function setDifficulty(string $difficulty): Progression
    {
        $this->difficulty = $difficulty;

        return $this;
    }

    public function getDifficulty(): ?string
    {
        return $this->difficulty;
    }

    public function setDescription(string $description): Progression
    {
        $this->description = $description;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setFile(File $file = null): Progression
    {
        $this->file = $file;

        return $this;
    }

    public function getFile(): ?File
    {
        return $this->file;
    }

    /**
     * @return Collection<int, User>
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        $this->users->removeElement($user);

        return $this;
    }

    public function getGame(): ?Game
    {
        return $this->game;
    }

    public function setGame(?Game $game): self
    {
        $this->game = $game;

        return $this;
    }

    public function getType(): ?Type
    {
        return $this->type;
    }

    public function setType(?Type $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Collection<int, File>
     */
    public function getVideos(): Collection
    {
        return $this->videos;
    }

    public function addVideo(File $video): self
    {
        if (!$this->videos->contains($video)) {
            $this->videos[] = $video;
        }

        return $this;
    }

    public function removeVideo(File $video): self
    {
        $this->videos->removeElement($video);

        return $this;
    }
}
