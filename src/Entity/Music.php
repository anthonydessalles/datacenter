<?php

namespace App\Entity;

use App\Repository\MusicRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Serializer\Filter\GroupFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\ORM\Mapping\OrderBy;

/**
 * @ORM\Entity(repositoryClass=MusicRepository::class)
 * @ApiResource(
 *     normalizationContext={"groups"={"read"}, "enable_max_depth"="true"},
 *     attributes={"order"={"title": "ASC"}}
 * )
 * @ApiFilter(GroupFilter::class, arguments={"parameterName"="groups", "overrideDefaultGroups"="true"})
 * @ApiFilter(SearchFilter::class, properties={"types.id"="exact", "title"="partial"})
 */
class Music
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"read", "list"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"read", "list"})
     */
    private $title;

    /**
     * @ORM\ManyToMany(targetEntity=Link::class)
     * @Groups({"read"})
     * @MaxDepth(1)
     * @OrderBy({"url" = "ASC"})
     */
    private $links;

    /**
     * @ORM\ManyToMany(targetEntity=Type::class, inversedBy="music")
     * @Groups({"read"})
     * @MaxDepth(1)
     * @OrderBy({"slug" = "ASC"})
     */
    private $types;

    public function __construct()
    {
        $this->links = new ArrayCollection();
        $this->types = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return Collection<int, Link>
     */
    public function getLinks(): Collection
    {
        return $this->links;
    }

    public function addLink(Link $link): self
    {
        if (!$this->links->contains($link)) {
            $this->links[] = $link;
        }

        return $this;
    }

    public function removeLink(Link $link): self
    {
        $this->links->removeElement($link);

        return $this;
    }

    /**
     * @return Collection<int, Type>
     */
    public function getTypes(): Collection
    {
        return $this->types;
    }

    public function addType(Type $type): self
    {
        if (!$this->types->contains($type)) {
            $this->types[] = $type;
        }

        return $this;
    }

    public function removeType(Type $type): self
    {
        $this->types->removeElement($type);

        return $this;
    }
}
