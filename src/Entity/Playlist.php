<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping\OrderBy;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

/**
 * @ORM\Table(name="playlist")
 * @ORM\Entity(repositoryClass="App\Repository\PlaylistRepository")
 * @ApiResource(
 *     normalizationContext={"groups"={"read"}},
 *     attributes={"order"={"title": "ASC"}}
 * )
 * @ApiFilter(SearchFilter::class, properties={"status"="exact", "title"="partial"})
 */
class Playlist
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id", type="integer")
     * @Groups({"read", "list"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true, unique=true)
     * @Groups({"read", "list"})
     */
    private $externalId;

    /**
     * @ORM\Column(name="title", type="string", length=255)
     * @Groups({"read", "list"})
     */
    private $title;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"read", "list"})
     */
    private $publishedAt;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $status;

    /**
     * @ORM\Column(name="embed_html", type="text", nullable=true)
     * @Groups({"read", "list"})
     */
    private $embedHtml;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Source", inversedBy="playlists")
     */
    private $source;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Game", mappedBy="playlists")
     */
    private $games;

    /**
     * @ORM\ManyToMany(targetEntity=Profile::class, mappedBy="playlists")
     */
    private $profiles;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"read", "list"})
     */
    private $productUrl;

    public function __construct()
    {
        $this->games = new ArrayCollection();
        $this->profiles = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getExternalId(): ?string
    {
        return $this->externalId;
    }

    public function setExternalId(?string $externalId): self
    {
        $this->externalId = $externalId;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getPublishedAt(): ?\DateTimeInterface
    {
        return $this->publishedAt;
    }

    public function setPublishedAt(?\DateTimeInterface $publishedAt): self
    {
        $this->publishedAt = $publishedAt;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getEmbedHtml(): ?string
    {
        return $this->embedHtml;
    }

    public function setEmbedHtml(?string $embedHtml): self
    {
        $this->embedHtml = $embedHtml;

        return $this;
    }

    public function getSource(): ?Source
    {
        return $this->source;
    }

    public function setSource(?Source $source): self
    {
        $this->source = $source;

        return $this;
    }

    /**
     * @return Collection|Game[]
     */
    public function getGames(): Collection
    {
        return $this->games;
    }

    public function addGame(Game $game): self
    {
        if (!$this->games->contains($game)) {
            $this->games[] = $game;
            $game->addPlaylist($this);
        }

        return $this;
    }

    public function removeGame(Game $game): self
    {
        if ($this->games->contains($game)) {
            $this->games->removeElement($game);
            $game->removePlaylist($this);
        }

        return $this;
    }

    /**
     * @return Collection|Profile[]
     */
    public function getProfiles(): Collection
    {
        return $this->profiles;
    }

    public function addProfile(Profile $profile): self
    {
        if (!$this->profiles->contains($profile)) {
            $this->profiles[] = $profile;
            $profile->addPlaylist($this);
        }

        return $this;
    }

    public function removeProfile(Profile $profile): self
    {
        if ($this->profiles->removeElement($profile)) {
            $profile->removePlaylist($this);
        }

        return $this;
    }

    public function getProductUrl(): ?string
    {
        return $this->productUrl;
    }

    public function setProductUrl(?string $productUrl): self
    {
        $this->productUrl = $productUrl;

        return $this;
    }
}
