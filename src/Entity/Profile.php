<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OrderBy;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Serializer\Filter\GroupFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\ExistsFilter;

/**
 * @ORM\Table(name="profile")
 * @ORM\Entity(repositoryClass="App\Repository\ProfileRepository")
 * @ApiResource(
 *     normalizationContext={"groups"={"read"}, "enable_max_depth"="true"},
 *     attributes={"order"={"name": "ASC"}}
 * )
 * @ApiFilter(GroupFilter::class, arguments={"parameterName"="groups", "overrideDefaultGroups"="true"})
 * @ApiFilter(SearchFilter::class, properties={"types.id"="exact", "name"="partial"})
 * @ApiFilter(ExistsFilter::class, properties={"ofTheYear"})
 */
class Profile
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(name="id", type="integer")
     * @Groups({"read", "list"})
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=255)
     * @Groups({"read", "list"})
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity=Link::class, inversedBy="profiles")
     * @Groups({"read"})
     * @MaxDepth(1)
     * @OrderBy({"url" = "ASC"})
     */
    private $links;

    /**
     * @ORM\ManyToMany(targetEntity=Playlist::class, inversedBy="profiles")
     * @Groups({"read"})
     * @MaxDepth(1)
     * @OrderBy({"title" = "ASC"})
     */
    private $playlists;

    /**
     * @ORM\OneToMany(targetEntity=Championship::class, mappedBy="profile")
     * @Groups({"read"})
     * @MaxDepth(1)
     * @OrderBy({"title" = "ASC"})
     */
    private $championships;

    /**
     * @ORM\ManyToMany(targetEntity=Sport::class)
     * @Groups({"read"})
     * @MaxDepth(1)
     * @OrderBy({"title" = "ASC"})
     */
    private $sports;

    /**
     * @ORM\Column(type="integer", nullable=true, unique=true)
     * @Groups({"read", "list"})
     */
    private $ofTheYear;

    /**
     * @ORM\ManyToMany(targetEntity=Type::class, inversedBy="profiles")
     * @ORM\JoinTable(name="profile_type")
     * @Groups({"read"})
     * @MaxDepth(1)
     * @OrderBy({"slug" = "ASC"})
     */
    private $types;

    /**
     * @ORM\ManyToMany(targetEntity=Score::class, inversedBy="profiles")
     * @Groups({"read"})
     * @MaxDepth(1)
     */
    private $scores;

    /**
     * @ORM\ManyToMany(targetEntity=Recording::class, inversedBy="profiles")
     * @Groups({"read"})
     * @MaxDepth(1)
     */
    private $recordings;

    public function __construct()
    {
        $this->links = new ArrayCollection();
        $this->playlists = new ArrayCollection();
        $this->championships = new ArrayCollection();
        $this->sports = new ArrayCollection();
        $this->types = new ArrayCollection();
        $this->scores = new ArrayCollection();
        $this->recordings = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Link[]
     */
    public function getLinks(): Collection
    {
        return $this->links;
    }

    public function addLink(Link $link): self
    {
        if (!$this->links->contains($link)) {
            $this->links[] = $link;
        }

        return $this;
    }

    public function removeLink(Link $link): self
    {
        $this->links->removeElement($link);

        return $this;
    }

    /**
     * @return Collection|Playlist[]
     */
    public function getPlaylists(): Collection
    {
        return $this->playlists;
    }

    public function addPlaylist(Playlist $playlist): self
    {
        if (!$this->playlists->contains($playlist)) {
            $this->playlists[] = $playlist;
        }

        return $this;
    }

    public function removePlaylist(Playlist $playlist): self
    {
        $this->playlists->removeElement($playlist);

        return $this;
    }

    /**
     * @return Collection|Championship[]
     */
    public function getChampionships(): Collection
    {
        return $this->championships;
    }

    public function addChampionship(Championship $championship): self
    {
        if (!$this->championships->contains($championship)) {
            $this->championships[] = $championship;
            $championship->setProfile($this);
        }

        return $this;
    }

    public function removeChampionship(Championship $championship): self
    {
        if ($this->championships->removeElement($championship)) {
            // set the owning side to null (unless already changed)
            if ($championship->getProfile() === $this) {
                $championship->setProfile(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Sport>
     */
    public function getSports(): Collection
    {
        return $this->sports;
    }

    public function addSport(Sport $sport): self
    {
        if (!$this->sports->contains($sport)) {
            $this->sports[] = $sport;
        }

        return $this;
    }

    public function removeSport(Sport $sport): self
    {
        $this->sports->removeElement($sport);

        return $this;
    }

    public function getOfTheYear(): ?int
    {
        return $this->ofTheYear;
    }

    public function setOfTheYear(?int $ofTheYear): self
    {
        $this->ofTheYear = $ofTheYear;

        return $this;
    }

    /**
     * @return Collection<int, Type>
     */
    public function getTypes(): Collection
    {
        return $this->types;
    }

    public function addType(Type $type): self
    {
        if (!$this->types->contains($type)) {
            $this->types[] = $type;
        }

        return $this;
    }

    public function removeType(Type $type): self
    {
        $this->types->removeElement($type);

        return $this;
    }

    /**
     * @return Collection<int, Score>
     */
    public function getScores(): Collection
    {
        return $this->scores;
    }

    public function addScore(Score $score): self
    {
        if (!$this->scores->contains($score)) {
            $this->scores[] = $score;
        }

        return $this;
    }

    public function removeScore(Score $score): self
    {
        $this->scores->removeElement($score);

        return $this;
    }

    /**
     * @return Collection<int, Recording>
     */
    public function getRecordings(): Collection
    {
        return $this->recordings;
    }

    public function addRecording(Recording $recording): self
    {
        if (!$this->recordings->contains($recording)) {
            $this->recordings[] = $recording;
        }

        return $this;
    }

    public function removeRecording(Recording $recording): self
    {
        $this->recordings->removeElement($recording);

        return $this;
    }
}
