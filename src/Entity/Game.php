<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OrderBy;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Core\Serializer\Filter\GroupFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\ExistsFilter;

/**
 * Game
 *
 * @ORM\Table(name="game")
 * @ORM\Entity(repositoryClass="App\Repository\GameRepository")
 * @ApiResource(
 *     normalizationContext={"groups"={"read"}, "enable_max_depth"="true"},
 *     attributes={"order"={"name": "ASC"}}
 * )
 * @ApiFilter(BooleanFilter::class, properties={"isEnabled"})
 * @ApiFilter(GroupFilter::class, arguments={"parameterName"="groups", "overrideDefaultGroups"="true"})
 * @ApiFilter(SearchFilter::class, properties={"types.id"="exact", "name"="partial", "date"="partial"})
 * @ApiFilter(ExistsFilter::class, properties={"ofTheYear"})
 */
class Game
{
    const GAMES = 'games';
    const GAMES_DIFFICULTIES = [
        '***',
        'Hard',
        'Legend',
        'Legendary',
        'Professional',
        'World Class',
        'Ultimate',
    ];
    const POSSIBLE_SCORE_TYPES = [
        'football',
        'wrestling',
    ];
    const POSSIBLE_TYPES = [
        'DC',
        'DS',
        'GB',
        'GBA',
        'GBC',
        'GC',
        'MD',
        'N64',
        'NES',
        'PC',
        'PS1',
        'PS2',
        'PS3',
        'PS4',
        'PSP',
        'SMS',
        'SNES',
        'Steam',
        'Switch',
        'Wii',
        'XB',
        'XB360'
    ];

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"read", "list"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Groups({"read", "list"})
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255)
     * @Groups({"read", "list"})
     */
    private $slug;

    /**
     * @ORM\ManyToMany(targetEntity="User", inversedBy="games")
     * @ORM\JoinTable(name="game_has_user")
     */
    private $users;

    /**
     * @ORM\ManyToMany(targetEntity="Type", inversedBy="games")
     * @ORM\JoinTable(name="game_has_type")
     * @Groups({"read"})
     * @MaxDepth(1)
     * @OrderBy({"slug" = "ASC"})
     */
    private $types;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"read", "list"})
     */
    private $date;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\File", inversedBy="games")
     * @Groups({"read"})
     * @MaxDepth(1)
     * @OrderBy({"date" = "ASC", "path" = "ASC", "filename" = "ASC"})
     */
    private $files;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Playlist", inversedBy="games")
     * @OrderBy({"title" = "ASC"})
     * @Groups({"read"})
     * @MaxDepth(1)
     * @OrderBy({"title" = "ASC"})
     */
    private $playlists;

    /**
     * @ORM\Column(type="boolean", options={"default":"1"})
     */
    private $isEnabled;

    /**
     * @ORM\ManyToMany(targetEntity=Album::class, inversedBy="games")
     * @Groups({"read"})
     * @MaxDepth(1)
     * @OrderBy({"title" = "ASC"})
     */
    private $albums;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $frenchName;

    /**
     * @ORM\OneToMany(targetEntity=Championship::class, mappedBy="game")
     */
    private $championships;

    /**
     * @ORM\OneToMany(targetEntity=Compatibility::class, mappedBy="game")
     * @Groups({"read"})
     * @MaxDepth(1)
     */
    private $compatibilities;

    /**
     * @ORM\OneToMany(targetEntity=Score::class, mappedBy="game")
     */
    private $scores;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"read"})
     */
    private $igdbId;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"read"})
     */
    private $igdbUrl;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $difficulties;

    /**
     * @ORM\ManyToOne(targetEntity=Game::class)
     * @Groups({"read"})
     */
    private $suggestion;

    /**
     * @ORM\OneToMany(targetEntity=Progression::class, mappedBy="game")
     * @Groups({"read"})
     * @MaxDepth(1)
     * @OrderBy({"name" = "ASC"})
     */
    private $progressions;

    /**
     * @ORM\Column(type="integer", nullable=true, unique=true)
     * @Groups({"read", "list"})
     */
    private $ofTheYear;

    /**
     * @ORM\ManyToMany(targetEntity=Screenshot::class, inversedBy="games")
     * @Groups({"read"})
     * @MaxDepth(1)
     * @OrderBy({"title" = "ASC"})
     */
    private $screenshots;

    public function __construct() {
        $this->users = new ArrayCollection();
        $this->types = new ArrayCollection();
        $this->files = new ArrayCollection();
        $this->playlists = new ArrayCollection();
        $this->albums = new ArrayCollection();
        $this->championships = new ArrayCollection();
        $this->compatibilities = new ArrayCollection();
        $this->scores = new ArrayCollection();
        $this->progressions = new ArrayCollection();
        $this->screenshots = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Game
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Game
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Add user
     *
     * @param \App\Entity\User $user
     *
     * @return Game
     */
    public function addUser(\App\Entity\User $user)
    {
        $this->users[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param \App\Entity\User $user
     */
    public function removeUser(\App\Entity\User $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Add type
     *
     * @param \App\Entity\Type $type
     *
     * @return Game
     */
    public function addType(\App\Entity\Type $type)
    {
        $this->types[] = $type;

        return $this;
    }

    /**
     * Remove type
     *
     * @param \App\Entity\Type $type
     */
    public function removeType(\App\Entity\Type $type)
    {
        $this->types->removeElement($type);
    }

    /**
     * Get types
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTypes()
    {
        return $this->types;
    }

    public function __toString()
    {
        return $this->getName();
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return Collection|File[]
     */
    public function getFiles(): Collection
    {
        return $this->files;
    }

    public function addFile(File $file): self
    {
        if (!$this->files->contains($file)) {
            $this->files[] = $file;
        }

        return $this;
    }

    public function removeFile(File $file): self
    {
        if ($this->files->contains($file)) {
            $this->files->removeElement($file);
        }

        return $this;
    }

    /**
     * @return Collection|Playlist[]
     */
    public function getPlaylists(): Collection
    {
        return $this->playlists;
    }

    public function addPlaylist(Playlist $playlist): self
    {
        if (!$this->playlists->contains($playlist)) {
            $this->playlists[] = $playlist;
        }

        return $this;
    }

    public function removePlaylist(Playlist $playlist): self
    {
        if ($this->playlists->contains($playlist)) {
            $this->playlists->removeElement($playlist);
        }

        return $this;
    }

    public function getIsEnabled(): ?bool
    {
        return $this->isEnabled;
    }

    public function setIsEnabled(bool $isEnabled): self
    {
        $this->isEnabled = $isEnabled;

        return $this;
    }

    /**
     * @return Collection|Album[]
     */
    public function getAlbums(): Collection
    {
        return $this->albums;
    }

    public function addAlbum(Album $album): self
    {
        if (!$this->albums->contains($album)) {
            $this->albums[] = $album;
        }

        return $this;
    }

    public function removeAlbum(Album $album): self
    {
        $this->albums->removeElement($album);

        return $this;
    }

    public function getFrenchName(): ?string
    {
        return $this->frenchName;
    }

    public function setFrenchName(?string $frenchName): self
    {
        $this->frenchName = $frenchName;

        return $this;
    }

    /**
     * @return Collection|Championship[]
     */
    public function getChampionships(): Collection
    {
        return $this->championships;
    }

    public function addChampionship(Championship $championship): self
    {
        if (!$this->championships->contains($championship)) {
            $this->championships[] = $championship;
            $championship->setGame($this);
        }

        return $this;
    }

    public function removeChampionship(Championship $championship): self
    {
        if ($this->championships->removeElement($championship)) {
            // set the owning side to null (unless already changed)
            if ($championship->getGame() === $this) {
                $championship->setGame(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Compatibility[]
     */
    public function getCompatibilities(): Collection
    {
        return $this->compatibilities;
    }

    public function addCompatibility(Compatibility $compatibility): self
    {
        if (!$this->compatibilities->contains($compatibility)) {
            $this->compatibilities[] = $compatibility;
            $compatibility->setGame($this);
        }

        return $this;
    }

    public function removeCompatibility(Compatibility $compatibility): self
    {
        if ($this->compatibilities->removeElement($compatibility)) {
            // set the owning side to null (unless already changed)
            if ($compatibility->getGame() === $this) {
                $compatibility->setGame(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Score[]
     */
    public function getScores(): Collection
    {
        return $this->scores;
    }

    public function getIgdbId(): ?int
    {
        return $this->igdbId;
    }

    public function setIgdbId(?int $igdbId): self
    {
        $this->igdbId = $igdbId;

        return $this;
    }

    public function getIgdbUrl(): ?string
    {
        return $this->igdbUrl;
    }

    public function setIgdbUrl(?string $igdbUrl): self
    {
        $this->igdbUrl = $igdbUrl;

        return $this;
    }

    public function getDifficulties(): ?string
    {
        return $this->difficulties;
    }

    public function setDifficulties(?string $difficulties): self
    {
        $this->difficulties = $difficulties;

        return $this;
    }

    public function getSuggestion(): ?self
    {
        return $this->suggestion;
    }

    public function setSuggestion(?self $suggestion): self
    {
        $this->suggestion = $suggestion;

        return $this;
    }

    /**
     * @return Collection<int, Progression>
     */
    public function getProgressions(): Collection
    {
        return $this->progressions;
    }

    public function addProgression(Progression $progression): self
    {
        if (!$this->progressions->contains($progression)) {
            $this->progressions[] = $progression;
            $progression->setGame($this);
        }

        return $this;
    }

    public function removeProgression(Progression $progression): self
    {
        if ($this->progressions->removeElement($progression)) {
            // set the owning side to null (unless already changed)
            if ($progression->getGame() === $this) {
                $progression->setGame(null);
            }
        }

        return $this;
    }

    public function getOfTheYear(): ?int
    {
        return $this->ofTheYear;
    }

    public function setOfTheYear(?int $ofTheYear): self
    {
        $this->ofTheYear = $ofTheYear;

        return $this;
    }

    /**
     * @return Collection<int, Screenshot>
     */
    public function getScreenshots(): Collection
    {
        return $this->screenshots;
    }

    public function addScreenshot(Screenshot $screenshot): self
    {
        if (!$this->screenshots->contains($screenshot)) {
            $this->screenshots[] = $screenshot;
        }

        return $this;
    }

    public function removeScreenshot(Screenshot $screenshot): self
    {
        $this->screenshots->removeElement($screenshot);

        return $this;
    }
}
