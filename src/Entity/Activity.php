<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Serializer\Filter\GroupFilter;

/**
 * Activity
 *
 * @ORM\Table(name="activity")
 * @ORM\Entity(repositoryClass="App\Repository\ActivityRepository")
 * @ApiResource(
 *     normalizationContext={"groups"={"read"}, "enable_max_depth"="true"},
 *     attributes={"order"={"start": "ASC"}}
 * )
 * @ApiFilter(DateFilter::class, properties={"start", "end"})
 * @ApiFilter(SearchFilter::class, properties={"users.email", "exact"})
 * @ApiFilter(SearchFilter::class, properties={"users.username", "exact"})
 * @ApiFilter(GroupFilter::class, arguments={"parameterName"="groups", "overrideDefaultGroups"="true"})
 */
class Activity
{
    const ACTIVITIES = 'activities';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"read", "list"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Groups({"read", "list"})
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start", type="datetime")
     * @Groups({"read", "list"})
     */
    private $start;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end", type="datetime", nullable=true)
     * @Groups({"read", "list"})
     */
    private $end;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     * @Groups({"read", "list"})
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="Place", inversedBy="activities")
     * @ORM\JoinColumn(name="place_id", referencedColumnName="id")
     */
    private $place;

    /**
     * One Activity has One Score.
     * @ORM\OneToOne(targetEntity="Score", mappedBy="activity", cascade={"persist"})
     */
    private $score;

    /**
     * @ORM\ManyToOne(targetEntity="Type", inversedBy="activities")
     * @ORM\JoinColumn(name="type_id", referencedColumnName="id")
     * @Groups({"read"})
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="event_id", type="string", length=255, nullable=true, unique=true)
     * @Groups({"read"})
     */
    private $eventId;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", inversedBy="activities")
     * @Groups({"read"})
     * @MaxDepth(1)
     */
    private $users;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Album", inversedBy="activities")
     * @Groups({"read"})
     * @MaxDepth(1)
     */
    private $albums;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Source", inversedBy="activities")
     */
    private $source;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->albums = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Activity
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set start
     *
     * @param \DateTime $start
     *
     * @return Activity
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * Get start
     *
     * @return \DateTime
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set end
     *
     * @param \DateTime $end
     *
     * @return Activity
     */
    public function setEnd($end)
    {
        $this->end = $end;

        return $this;
    }

    /**
     * Get end
     *
     * @return \DateTime
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Activity
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set place
     *
     * @param \App\Entity\Place $place
     *
     * @return Activity
     */
    public function setPlace(\App\Entity\Place $place = null)
    {
        $this->place = $place;

        return $this;
    }

    /**
     * Get place
     *
     * @return \App\Entity\Place
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * Set score
     *
     * @param \App\Entity\Score $score
     *
     * @return Activity
     */
    public function setScore(\App\Entity\Score $score = null)
    {
        $this->score = $score;
        $score->setActivity($this);

        return $this;
    }

    /**
     * Get score
     *
     * @return \App\Entity\Score
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * Set type
     *
     * @param \App\Entity\Type $type
     *
     * @return Data
     */
    public function setType(\App\Entity\Type $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \App\Entity\Type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set eventId
     *
     * @param string $eventId
     *
     * @return Activity
     */
    public function setEventId($eventId)
    {
        $this->eventId = $eventId;

        return $this;
    }

    /**
     * Get eventId
     *
     * @return string
     */
    public function getEventId()
    {
        return $this->eventId;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
        }

        return $this;
    }

    public function __toString()
    {
        return $this->getName()
            . " (" . $this->getStart()->format('Y-m-d H:i:s') . " - "
            . ($this->getEnd() ? $this->getEnd()->format('Y-m-d H:i:s') : "") . ")";
    }

    /**
     * @return Collection|Album[]
     */
    public function getAlbums(): Collection
    {
        return $this->albums;
    }

    public function addAlbum(Album $album): self
    {
        if (!$this->albums->contains($album)) {
            $this->albums[] = $album;
        }

        return $this;
    }

    public function removeAlbum(Album $album): self
    {
        if ($this->albums->contains($album)) {
            $this->albums->removeElement($album);
        }

        return $this;
    }

    public function getSource(): ?Source
    {
        return $this->source;
    }

    public function setSource(?Source $source): self
    {
        $this->source = $source;

        return $this;
    }
}
