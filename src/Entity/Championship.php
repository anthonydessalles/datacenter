<?php

namespace App\Entity;

use App\Repository\ChampionshipRepository;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Table(name="championship")
 * @ORM\Entity(repositoryClass=ChampionshipRepository::class)
 * @ApiResource(
 *     normalizationContext={"groups"={"read"}}
 * )
 */
class Championship
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id", type="integer")
     * @Groups({"read"})
     */
    private $id;

    /**
     * @ORM\Column(name="title", type="string", length=255)
     * @Groups({"read"})
     */
    private $title;

    /**
     * @ORM\Column(name="holder", type="string", length=255)
     * @Groups({"read"})
     */
    private $holder;

    /**
     * @ORM\Column(name="mode", type="string", length=255, nullable=true)
     * @Groups({"read"})
     */
    private $mode;

    /**
     * @ORM\ManyToOne(targetEntity=Game::class, inversedBy="championships")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"read"})
     */
    private $game;

    /**
     * @ORM\ManyToOne(targetEntity=Type::class, inversedBy="championships")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"read"})
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity=Link::class, inversedBy="championships")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"read"})
     */
    private $link;

    /**
     * @ORM\ManyToOne(targetEntity=Profile::class, inversedBy="championships")
     * @ORM\JoinColumn(nullable=false)
     */
    private $profile;

    /**
     * @ORM\ManyToOne(targetEntity=File::class, inversedBy="championships")
     * @Groups({"read"})
     */
    private $file;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getHolder(): ?string
    {
        return $this->holder;
    }

    public function setHolder(string $holder): self
    {
        $this->holder = $holder;

        return $this;
    }

    public function getMode(): ?string
    {
        return $this->mode;
    }

    public function setMode(?string $mode): self
    {
        $this->mode = $mode;

        return $this;
    }

    public function getGame(): ?Game
    {
        return $this->game;
    }

    public function setGame(?Game $game): self
    {
        $this->game = $game;

        return $this;
    }

    public function getType(): ?Type
    {
        return $this->type;
    }

    public function setType(?Type $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getLink(): ?Link
    {
        return $this->link;
    }

    public function setLink(?Link $link): self
    {
        $this->link = $link;

        return $this;
    }

    public function getProfile(): ?Profile
    {
        return $this->profile;
    }

    public function setProfile(?Profile $profile): self
    {
        $this->profile = $profile;

        return $this;
    }

    public function getFile(): ?File
    {
        return $this->file;
    }

    public function setFile(?File $file): self
    {
        $this->file = $file;

        return $this;
    }
}
