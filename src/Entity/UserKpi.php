<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\UserKpiRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * @ORM\Entity(repositoryClass=UserKpiRepository::class)
 * @ApiResource(
 *     normalizationContext={"groups"={"read"}, "enable_max_depth"="true"},
 *     attributes={"order"={"total": "DESC"}}
 * )
 */
class UserKpi
{
    const KPI_TYPE_ACTIVITY = 'activity';
    const KPI_TYPE_SCORE = 'score';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="userKpis")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity=Type::class, inversedBy="userKpis")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"read"})
     * @MaxDepth(1)
     */
    private $type;

    /**
     * @ORM\Column(type="float")
     * @Groups({"read"})
     */
    private $total;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"read"})
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Groups({"read"})
     */
    private $totalPoints;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Groups({"read"})
     */
    private $totalWins;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Groups({"read"})
     */
    private $totalDraws;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Groups({"read"})
     */
    private $totalLosses;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"read"})
     */
    private $kpiType;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getType(): ?Type
    {
        return $this->type;
    }

    public function setType(?Type $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getTotal(): ?float
    {
        return $this->total;
    }

    public function setTotal(float $total): self
    {
        $this->total = $total;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getTotalPoints(): ?float
    {
        return $this->totalPoints;
    }

    public function setTotalPoints(?float $totalPoints): self
    {
        $this->totalPoints = $totalPoints;

        return $this;
    }

    public function getTotalWins(): ?float
    {
        return $this->totalWins;
    }

    public function setTotalWins(?float $totalWins): self
    {
        $this->totalWins = $totalWins;

        return $this;
    }

    public function getTotalDraws(): ?float
    {
        return $this->totalDraws;
    }

    public function setTotalDraws(?float $totalDraws): self
    {
        $this->totalDraws = $totalDraws;

        return $this;
    }

    public function getTotalLosses(): ?float
    {
        return $this->totalLosses;
    }

    public function setTotalLosses(?float $totalLosses): self
    {
        $this->totalLosses = $totalLosses;

        return $this;
    }

    public function getKpiType(): ?string
    {
        return $this->kpiType;
    }

    public function setKpiType(string $kpiType): self
    {
        $this->kpiType = $kpiType;

        return $this;
    }
}
