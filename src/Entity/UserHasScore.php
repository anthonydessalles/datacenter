<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

/**
 * UserHasScore
 *
 * @ORM\Table(name="user_has_score")
 * @ORM\Entity(repositoryClass="App\Repository\UserHasScoreRepository")
 * @ApiResource(
 *     normalizationContext={"groups"={"read"}, "enable_max_depth"="true"}
 * )
 * @ApiFilter(SearchFilter::class, properties={"user.username", "exact"})
 */
class UserHasScore
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_home", type="boolean")
     * @Groups({"read"})
     */
    private $isHome;

    /**
     * @var float
     *
     * @ORM\Column(name="points", type="float", nullable=true)
     * @Groups({"read"})
     */
    private $points;

    /**
     * @var float
     *
     * @ORM\Column(name="assists", type="float", nullable=true)
     */
    private $assists;

    /**
     * @var float
     *
     * @ORM\Column(name="saves", type="float", nullable=true)
     */
    private $saves;

    /**
     * @ORM\ManyToOne(targetEntity="Score", inversedBy="scoreUsers")
     * @ORM\JoinColumn(name="score_id", referencedColumnName="id")
     * @Groups({"read"})
     */
    private $score;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="userScores")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $gameTime;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $distance;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $sprints;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $activity;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $maxSprint;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $averageSprint;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $maxShot;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $runningTime;

    /**
     * @ORM\ManyToOne(targetEntity=File::class, inversedBy="userHasScores")
     */
    private $file;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"read"})
     */
    private $isWin;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"read"})
     */
    private $isDraw;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"read"})
     */
    private $isLose;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set isHome
     *
     * @param boolean $isHome
     *
     * @return UserHasScore
     */
    public function setIsHome($isHome)
    {
        $this->isHome = $isHome;

        return $this;
    }

    /**
     * Get isHome
     *
     * @return bool
     */
    public function getIsHome()
    {
        return $this->isHome;
    }

    /**
     * Set points
     *
     * @param float $points
     *
     * @return UserHasScore
     */
    public function setPoints($points = null)
    {
        $this->points = $points;

        return $this;
    }

    /**
     * Get points
     *
     * @return float
     */
    public function getPoints()
    {
        return $this->points;
    }

    /**
     * Set assists
     *
     * @param float $assists
     *
     * @return UserHasScore
     */
    public function setAssists($assists)
    {
        $this->assists = $assists;

        return $this;
    }

    /**
     * Get assists
     *
     * @return float
     */
    public function getAssists()
    {
        return $this->assists;
    }

    /**
     * Set saves
     *
     * @param float $saves
     *
     * @return UserHasScore
     */
    public function setSaves($saves)
    {
        $this->saves = $saves;

        return $this;
    }

    /**
     * Get saves
     *
     * @return float
     */
    public function getSaves()
    {
        return $this->saves;
    }

    /**
     * Set score
     *
     * @param \App\Entity\Score $score
     *
     * @return UserHasScore
     */
    public function setScore(\App\Entity\Score $score = null)
    {
        $this->score = $score;

        return $this;
    }

    /**
     * Get score
     *
     * @return \App\Entity\Score
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * Set user
     *
     * @param \App\Entity\User $user
     *
     * @return UserHasUser
     */
    public function setUser(\App\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \App\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    public function getGameTime(): ?int
    {
        return $this->gameTime;
    }

    public function setGameTime(?int $gameTime): self
    {
        $this->gameTime = $gameTime;

        return $this;
    }

    public function getDistance(): ?float
    {
        return $this->distance;
    }

    public function setDistance(?float $distance): self
    {
        $this->distance = $distance;

        return $this;
    }

    public function getSprints(): ?int
    {
        return $this->sprints;
    }

    public function setSprints(?int $sprints): self
    {
        $this->sprints = $sprints;

        return $this;
    }

    public function getActivity(): ?int
    {
        return $this->activity;
    }

    public function setActivity(?int $activity): self
    {
        $this->activity = $activity;

        return $this;
    }

    public function getMaxSprint(): ?float
    {
        return $this->maxSprint;
    }

    public function setMaxSprint(?float $maxSprint): self
    {
        $this->maxSprint = $maxSprint;

        return $this;
    }

    public function getAverageSprint(): ?float
    {
        return $this->averageSprint;
    }

    public function setAverageSprint(?float $averageSprint): self
    {
        $this->averageSprint = $averageSprint;

        return $this;
    }

    public function getMaxShot(): ?float
    {
        return $this->maxShot;
    }

    public function setMaxShot(?float $maxShot): self
    {
        $this->maxShot = $maxShot;

        return $this;
    }

    public function getRunningTime(): ?string
    {
        return $this->runningTime;
    }

    public function setRunningTime(?string $runningTime): self
    {
        $this->runningTime = $runningTime;

        return $this;
    }

    public function getFile(): ?File
    {
        return $this->file;
    }

    public function setFile(?File $file): self
    {
        $this->file = $file;

        return $this;
    }

    public function getIsWin(): ?bool
    {
        return $this->isWin;
    }

    public function setIsWin(?bool $isWin): self
    {
        $this->isWin = $isWin;

        return $this;
    }

    public function getIsDraw(): ?bool
    {
        return $this->isDraw;
    }

    public function setIsDraw(?bool $isDraw): self
    {
        $this->isDraw = $isDraw;

        return $this;
    }

    public function getIsLose(): ?bool
    {
        return $this->isLose;
    }

    public function setIsLose(?bool $isLose): self
    {
        $this->isLose = $isLose;

        return $this;
    }
}
