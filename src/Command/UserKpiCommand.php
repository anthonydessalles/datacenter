<?php

namespace App\Command;

use App\Entity\User;
use App\Manager\UserKpiManager;
use App\Repository\UserRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class UserKpiCommand extends Command
{
    protected static $defaultName = 'app:user:kpi';
    protected static $defaultDescription = 'Generate all the Kpis of users with emails';
    private UserRepository $userRepository;
    private UserKpiManager $userKpiManager;

    public function __construct(UserRepository $userRepository, UserKpiManager $userKpiManager)
    {
        $this->userRepository = $userRepository;
        $this->userKpiManager = $userKpiManager;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription(self::$defaultDescription)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        /** @var User $user */
        foreach ($this->userRepository->getUsersWithEmail() as $user) {
            $isSaved = $this->userKpiManager->saveUserKpis($user);

            $output->writeln($user->getUsername() . ' ' . $isSaved);
        }

        $io->success('Done.');

        return Command::SUCCESS;
    }
}
