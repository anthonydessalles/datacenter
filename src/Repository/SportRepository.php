<?php

namespace App\Repository;

use App\Entity\Sport;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class SportRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Sport::class);
    }

    public function getSports()
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT s
                FROM App\Entity\Sport s
                ORDER BY s.title ASC'
            )
            ->getResult();
    }

    public function getSportsFromName($name)
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT s
                FROM App\Entity\Sport s
                WHERE s.title LIKE :name 
                ORDER BY s.title ASC'
            )
            ->setParameter('name', '%'.$name.'%')
            ->getResult();
    }
}
