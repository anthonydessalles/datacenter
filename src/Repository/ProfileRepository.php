<?php

namespace App\Repository;

use App\Entity\Profile;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Profile|null find($id, $lockMode = null, $lockVersion = null)
 * @method Profile|null findOneBy(array $criteria, array $orderBy = null)
 * @method Profile[]    findAll()
 * @method Profile[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProfileRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Profile::class);
    }

    public function getEnrichedProfiles(): array
    {
        $enrichedProfiles = [];

        foreach($this->getProfiles() as $profile) {
            // If has Championships
            if (count($profile->getChampionships())) {
                $enrichedProfiles[$profile->getId()] = true;
            }

            // If has Scores
            if (count($profile->getScores())) {
                $enrichedProfiles[$profile->getId()] = true;
            }

            // If has Recordings
            if (count($profile->getRecordings())) {
                $enrichedProfiles[$profile->getId()] = true;
            }

            // If has Sports
            if (count($profile->getSports())) {
                $enrichedProfiles[$profile->getId()] = true;
            }
        }
        
        return $enrichedProfiles;
    }

    public function getProfiles()
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT p
                FROM App\Entity\Profile p
                ORDER BY p.name ASC'
            )
            ->getResult();
    }
}
