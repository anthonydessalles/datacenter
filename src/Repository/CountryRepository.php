<?php

namespace App\Repository;

use App\Entity\Country;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class CountryRepository
 * @package App\Repository
 */
class CountryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Country::class);
    }

    public function findAll()
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT c FROM App\Entity\Country c ORDER BY c.slug ASC'
            )
            ->getResult();
    }
}
