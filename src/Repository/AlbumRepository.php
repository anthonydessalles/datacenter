<?php

namespace App\Repository;

use App\Entity\Album;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Album|null find($id, $lockMode = null, $lockVersion = null)
 * @method Album|null findOneBy(array $criteria, array $orderBy = null)
 * @method Album[]    findAll()
 * @method Album[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AlbumRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Album::class);
    }

    /**
     * @param null $offset
     * @param null $limit
     * @return mixed
     */
    public function findAllOrderByName($offset = null, $limit = null)
    {
        $qb = $this->createQueryBuilder('a')
            ->orderBy('a.title', 'ASC');

        if ($offset) {
            $qb->setFirstResult($offset);
        }

        if ($limit) {
            $qb->setMaxResults($limit);
        }

        return $qb->getQuery()->execute();
    }

    public function getAlbumsFromTitle($title)
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT a
                FROM App\Entity\Album a
                WHERE a.title LIKE :title
                ORDER BY a.title ASC'
            )
            ->setParameter('title', '%'.$title.'%')
            ->getResult();
    }

    public function getEnrichedAlbums()
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT a
                FROM App\Entity\Album a
                WHERE a.shareableUrl IS NOT NULL OR a.activities IS NOT EMPTY OR a.users IS NOT EMPTY
                ORDER BY a.title ASC'
            )
            ->getResult();
    }
}
