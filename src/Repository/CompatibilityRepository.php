<?php

namespace App\Repository;

use App\Entity\Compatibility;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Compatibility|null find($id, $lockMode = null, $lockVersion = null)
 * @method Compatibility|null findOneBy(array $criteria, array $orderBy = null)
 * @method Compatibility[]    findAll()
 * @method Compatibility[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CompatibilityRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Compatibility::class);
    }

    public function getCompatibilities()
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT c
                FROM App\Entity\Compatibility c
                ORDER BY c.id ASC'
            )
            ->getResult();
    }
}
