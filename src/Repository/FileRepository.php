<?php

namespace App\Repository;

use App\Entity\File;
use App\Entity\Game;
use App\Entity\Source;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class FileRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, File::class);
    }

    /**
     * @param \DateTime $from
     * @param \DateTime $to
     * @param $order
     * @return mixed
     */
    public function findAllBetweenDates(\DateTime $from, \DateTime $to, $order)
    {
        $qb = $this->createQueryBuilder('f')
            ->where('f.date BETWEEN :from AND :to')
            ->orderBy('f.date', $order)
            ->setParameter('from', $from)
            ->setParameter('to', $to)
            ->getQuery();

        return $qb->execute();
    }

    public function getFiles($offset = null, $limit = null)
    {
        $query = $this->getEntityManager()
            ->createQuery(
                'SELECT f.id, f.position, f.filename, f.path, f.sourceUrl, f.date, f.description, f.size, f.height, f.width, f.duration, f.embedHtml, s.id as source_id
                FROM App\Entity\File f
                LEFT JOIN f.source s
                ORDER BY f.path ASC, f.filename ASC'
            );

        if ($offset) {
            $query->setFirstResult($offset);
        }

        if ($limit) {
            $query->setMaxResults($limit);
        }

        return $query->getResult();
    }

    public function getFilesFromFilename($filename)
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT f
                FROM App\Entity\File f
                WHERE f.filename LIKE :filename
                ORDER BY f.filename ASC'
            )
            ->setParameter('filename', '%'.$filename.'%')
            ->getResult();
    }

    public function getFilesPaths()
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT DISTINCT f.path
                FROM App\Entity\File f
                ORDER BY f.path ASC'
            )
            ->getResult();
    }

    public function getFilteredFiles(?string $path, ?Game $game, ?Source $source, int $max)
    {
        $dqlQuery = <<<DQL
SELECT f.id, f.filename, f.path, f.sourceUrl, f.date, f.description, f.size, f.height, f.width, f.duration, f.embedHtml, s.id as source_id
FROM App\Entity\File f
INNER JOIN f.source s
WHERE f.source = :source
DQL;

        $parameters['source'] = $source;

        if ($path) {
            $dqlQuery = <<<DQL
$dqlQuery AND f.path = :path
DQL;
            $parameters['path'] = $path;
        }

        if ($game) {
            $dqlQuery = <<<DQL
$dqlQuery AND :game
MEMBER OF f.games
DQL;
            $parameters['game'] = $game;
        }

        $dqlQuery = <<<DQL
$dqlQuery
ORDER BY f.path ASC, f.filename ASC
DQL;

        $query = $this->getEntityManager()
            ->createQuery($dqlQuery)
            ->setMaxResults($max);
        ;

        foreach ($parameters as $key => $value) {
            $query->setParameter($key, $value);
        }

        return $query->getResult();
    }

    public function getScreenshotFilesFromPath($path)
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT f
                FROM App\Entity\File f
                WHERE f.path = :path AND f.source = 4
                ORDER BY f.filename ASC'
            )
            ->setParameter('path', $path)
            ->getResult();
    }

    public function getVideoFilesFromFilename($filename)
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT f
                FROM App\Entity\File f
                WHERE f.filename LIKE :filename AND (f.filename LIKE :mp4 OR f.filename LIKE :mkv OR f.filename LIKE :mpg)
                ORDER BY f.filename ASC'
            )
            ->setParameter('filename', '%'.$filename.'%')
            ->setParameter('mp4', '%mp4%')
            ->setParameter('mkv', '%mkv%')
            ->setParameter('mpg', '%mpg%')
            ->getResult();
    }
}
