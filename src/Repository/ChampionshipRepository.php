<?php

namespace App\Repository;

use App\Entity\Championship;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\Game;
use App\Entity\Type;

/**
 * @method Championship|null find($id, $lockMode = null, $lockVersion = null)
 * @method Championship|null findOneBy(array $criteria, array $orderBy = null)
 * @method Championship[]    findAll()
 * @method Championship[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ChampionshipRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Championship::class);
    }

    public function getChampionshipFromId($id)
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT c
                FROM App\Entity\Championship c
                WHERE c.id = :id
                ORDER BY c.id ASC'
            )
            ->setParameter('id', $id)
            ->getResult();
    }

    public function getChampionships()
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT c
                FROM App\Entity\Championship c
                ORDER BY c.title ASC, c.holder ASC'
            )
            ->getResult();
    }

    public function getChampionshipsHolders()
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT DISTINCT c.holder
                FROM App\Entity\Championship c
                ORDER BY c.holder ASC'
            )
            ->getResult();
    }

    public function getChampionshipsTitles()
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT DISTINCT c.title
                FROM App\Entity\Championship c
                ORDER BY c.title ASC'
            )
            ->getResult();
    }

    public function getFilteredChampionships(string $title = null, string $holder = null, Game $game = null, Type $type = null)
    {
        $dqlQuery = <<<DQL
SELECT c
FROM App\Entity\Championship c
WHERE c.id > 0
DQL;
        $parameters = [];

        if ($title) {
            $dqlQuery = <<<DQL
$dqlQuery AND c.title = :title
DQL;
            $parameters['title'] = $title;
        }

        if ($holder) {
            $dqlQuery = <<<DQL
$dqlQuery AND c.holder = :holder
DQL;
            $parameters['holder'] = $holder;
        }

        if ($game) {
            $dqlQuery = <<<DQL
$dqlQuery AND c.game = :game
DQL;
            $parameters['game'] = $game;
        }

        if ($type) {
            $dqlQuery = <<<DQL
$dqlQuery AND c.type = :type
DQL;
            $parameters['type'] = $type;
        }

        $dqlQuery = <<<DQL
$dqlQuery
ORDER BY c.title ASC, c.holder ASC
DQL;

        $query = $this->getEntityManager()
            ->createQuery($dqlQuery);

        foreach ($parameters as $key => $value) {
            $query->setParameter($key, $value);
        }

        return $query->getResult();
    }
}
