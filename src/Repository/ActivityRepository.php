<?php

namespace App\Repository;

use App\Entity\Activity;
use App\Entity\Source;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class ActivityRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Activity::class);
    }

    /**
     * @param \DateTime $from
     * @param \DateTime $to
     * @param $order
     * @return mixed
     */
    public function findAllBetweenDates(\DateTime $from, \DateTime $to, $order)
    {
        $qb = $this->createQueryBuilder('a')
            ->where('a.start BETWEEN :from AND :to')
            ->orderBy('a.start', $order)
            ->setParameter('from', $from)
            ->setParameter('to', $to)
            ->getQuery();

        return $qb->execute();
    }

    /**
     * @param string $name
     * @return int|mixed|string
     */
    public function findAllByName(string $name)
    {
        $qb = $this->createQueryBuilder('a')
            ->where('a.name LIKE :name')
            ->setParameter('name', '%' . $name . '%')
            ->getQuery();

        return $qb->execute();
    }

    /**
     * @param $value
     * @param null $offset
     * @param null $limit
     * @return mixed
     */
    public function findAllOrderByStart($value, $offset = null, $limit = null)
    {
        $qb = $this->createQueryBuilder('a')
            ->orderBy('a.start', $value);

        if ($offset) {
            $qb->setFirstResult($offset);
        }

        if ($limit) {
            $qb->setMaxResults($limit);
        }

        return $qb->getQuery()->execute();
    }

    public function getFilteredActivities(Source $source, int $max)
    {
        $dqlQuery = <<<DQL
SELECT a
FROM App\Entity\Activity a
WHERE a.source = :source
ORDER BY a.start DESC
DQL;

        $parameters['source'] = $source;

        $query = $this->getEntityManager()
            ->createQuery($dqlQuery)
            ->setMaxResults($max);
        ;

        foreach ($parameters as $key => $value) {
            $query->setParameter($key, $value);
        }

        return $query->getResult();
    }
}
