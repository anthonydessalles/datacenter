<?php

namespace App\Repository;

use App\Entity\Game;
use App\Entity\Type;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class GameRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Game::class);
    }

    public function getEnrichedGames(): array
    {
        $enrichedGames = [];

        foreach($this->getGames() as $game) {
            // Compatibility >Bad
            foreach ($game->getCompatibilities() as $compatibility) {
                if (in_array($compatibility->getStatus(), ["Perfect","Great","Okay"])) {
                    $enrichedGames[$game->getId()] = true;
                }
            }

            // Steam Games
            foreach ($game->getTypes() as $type) {
                if ("steam" == $type->getSlug()) {
                    $enrichedGames[$game->getId()] = true;
                }
            }

            // If has suggestion
            if ($game->getSuggestion()) {
                $enrichedGames[$game->getId()] = true;
            }
        }
        
        return $enrichedGames;
    }

    public function getFilteredGames(?Type $type, int $max)
    {
        $dqlQuery = <<<DQL
SELECT g
FROM App\Entity\Game g
WHERE g.id IS NOT NULL
DQL;

        $parameters = [];

        if ($type) {
            $dqlQuery = <<<DQL
$dqlQuery AND :type
MEMBER OF g.types
DQL;
            $parameters['type'] = $type;
        }

        $dqlQuery = <<<DQL
$dqlQuery
ORDER BY g.name ASC
DQL;

        $query = $this->getEntityManager()
            ->createQuery($dqlQuery)
            ->setMaxResults($max);
        ;

        foreach ($parameters as $key => $value) {
            $query->setParameter($key, $value);
        }

        return $query->getResult();
    }

    public function getGames($offset = null, $limit = null)
    {
        $qb = $this->createQueryBuilder('g')
            ->orderBy('g.name', 'ASC');

        if ($offset) {
            $qb->setFirstResult($offset);
        }

        if ($limit) {
            $qb->setMaxResults($limit);
        }

        return $qb->getQuery()->execute();
    }

  	public function getGamesFromName($name)
  	{
  		return $this->getEntityManager()
              ->createQuery(
                  'SELECT g
                  FROM App\Entity\Game g
                  WHERE g.name LIKE :name
                  ORDER BY g.name ASC'
              )
              ->setParameter('name', '%'.$name.'%')
              ->getResult();
  	}
}
