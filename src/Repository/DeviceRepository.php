<?php

namespace App\Repository;

use App\Entity\Device;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class DeviceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Device::class);
    }

    public function getDevices()
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT d
                FROM App\Entity\Device d
                ORDER BY d.title ASC'
            )
            ->getResult();
    }

    public function getDevicesFromTitle($title)
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT d
                FROM App\Entity\Device d
                WHERE d.title LIKE :title
                ORDER BY d.title ASC'
            )
            ->setParameter('title', '%'.$title.'%')
            ->getResult();
    }
}
