<?php

namespace App\Repository;

use App\Entity\Album;
use App\Entity\Film;
use App\Entity\Type;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class FilmRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Film::class);
    }

    public function getFilms()
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT f
                FROM App\Entity\Film f
                ORDER BY f.title ASC'
            )
            ->getResult();
    }

    public function getFilmsFromName($name)
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT f
                FROM App\Entity\Film f
                WHERE f.title LIKE :name 
                ORDER BY f.title ASC'
            )
            ->setParameter('name', '%'.$name.'%')
            ->getResult();
    }

    public function getFilteredFilms(?int $year, ?Type $type, ?Album $album, int $max)
    {
        $dqlQuery = <<<DQL
SELECT f
FROM App\Entity\Film f
WHERE f.id IS NOT NULL
DQL;

        $parameters = [];
        if ($year) {
            $dqlQuery = <<<DQL
$dqlQuery AND f.year = :year
DQL;
            $parameters['year'] = $year;
        }

        if ($type) {
            $dqlQuery = <<<DQL
$dqlQuery AND :type
MEMBER OF f.types
DQL;
            $parameters['type'] = $type;
        }

        if ($album) {
            $dqlQuery = <<<DQL
$dqlQuery AND :album
MEMBER OF f.albums
DQL;
            $parameters['album'] = $album;
        }

        $dqlQuery = <<<DQL
$dqlQuery
ORDER BY f.title ASC
DQL;

        $query = $this->getEntityManager()
            ->createQuery($dqlQuery)
            ->setMaxResults($max);
        ;

        foreach ($parameters as $key => $value) {
            $query->setParameter($key, $value);
        }

        return $query->getResult();
    }
}
