<?php

namespace App\Repository;

use App\Entity\Serie;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Serie|null find($id, $lockMode = null, $lockVersion = null)
 * @method Serie|null findOneBy(array $criteria, array $orderBy = null)
 * @method Serie[]    findAll()
 * @method Serie[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SerieRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Serie::class);
    }

    public function getSeries()
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT s
                FROM App\Entity\Serie s
                ORDER BY s.title ASC'
            )
            ->getResult();
    }

    public function getSeriesFromName($name)
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT s
                FROM App\Entity\Serie s
                WHERE s.title LIKE :name 
                ORDER BY s.title ASC'
            )
            ->setParameter('name', '%'.$name.'%')
            ->getResult();
    }
}
