<?php

namespace App\Repository;

use App\Entity\Game;
use App\Entity\Playlist;
use App\Entity\Source;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Playlist|null find($id, $lockMode = null, $lockVersion = null)
 * @method Playlist|null findOneBy(array $criteria, array $orderBy = null)
 * @method Playlist[]    findAll()
 * @method Playlist[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlaylistRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Playlist::class);
    }

    /**
     * @param null $offset
     * @param null $limit
     * @return int|mixed|string
     */
    public function findAll($offset = null, $limit = null)
    {
        $qb = $this->createQueryBuilder('p')
            ->orderBy('p.title', 'ASC');

        if ($offset) {
            $qb->setFirstResult($offset);
        }

        if ($limit) {
            $qb->setMaxResults($limit);
        }

        return $qb->getQuery()->execute();
    }

    public function getFilteredPlaylists(string $status, Game $game = null, Source $source = null)
    {
        $dqlQuery = <<<DQL
SELECT p
FROM App\Entity\Playlist p
WHERE p.status = :status
DQL;
        $parameters = [
            'status' => $status,
        ];

        if ($game) {
            $dqlQuery = <<<DQL
$dqlQuery AND :game
MEMBER OF p.games
DQL;
            $parameters['game'] = $game;
        }

        if ($source) {
            $dqlQuery = <<<DQL
$dqlQuery AND p.source = :source
DQL;
            $parameters['source'] = $source;
        }

        $dqlQuery = <<<DQL
$dqlQuery
ORDER BY p.title ASC
DQL;

        $query = $this->getEntityManager()
            ->createQuery($dqlQuery);

        foreach ($parameters as $key => $value) {
            $query->setParameter($key, $value);
        }

        return $query->getResult();
    }

    public function getPlaylistsFromTitle($title)
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT p
                FROM App\Entity\Playlist p
                WHERE p.title LIKE :title
                ORDER BY p.title ASC'
            )
            ->setParameter('title', '%'.$title.'%')
            ->getResult();
    }

    public function getPlaylistsStatus()
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT DISTINCT p.status
                FROM App\Entity\Playlist p
                ORDER BY p.status ASC'
            )
            ->getResult();
    }
}
