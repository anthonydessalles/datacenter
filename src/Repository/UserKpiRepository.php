<?php

namespace App\Repository;

use App\Entity\Type;
use App\Entity\User;
use App\Entity\UserKpi;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserKpi|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserKpi|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserKpi[]    findAll()
 * @method UserKpi[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserKpiRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserKpi::class);
    }

    /**
     * @return array|mixed
     */
    public function findAll()
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT uk FROM App\Entity\UserKpi uk ORDER BY uk.user ASC, uk.kpiType ASC, uk.total DESC'
            )
            ->getResult();
    }
}
