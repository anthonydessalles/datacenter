<?php

namespace App\Repository;

use App\Entity\Recording;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Recording>
 *
 * @method Recording|null find($id, $lockMode = null, $lockVersion = null)
 * @method Recording|null findOneBy(array $criteria, array $orderBy = null)
 * @method Recording[]    findAll()
 * @method Recording[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RecordingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Recording::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(Recording $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(Recording $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function getRecordingsFromName($name)
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT r
                FROM App\Entity\Recording r
                WHERE r.name LIKE :name
                ORDER BY r.name ASC'
            )
            ->setParameter('name', '%'.$name.'%')
            ->getResult();
    }
}
