<?php

namespace App\Repository;

use App\Entity\User;
use App\Entity\UserHasScore;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class UserHasScoreRepository
 * @package App\Repository
 */
class UserHasScoreRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserHasScore::class);
    }

    /**
     * @return array|mixed
     */
    public function findAll()
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT us FROM App\Entity\UserHasScore us ORDER BY us.id ASC'
            )
            ->getResult();
    }

    /**
     * @param User $user
     * @return mixed
     */
    public function findAllbyUser(User $user)
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT us FROM App\Entity\UserHasScore us WHERE us.user = :user ORDER BY us.id ASC'
            )
            ->setParameter('user', $user)
            ->getResult();
    }
}
