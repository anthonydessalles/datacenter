<?php

namespace App\Repository;

use App\Entity\Location;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class LocationRepository
 * @package App\Repository
 */
class LocationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Location::class);
    }

    public function findAll()
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT l FROM App\Entity\Location l ORDER BY l.slug ASC'
            )
            ->getResult();
    }
}
