<?php

namespace App\Repository;

use App\Entity\Place;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class PlaceRepository
 * @package App\Repository
 */
class PlaceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Place::class);
    }

    public function findAll()
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT p FROM App\Entity\Place p ORDER BY p.name ASC'
            )
            ->getResult();
    }
}
