<?php

namespace App\Repository;

use App\Entity\Book;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class BookRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Book::class);
    }

    public function getBooks()
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT b
                FROM App\Entity\Book b
                ORDER BY b.title ASC'
            )
            ->getResult();
    }

    public function getBooksFromName($name)
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT b
                FROM App\Entity\Book b
                WHERE b.title LIKE :name 
                ORDER BY b.title ASC'
            )
            ->setParameter('name', '%'.$name.'%')
            ->getResult();
    }
}
