<?php

namespace App\Repository;

use App\Entity\Progression;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class ProgressionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Progression::class);
    }

    /**
     * @return array|mixed
     */
    public function findAll()
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT p FROM App\Entity\Progression p ORDER BY p.name ASC, p.game ASC'
            )
            ->getResult();
    }

    public function getProgressionsFromName($name)
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT p
                FROM App\Entity\Progression p
                WHERE p.name LIKE :name
                ORDER BY p.name ASC'
            )
            ->setParameter('name', '%'.$name.'%')
            ->getResult();
    }
}
