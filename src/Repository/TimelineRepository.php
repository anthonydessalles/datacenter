<?php

namespace App\Repository;

use App\Entity\Timeline;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class TimelineRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Timeline::class);
    }

    public function getTimelines()
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT t
                FROM App\Entity\Timeline t
                ORDER BY t.name ASC'
            )
            ->getResult();
    }

    public function getTimelinesFromSlug($slug)
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT t 
                FROM App\Entity\Timeline t
                WHERE t.name LIKE :slug 
                ORDER BY t.name ASC'
            )
            ->setParameter('slug', '%'.$slug.'%')
            ->getResult();
    }
}
