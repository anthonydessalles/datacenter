<?php

namespace App\Manager;

use App\Entity\File;
use App\Entity\Screenshot;
use App\Entity\Game;
use App\Entity\Playlist;
use App\Repository\FileRepository;

class GameManager
{
    private FileRepository $fileRepository;

    public function __construct(FileRepository $fileRepository)
    {
        $this->fileRepository = $fileRepository;
    }

    public function getGameMarkdownDetails(Game $game): array
    {
        $gameUpdates = $gameTypes = [];
        $files = [];
        
        /** @var Screenshot $screenshot */
        foreach ($game->getScreenshots() as $screenshot) {
            /** @var File $file */
            foreach ($this->fileRepository->getScreenshotFilesFromPath($screenshot->getTitle()) as $file) {
                $filePath = $file->getPath();
                $filePathArray = explode(" ", $filePath);
                $files[] = $file;
                
                foreach (Game::POSSIBLE_TYPES as $possibleType) {
                    if (in_array($possibleType, $filePathArray)) {
                        $gameUpdates[] = $filePath;
                        $gameTypes[] = $possibleType;
                    }
                }
            }
        }

        /** @var Playlist $playlist */
        foreach ($game->getPlaylists() as $playlist) {
            $playlistTitle = $playlist->getTitle();
            $playlistTitleArray = explode(" ", $playlistTitle);
            $gameUpdates[] = $playlistTitle;

            foreach (Game::POSSIBLE_TYPES as $possibleType) {
                if (in_array($possibleType, $playlistTitleArray)) {
                    $gameTypes[] = $possibleType;
                }
            }
        }

        $gameDetails = [
            'name' => $game->getName(),
            'french_name' => $game->getFrenchName(),
            'slug' => $game->getSlug(),
            'post_date' => $game->getDate(),
            'files' => $files,
            'playlists' => $game->getPlaylists(),
            'users' => $game->getUsers(),
            'types' => array_unique($gameTypes),
            'updates' => array_unique($gameUpdates),
        ];

        return $gameDetails;
    }
}
