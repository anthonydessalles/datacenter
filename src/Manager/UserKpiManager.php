<?php

namespace App\Manager;

use App\Entity\User;
use App\Entity\UserKpi;
use App\Repository\TypeRepository;
use App\Repository\UserKpiRepository;
use App\Repository\UserRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\Game;
use App\Entity\Score;

class UserKpiManager
{
    private UserRepository $userRepository;
    private TypeRepository $typeRepository;
    private UserKpiRepository $userKpiRepository;
    private ManagerRegistry $managerRegistry;

    private function getScoreTypeName(Score $score, Game $game = null, $includeDifficulty = false): string
    {
      $scoreType = $score && $score->getType() ? $score->getType()->getSlug() : 'n/a';

      if ($game) {
        $possibleScoreType = 'gaming';

        foreach ($game->getTypes() as $type) {
          if (in_array($type->getSlug(), Game::POSSIBLE_SCORE_TYPES)) {
            $possibleScoreType .= "_" . $type->getSlug();
          }
        }

        if (in_array($score->getDifficulty(), Game::GAMES_DIFFICULTIES) && $includeDifficulty) {
          $possibleScoreType .= "_" . $score->getDifficulty();
        }

        $scoreType = $possibleScoreType;
      }

      return $scoreType;
    }

    public function __construct(UserRepository $userRepository, TypeRepository $typeRepository, UserKpiRepository $userKpiRepository, ManagerRegistry $managerRegistry)
    {
        $this->userRepository = $userRepository;
        $this->typeRepository = $typeRepository;
        $this->userKpiRepository = $userKpiRepository;
        $this->managerRegistry = $managerRegistry;
    }

    public function getUserScoresAvg(User $user, $includeDifficulty = false): array
    {
        $userScores = [];
        $userScoresAvg = [];

        // Create types
        foreach ($user->getUserScores() as $userScore) {
          $scoreType = $this->getScoreTypeName($userScore->getScore(), $userScore->getScore()->getGame(), $includeDifficulty);

          $userScores[$scoreType] = [
            'count' => 0,
            'pts' => 0,
            'wins' => 0,
            'draws' => 0,
            'losses' => 0,
          ];
        }

        // Add stats
        foreach ($user->getUserScores() as $userScore) {
          $scoreType = $this->getScoreTypeName($userScore->getScore(), $userScore->getScore()->getGame(), $includeDifficulty);

          $userScores[$scoreType]['count'] += 1;
          $userScores[$scoreType]['pts'] += $userScore->getPoints();

          if ($userScore->getIsWin()) {
            $userScores[$scoreType]['wins'] += 1;
          }

          if ($userScore->getIsDraw()) {
            $userScores[$scoreType]['draws'] += 1;
          }

          if ($userScore->getIsLose()) {
            $userScores[$scoreType]['losses'] += 1;
          }
        }

        ksort($userScores);

        return $userScores;
    }

    public function saveUserKpis(User $user)
    {
      $entityManager = $this->managerRegistry->getManager();
      
      foreach ($user->getUserKpis() as $userKpi) {
        $entityManager->remove($userKpi);                
      }
      $entityManager->flush();

      $userData = [];
      foreach ($user->getActivities() as $activity) {
        if ($activity->getType()) {
          $activityTypeId = $activity->getType()->getId();
          if (isset($userData[$activityTypeId])) {
            $userData[$activityTypeId] += 1;
          } else {
            $userData[$activityTypeId] = 1;
          }
        }
      }

      if (!empty($userData)) {
        foreach ($userData as $typeId => $total) {
          $userKpi = new UserKpi();
          $userKpi->setUser($user);
          $userKpi->setType($this->typeRepository->find($typeId));
          $userKpi->setTotal($total);
          $userKpi->setUpdatedAt(new \DateTime());
          $userKpi->setKpiType(UserKpi::KPI_TYPE_ACTIVITY);

          $entityManager->persist($userKpi);
        }

        $entityManager->flush();
      }

      foreach ($this->getUserScoresAvg($user) as $key => $stats) {
        if ($this->typeRepository->findOneBy(['slug' => $key])) {
          $userKpi = new UserKpi();
          $userKpi->setUser($user);
          $userKpi->setType($this->typeRepository->findOneBy(['slug' => $key]));
          $userKpi->setTotal($stats['count']);
          $userKpi->setTotalPoints($stats['pts']);
          $userKpi->setTotalWins($stats['wins']);
          $userKpi->setTotalDraws($stats['draws']);
          $userKpi->setTotalLosses($stats['losses']);
          $userKpi->setUpdatedAt(new \DateTime());
          $userKpi->setKpiType(UserKpi::KPI_TYPE_SCORE);

          $entityManager->persist($userKpi);
        }
      }

      $entityManager->flush();

      return true;
    }
}
